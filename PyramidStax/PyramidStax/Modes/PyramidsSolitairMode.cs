﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.Delegation;
using PyramidStax.Cards;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Util;
using Sunbow.Util.Manipulation;

namespace PyramidStax.Modes
{
    public abstract class PyramidsSolitairMode : Mode
    {
        public abstract Stack Undos { get; }
        public abstract Deck Deck { get; }

        public bool IsInBackground { get; set; }
        public bool UserControlEnabled { get; protected set; }

        public Timer Timer { get; private set; }

        Texture2D blurredTexture;

        protected TwoValueAnimator<Color> backgroundColor = new TwoValueAnimator<Color>(new SinusSingleAnimator(Globals.LevelOverWaitDuration), new TwoValueInterpolator<Color>(Color.Lerp));

        int score;
        public int Score
        { 
            get { return score; } 
            set { 
                score = value; 
                scoreSize = Assets.FontSmall.MeasureString(score.ToString("### ##0")); 
            } 
        }

        protected Vector2 scoreSize;


        protected PyramidsSolitairMode()
        {
            Score = 0;
            Timer = new Timer(this);

        }
        public virtual void Reset()
        {
            Score = 0;
            Deck.CardMover.Clear();
        }

        public virtual void StartNewLevel()
        {
            backgroundColor.Interpolator.StartValue = Globals.Bright;
            backgroundColor.Animator.Reset();
            backgroundColor.Animator.IsPaused = true;
        }

        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (IsInBackground)
            {
                MakeBlurredTexture(spriteBatch);
            }
            GraphicsDevice.Clear(backgroundColor);

            Draw(spriteBatch);

            if (IsInBackground)
                DrawBlurredTexture(spriteBatch);
        }

        protected abstract void Draw(SpriteBatch batch);


        private void MakeBlurredTexture(SpriteBatch batch)
        {
            RenderTarget2D rt = new RenderTarget2D(Main.Instance.GraphicsDevice, 480 / 20, Globals.TIMER_POS_Y / 20);

            ViewController.CameraMatrix = Matrix.CreateScale(0.05f);
            GraphicsDevice.SetRenderTarget(rt);

            GraphicsDevice.Clear(backgroundColor);
            this.Draw(batch);

            ViewController.CameraMatrix = Matrix.Identity;
            GraphicsDevice.SetRenderTarget(null);

            blurredTexture = rt;
        }

        private void DrawBlurredTexture(SpriteBatch batch)
        {
            if (blurredTexture != null)
            {
                batch.Prepare();
                batch.Draw(blurredTexture, new Rectangle(0, 0, 480, Globals.TIMER_POS_Y), Color.White);
                batch.End();
            }
        }

        protected override void Update(GameTime gameTime, InputState inputState)
        {
            backgroundColor.Update(gameTime.GetElapsedSeconds());
        }

        
        internal abstract void LevelSolved();

        internal abstract void GameOver();
    }
}
