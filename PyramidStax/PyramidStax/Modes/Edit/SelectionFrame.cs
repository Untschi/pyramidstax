﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Shapes.Geometry;
using Shapes.Misc;

namespace PyramidStax.Modes.Edit
{
    public class SelectionFrame
    {

        bool isMouseDown;
        Vector2 dragStart, dragEnd;

        public Rect SelectionRect { get { return selectionRect; } }
        Rect selectionRect = new Rect(1, 1);


        public void MouseDown(Vector2 position)
        {
            isMouseDown = true;
            dragStart = position;
            dragEnd = dragStart;
            UpdateSelectionRect();
        }

        public void MouseUp(Vector2 position)
        {
            isMouseDown = false;
            dragEnd = position;

            UpdateSelectionRect();
        }

        public bool MouseUpdate(Vector2 position)
        {
            if (!isMouseDown || Vector2.DistanceSquared(position, dragEnd) <= 1)
                return false;

            dragEnd = position;
            UpdateSelectionRect();
            return true;
        }

        private void UpdateSelectionRect()
        {
            selectionRect.Position = new Vector2(Math.Min(dragStart.X, dragEnd.X), Math.Min(dragStart.Y, dragEnd.Y));
            selectionRect.ChangeDimension(MathHelper.Distance(dragStart.X, dragEnd.X), MathHelper.Distance(dragStart.Y, dragEnd.Y));
        }

        public void Draw(PrimitiveDrawer drawer)
        {
            if (!isMouseDown)
                return;

            drawer.DrawLineStrip(selectionRect.ToLineStrip(), Color.Green);
        }
    }
}
