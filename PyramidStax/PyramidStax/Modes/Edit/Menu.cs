﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.Delegation;
using PyramidStax.Modes.Menu;
using System.IO;
using Microsoft.Xna.Framework;
using starLiGHT.GUI.Widgets;
using starLiGHT.GUI;
using PyramidStax.Modes.Menu.LevelSelection;
using Sunbow.Util.IO;
using System.IO.IsolatedStorage;
using System.Reflection;
using System.Diagnostics;
using PyramidStax.Cards;
using Sunbow.Util;
using Sunbow.Util.Web;
using PyramidStax.Modes.Menu.Help;

namespace PyramidStax.Modes.Edit
{
    public class Menu : MenuMode
    {
        PyramidsEdit editMode;
        ContainerWidget settingsContainer, saveContainer;
        CheckButton settingsTab, saveTab, 
            loadBtn,
            EnableGrid, EnableAngleSnap;
        Button saveBtn, newBtn, uploadBtn, openDirBtn,
            cardUpBtn, cardDownBtn, cardDeleteBtn,
            rotLeftBtn, rotRightBtn,
            staxPlaceholderBtn, downloadLevels,
            helpBtn;
        TextBox levelNameTxbx;
        RadioButtonController tabController;
        LevelList levelList;


        public Menu(PyramidsEdit editMode)
            : base(Main.Instance.Proxy.GetMode<MainMenu>(), Main.Instance.GuiManager, "EditorMenu")
        {
            this.editMode = editMode;
        }

        protected override void Initialize()
        {
            container.Location = new Vector2(0, Globals.AD_HEIGHT);

            levelList = new LevelList("editor_load", container);
            levelList.LevelSelected += levelList_LevelSelected;
            levelList.PageContainer.Visible = false;
            levelList.PageContainer.Location = new Vector2(40, 80);

#if DEBUG
            CreateButton(Name + "_download_levels", new Vector2(70, 400), new Vector2(60, 60), DownloadLevels, "EmailButton", levelList.PageContainer);
#endif

            container.Widgets.Add(levelList.PageContainer);

            CreateBackButton(95);
            title.Visible = false;

            // SETTINGS
            settingsContainer = new ContainerWidget(Main.Instance.GuiManager, "settingsContainer");
            settingsContainer.Size = new Vector2(300, 160);
            settingsContainer.Location = new Vector2(100, 561);
            container.Widgets.Add(settingsContainer);

#if DEBUG
            staxPlaceholderBtn = CreateButton("placeholder", new Vector2(10, 10), new Vector2(60, 60), (o, a) => editMode.ConvertPlaceholder(), "StaxPlaceholderButton", settingsContainer);
#else
            helpBtn = CreateButton("help", new Vector2(10, 10), new Vector2(60, 60), (o, a) => ShowHelp(), "HelpButton", settingsContainer);
#endif
            cardDeleteBtn = CreateButton("cardDelete", new Vector2(10, 80), new Vector2(60, 60), (o, a) => editMode.DeleteCards(), "AbortButton", settingsContainer);

            rotLeftBtn = CreateButton("rotleft", new Vector2(90, 10), new Vector2(60, 60), null, "RotateLeftButton", settingsContainer);
            rotLeftBtn.StateChanged += RotationStateChanged;

            rotRightBtn = CreateButton("rotRight", new Vector2(90, 80), new Vector2(60, 60), null, "RotateRightButton", settingsContainer);
            rotRightBtn.StateChanged += RotationStateChanged;

            cardUpBtn = CreateButton("cardUp", new Vector2(160, 10), new Vector2(60, 60), (o, a) => editMode.CardUp(), "UpButton", settingsContainer);
            cardDownBtn = CreateButton("cardDown", new Vector2(160, 80), new Vector2(60, 60), (o, a) => editMode.CardDown(), "DownButton", settingsContainer);
          

            EnableGrid = CreateCheckButton("grid", new Vector2(240, 10), new Vector2(60, 60), (o, a) => SetGrid(), "GridSnapButton", settingsContainer);
            EnableAngleSnap = CreateCheckButton("angleSnap", new Vector2(240, 80), new Vector2(60, 60), (o, a) => SetAngleSnap(), "AngleSnapButton", settingsContainer);

            // SAVE
            saveContainer = new ContainerWidget(Main.Instance.GuiManager, "saveContainer");
            saveContainer.Size = new Vector2(300, 160);
            saveContainer.Location = new Vector2(100, 561);
            container.Widgets.Add(saveContainer);

            levelNameTxbx = new TextBox(GuiManager, "level_text_box")
            {
                Font = Assets.FontMedium,
#if DEBUG
                Size = new Vector2(210, 40),
#else
                Size = new Vector2(290, 40),
#endif
                Location = new Vector2(10, 20),
            };
            saveContainer.Widgets.Add(levelNameTxbx);

            newBtn = CreateButton("new_level", new Vector2(10, 80), new Vector2(60, 60), NewLevel, "NewButton", saveContainer);
            saveBtn = CreateButton("save_button", new Vector2(90, 80), new Vector2(60, 60), Save, "SaveButton", saveContainer);
            loadBtn = CreateCheckButton("load_btn", new Vector2(160, 80), new Vector2(60, 60), Load, "LoadButton", saveContainer);
#if DEBUG
            openDirBtn = CreateButton("open_dir", new Vector2(240, 10), new Vector2(60, 60), OpenDir, "ExploreButton", saveContainer);
#endif
            uploadBtn = CreateButton("upload", new Vector2(240, 80), new Vector2(60, 60), Upload, "EmailButton", saveContainer);

            settingsTab = CreateCheckButton("_settingsTab", new Vector2(420, 580), new Vector2(60, 60), (o, a) => TabCheckedChanged(), "SettingsTabButton", container);
            saveTab = CreateCheckButton("_saveTab", new Vector2(420, 640), new Vector2(60, 60), (o, a) => TabCheckedChanged(), "FileInfoTabButton", container);
            tabController = new RadioButtonController(settingsTab, saveTab);
            settingsTab.Checked = true;
        }

        void RotationStateChanged(object sender, EventArgs e)
        {
            if ((sender as Button).CurrentState.Name != "Pushed")
            {
                editMode.CardRotationDirection = 0;
            }
            else
            {
                editMode.CardRotationDirection = (sender == rotLeftBtn) ? -1 : 1;
            }
        }

        private void ShowHelp()
        {
            Proxy.SetMode<EditorHelpMode>();
        }

        private void TabCheckedChanged()
        {
            settingsContainer.Visible = settingsTab.Checked;
            saveContainer.Visible = saveTab.Checked;
        }


        private string Save(string fileName)
        {
            bool isStaxLevel = false;

            foreach (Card c in editMode.PyramidStack.Cards)
            {
                if (c.IsPlaceholder())
                {
                    isStaxLevel = true;
                    break;
                }
            }

            if (isStaxLevel)
                fileName = Path.Combine("Stax", fileName);
            else
                fileName = Path.Combine("Pyramids", fileName);

            string path = Path.Combine("Levels", fileName + ".csv");

            PersistHelper.SavePyramidStack(path, editMode.PyramidStack, isStaxLevel);

            return path;
        }

        private void SetGrid()
        {
            editMode.IsGridEnabled = EnableGrid.Checked;
        }
        private void SetAngleSnap()
        {
            editMode.IsAngleSnapEnabled = EnableAngleSnap.Checked;
        }

        private void NewLevel(object sender, EventArgs args)
        {
            levelNameTxbx.Text = string.Empty;
            editMode.NewLevel();
        }

        private void Save(object sender, EventArgs args)
        {
            if (string.IsNullOrWhiteSpace(levelNameTxbx.Text))
                return;

            Save(levelNameTxbx.Text);
        }

        private void Load(object sender, EventArgs args)
        {
            if (loadBtn.Checked)
            {
                levelList.FillWithOwnLevels(true, true);
                levelList.PageContainer.Visible = true;
                editMode.IsEditEnabled = false;
            }
            else
            {
                levelList.PageContainer.Visible = false;
                editMode.IsEditEnabled = true;
            }
        }

        void levelList_LevelSelected(Table obj, string id)
        {
            editMode.LoadLevel(obj);

            string lvlname = obj.FileName.Split('\\').Last();
            if(lvlname.EndsWith(".csv"))
                lvlname = lvlname.Remove(lvlname.Length - ".csv".Length);

            levelNameTxbx.Text = lvlname;

            loadBtn.Checked = false;
        }


        private void Upload(object sender, EventArgs args)
        {
            if (this.editMode.PyramidStack.CardsLeft < 9)
                return;

            string path = Save(this.levelNameTxbx.Text);


            WebQuery query = new WebQuery();
            query.AddEntries(
                "action", "submit_level",
                "name", this.levelNameTxbx.Text,
                "author", Main.Instance.GameSettings.PlayerName);

            using (IsolatedStorageFile store = Helper.GetUserStoreForAppDomain())
            using (IsolatedStorageFileStream stream = store.OpenFile(path, FileMode.OpenOrCreate))
            using (StreamReader reader = new StreamReader(stream))
                query.AddEntries("data", reader.ReadToEnd());

            WebRequester requester = new WebRequester("http://www.liquidfirearts.com/sunbow/games/pyramid_stax/pyramid_stax_levels.php", query);

            requester.RequestFailed += (a, b, c) => { Debugger.Break(); };
            requester.CreateWebRequest();
        }

        private void DownloadLevels(object sender, EventArgs args)
        {
            Main.Instance.WaitForWeb = true;

            WebQuery query = new WebQuery();
            query.AddEntries("action", "load_levels");

            WebRequester requester = new WebRequester("http://www.liquidfirearts.com/sunbow/games/pyramid_stax/pyramid_stax_levels.php", query);

            requester.RequestFailed += (a, b, c) => { Debugger.Break(); };
            requester.RequestSucceeded += (a) => { levelList.FillWithOnlineLevels(a); Main.Instance.WaitForWeb = false; };
            requester.CreateWebRequest();
        }

        private void OpenDir(object sender, EventArgs args)
        {
#if WINDOWS
            // take it for opening storage folder


            // Create a file in isolated storage.
            IsolatedStorageFile store = Sunbow.Util.Helper.GetUserStoreForAppDomain();

                if (!store.DirectoryExists("Levels"))
                    store.CreateDirectory("Levels");

            using (IsolatedStorageFileStream stream = new IsolatedStorageFileStream("storage_opener", FileMode.Create, store))
            {
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    writer.WriteLine("this file is created to retrieve the location of the isolated storage on windows.");
                    writer.Close();
                }
                stream.Close();
                // Retrieve the actual path of the file using reflection.
                string path = Path.Combine(
                    stream.GetType().GetField("m_FullPath", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(stream).ToString().TrimEnd("storage_opener".ToCharArray()), 
                    "Levels");


                Process.Start(path);
            }
#endif
        }
    }
}
