﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util;
using Sunbow.Util.Delegation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PyramidStax.Cards;
using Microsoft.Xna.Framework.Input;
using Shapes.Misc;
using Shapes.Misc.Appearance;
using PyramidStax.Modes.Pyramids;
using Sunbow.Util.IO;
using Shapes.Geometry;
using System.Diagnostics;

#if WINDOWS_PHONE
using Microsoft.Xna.Framework.Input.Touch;
#endif

namespace PyramidStax.Modes.Edit
{
    public class PyramidsEdit : ModeWithSubModes
    {
        const float ROTATION_PER_SECOND = MathHelper.PiOver2;
        static readonly Vector2 GridSpacing = new Vector2(10);
        static readonly float AngleSnapping = MathHelper.ToRadians(15);
        static readonly Rectangle EditorRect = new Rectangle(100, 800 - 160, 500, 180);


        Deck deck = new Deck(1, false, false, new Vector2(40, 700));

        public int CardRotationDirection { get { return cardRotationDirection; } set { previousCardRotationDirection = cardRotationDirection; cardRotationDirection = value; } }
        int cardRotationDirection, previousCardRotationDirection;

        public StaxStack PyramidStack { get { return pyramidStack; } }
        StaxStack pyramidStack;

        SelectionFrame selection = new SelectionFrame();

        PrimitiveDrawer drawer;

        List<Card> selectedCards = new List<Card>();
        bool dragging;

        public bool IsGridEnabled { get; set; }
        public bool IsAngleSnapEnabled { get; set; }

        public bool IsEditEnabled { get; set; }

        Timer timerDisplay = new Timer(null);
        float snappingUpdateTimer;

        float lastRotation;

        public PyramidsEdit()
        {
        }
        
        protected override void  Initialize()
        {
            pyramidStack = new StaxStack(deck);

            AddSubMode(new Menu(this));

            drawer = new PrimitiveDrawer(GraphicsDevice);

            IsEditEnabled = true;

            base.Initialize();
        }

        protected override void Enter()
        {
            //TextureGenerator generator = new TextureGenerator(GraphicsDevice);
            //selectionBorder =  generator.GeometryBorder(new Card(null, CardColor.undefined, CardValue.undefined).Bounds, Brush.CreateFallOffBrush(3, 2));

            Card.ForceTransform = true;

            base.Enter();
        }


        protected override void Leave()
        {
            Card.ForceTransform = false;
            base.Leave();
        }

        protected override void Update(GameTime gameTime, InputState inputState)
        {
            if (!IsEditEnabled)
                return;

            float seconds = gameTime.GetElapsedSeconds();

            deck.Update(seconds);


            Vector2 mousePosition = inputState.CurrentMouseState.GetPosition();

#if WINDOWS_PHONE

            List<Vector2> touches = GetPressedTouchPoints(inputState.TouchState);
            if (touches.Count > 1)
            {
                float x = touches[0].X - touches[1].X;
                float y = touches[0].Y - touches[1].Y;
                float rotation = (float)Math.Atan2(y, x);

                if (rotation != lastRotation)
                {
                    RotateCards(rotation - lastRotation);
                    lastRotation = rotation;
                }
                dragging = false;
                EnsureConstraints(selectedCards);
            }
            else
            {
#endif
            Vector2 mouseOffset = mousePosition - inputState.LastMouseState.GetPosition();

            if(selection.MouseUpdate(mousePosition))
            {
                UpdateSelection();
            }

            if (dragging)
            {
                foreach(Card c in selectedCards)
                    c.Position += mouseOffset;
            }

            if (!EditorRect.Contains(mousePosition.ToPoint()))
            {
                // MOUSE DOWN
                if (inputState.CurrentMouseState.LeftButton == ButtonState.Pressed
                    && inputState.LastMouseState.LeftButton == ButtonState.Released)
                {

                    if (deck.MainStack.Bounds.IsPointInside(mousePosition) && deck.CardsLeft > 0)
                    {
                        selectedCards.Clear();
                        Card c = deck.TakeCard();
                        selectedCards.Add(c);
                        pyramidStack.AddCard(c);
                        dragging = true;
                    }
                    else
                    {
                        Card c = pyramidStack.SelectCard(mousePosition, false);
                        if (c == null)
                        {
                            selection.MouseDown(mousePosition);
                        }
                        else if (selectedCards.Contains(c))
                        {
                            dragging = true;
                        }
                        else
                        {
                            selectedCards.Clear();
                            selectedCards.Add(c);
                            dragging = true;
                        }

                    }
                }
                // MOUSE UP
                else if (inputState.CurrentMouseState.LeftButton == ButtonState.Released
                    && inputState.LastMouseState.LeftButton == ButtonState.Pressed)
                {
                    if (dragging)
                    {
                        dragging = false;
                        pyramidStack.UpdateBlockings();
                        EnsureConstraints(selectedCards);
                    }
                    else
                    {
                        selection.MouseUp(mousePosition);
                        UpdateSelection();
                    }
                }
            }

#if WINDOWS_PHONE
            }
#endif

            if (inputState.IsNewKeyPress(Keys.Delete))
            {
                DeleteCards();
            }

            if (inputState.CurrentKeyboardStates[0].IsKeyDown(Keys.LeftShift))
            {
                float x = 0;
                float y = 0;
                if (inputState.CurrentKeyboardStates[0].IsKeyDown(Keys.Up))
                    y = -1;
                if (inputState.CurrentKeyboardStates[0].IsKeyDown(Keys.Down))
                    y = 1;
                if (inputState.CurrentKeyboardStates[0].IsKeyDown(Keys.Left))
                    x = -1;
                if (inputState.CurrentKeyboardStates[0].IsKeyDown(Keys.Right))
                    x = 1;

                foreach (Card c in pyramidStack.Cards)
                {
                    c.Position += new Vector2(x, y);
                }
            }
            else
            {

                if (CardRotationDirection == -1 || inputState.CurrentKeyboardStates[0].IsKeyDown(Keys.Left))
                    RotateCards(-seconds * ROTATION_PER_SECOND);
                if (CardRotationDirection == 1 || inputState.CurrentKeyboardStates[0].IsKeyDown(Keys.Right))
                    RotateCards(seconds * ROTATION_PER_SECOND);

                if (inputState.IsNewKeyPress(Keys.Left) || inputState.IsNewKeyPress(Keys.Right) || (cardRotationDirection != 0 && previousCardRotationDirection == 0))
                {
                    pyramidStack.UpdateBlockings();
                }

                if ((inputState.CurrentKeyboardStates[0].IsKeyUp(Keys.Left) && inputState.LastKeyboardStates[0].IsKeyDown(Keys.Left))
                    || (inputState.CurrentKeyboardStates[0].IsKeyUp(Keys.Right) && inputState.LastKeyboardStates[0].IsKeyDown(Keys.Right))
                    || (cardRotationDirection == 0 && previousCardRotationDirection != 0))
                {
                    EnsureConstraints(selectedCards);
                }

                if (inputState.IsNewKeyPress(Keys.Up))
                {
                    CardUp();
                }

                if (inputState.IsNewKeyPress(Keys.Down))
                {
                    CardDown();
                }
            }
            

            if (inputState.IsNewKeyPress(Keys.Insert)) // PLACEHOLDER
            {
                AddPlaceholder(mousePosition);
            }
            

            base.Update(gameTime, inputState);
        }

#if WINDOWS_PHONE
        private List<Vector2> GetPressedTouchPoints(TouchCollection touches)
        {
            List<Vector2> list = new List<Vector2>();
            foreach (TouchLocation loc in touches)
            {
                if (loc.State == TouchLocationState.Pressed || loc.State == TouchLocationState.Moved)
                {
                    list.Add(loc.Position);
                }
            }
            return list;
        }
#endif

        public void AddPlaceholder(Vector2 position)
        {
            selectedCards.Clear();

            Card placeHolder = new Card(null, CardColor.undefined, CardValue.undefined)
            {
                Position = position,
            };
            selectedCards.Add(placeHolder);
            pyramidStack.AddCard(placeHolder);
        }

        public void ConvertPlaceholder()
        {
            List<Card> tmp = new List<Card>();
            foreach (Card c in selectedCards)
            {
                if (c.IsPlaceholder())
                {
                    Card card = deck.TakeCard();
                    card.Position = c.Position;
                    card.Rotation = c.Rotation;

                    pyramidStack.TakeCard(c);
                    pyramidStack.AddCard(card);
                    tmp.Add(card);
                }
                else
                {
                    Card placeHolder = new Card(null, CardColor.undefined, CardValue.undefined)
                    {
                        Position = c.Position,
                        Rotation = c.Rotation,
                    };
                    pyramidStack.AddCard(placeHolder);
                    tmp.Add(placeHolder);

                    pyramidStack.TakeCard(c);
                    deck.AddCard(c);
                }
            }
            selectedCards.Clear();
            selectedCards.AddRange(tmp);
        }

        public void CardUp()
        {
            if (selectedCards.Count == 1)
            {
                Card card = selectedCards[0];
                pyramidStack.ArrangeUp(card);
                pyramidStack.UpdateBlockings();
            }
        }

        public void CardDown()
        {
            if (selectedCards.Count == 1)
            {
                Card card = selectedCards[0];
                pyramidStack.ArrangeDown(card);
                pyramidStack.UpdateBlockings();
            }
        }

        public void DeleteCards()
        {
            foreach (Card c in selectedCards)
            {
                pyramidStack.TakeCard(c);

                if(c.Deck == deck)
                    deck.AddCard(c);
            }

            selectedCards.Clear();
        }

        private void EnsureConstraints(IEnumerable<Card> cards)
        {
            foreach (Card c in cards)
            {
                KeepInBounds(c);

                if (IsGridEnabled)
                {
                    c.Position = new Vector2(
                        Snap(c.Position.X, GridSpacing.X),
                        Snap(c.Position.Y, GridSpacing.Y));
                }

                if (IsAngleSnapEnabled)
                {
                    c.Rotation = Snap((float)c.Rotation + MathHelper.TwoPi, AngleSnapping);
                }
            }
        }

        private void KeepInBounds(Card c)
        {
            const int top = 80;
            const int bottom = 625;
            const int left = 0;
            const int right = 480;

            float x = MathHelper.Clamp(c.Position.X, left, right);
            float y = MathHelper.Clamp(c.Position.Y, top, bottom);

            c.Position = new Vector2(x, y);
        }

        private float Snap(float currentValue, float snappingStep)
        {
            float val = currentValue;
            float mod = val % snappingStep;

            if (mod > 0)
            {
                val -= mod;

                if (snappingStep - mod < mod)
                    val += snappingStep;
            }
            return val;
        }

        private void UpdateSelection()
        {
            selectedCards.Clear();

            foreach(Card c in pyramidStack.Cards)
            {
                if(CollisionDetector2D.RectRectIntersection(c.Bounds, selection.SelectionRect))
                {
                    selectedCards.Add(c);
                }
            }
        }

        public void RotateCards(float angle)
        {
            Vector2 rotationCenter = new Vector2();
            foreach (Card c in selectedCards)
                rotationCenter += c.Position;

            rotationCenter /= selectedCards.Count;

            foreach (Card c in selectedCards)
            {
                Vector2 dir = c.Position - rotationCenter;
                float dist = dir.Length();
                float rot = angle + (float)Math.Atan2(dir.Y, dir.X);
                c.Position = rotationCenter + new Vector2(dist * (float)Math.Cos(rot), dist * (float)Math.Sin(rot));

                c.Rotation -= angle;
            }
        }

        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {

            spriteBatch.Prepare();

            //spriteBatch.Draw(Assets.GuiSheet.Texture, new Rectangle(0, Globals.TURNER_POS_Y, 400, 100), Assets.ColorDefault.SourceRectangle, Color.White);
            spriteBatch.Draw(Assets.GuiSheet.Texture, new Rectangle(420, Globals.TURNER_POS_Y, 60, 200), Assets.ColorDark.SourceRectangle, Color.White);
            //Assets.GuiSheet.Draw(spriteBatch, Assets.GuiSheet.GetFrame("editor_bg"), new Vector2(0, 800), 0, 1);

            deck.MainStack.Draw(spriteBatch);

            if (IsEditEnabled)
            {
                pyramidStack.Draw(spriteBatch);

                deck.DrawMovingCards(spriteBatch);
            }

            timerDisplay.Draw(spriteBatch);

            Vector2 size = Assets.FontSmall.MeasureString(deck.CardsLeft.ToString());
            spriteBatch.DrawString(Assets.FontSmall, deck.CardsLeft.ToString(), deck.MainStack.CardOnTop.Position - 0.5f * size + new Vector2(3, 2), Color.DarkOrange);

            pyramidStack.DrawCardOrder(spriteBatch);

            spriteBatch.End();

            foreach(Card c in selectedCards)
                drawer.DrawLineStrip(c.Bounds.ToLineStrip(), Color.Red);

            selection.Draw(drawer);


            base.Draw(gameTime, spriteBatch);
        }

        internal void NewLevel()
        {
            this.pyramidStack.Clear();
            this.selectedCards.Clear();
            this.deck.CollectAndShuffle();
        }

        internal void LoadLevel(Table obj)
        {
            NewLevel();
            try
            {
                PersistHelper.LoadPyramidStack(obj, deck, pyramidStack, true);
            }
            catch (Exception) { Debugger.Break(); }
        }
    }
}
