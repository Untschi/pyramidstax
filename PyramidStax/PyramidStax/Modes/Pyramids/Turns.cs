﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PyramidStax.Cards;
using Microsoft.Xna.Framework;
using Shapes.Geometry;
using Sunbow.Util.Delegation;

namespace PyramidStax.Modes.Pyramids
{
    public class PlayJokerTurn : Turn<PyramidsGame>
    {

        public override bool Do(PyramidsGame game)
        {
            if (game.Jokers.CardsLeft == 0)
                return false;

            Card card = game.Jokers.TakeCard();
            game.TurnedCards.AddCard(card);

            TurnTracker.AddToSeries(card);

            return true;
        }
        public override void Undo(PyramidsGame game)
        {
            game.Jokers.AddCard(game.TurnedCards.TakeCard());
        }
    }


    public class TakeCardAwayTurn : Turn<PyramidsGame>
    {
        //const int SCORE_SINGLE_CARD = 5;
        Card card;
        Vector2 originalPosition;
        Angle originalRotation;

        public TakeCardAwayTurn(Card card)
        {
            this.card = card;
            this.originalPosition = card.Position;
            this.originalRotation = card.Rotation;
        }

        public override bool Do(PyramidsGame game)
        {
            game.Pyramid.TakeCard(card);
            game.TurnedCards.AddCard(card);

            TurnTracker.AddToSeries(card);

            //TurnTracker.Score += TurnTracker.SeriesCount * SCORE_SINGLE_CARD;

            return true;
        }

        public override void Undo(PyramidsGame game)
        {
            Card c = game.TurnedCards.TakeCard();
            game.Pyramid.AddCard(c);
            c.Position = originalPosition;
            c.Rotation = originalRotation;


            if (TurnTracker.SeriesCount > 0)
            {
                //TurnTracker.Score -= TurnTracker.SeriesCount * SCORE_SINGLE_CARD;
                TurnTracker.RemoveLastFromSeries();
            }
            game.Pyramid.UpdateBlockings();
        }

    }

    public class NewCardTurn : Turn<PyramidsGame>
    {
        public override bool Do(PyramidsGame game)
        {
            if (game.Deck.CardsLeft == 0)
                return false;

            game.TurnedCards.AddCard(game.Deck.TakeCard());

            TurnTracker.ClearSeries();
            TurnTracker.ResetTurnPreview();

            return true;
        }
        public override void Undo(PyramidsGame game)
        {
            game.Deck.MainStack.AddCard(game.TurnedCards.TakeCard());
        }

    }

}
