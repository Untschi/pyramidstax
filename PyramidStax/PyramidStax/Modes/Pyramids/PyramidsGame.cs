﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util;
using Sunbow.Util.Delegation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PyramidStax.Cards;
using Microsoft.Xna.Framework.Input;
using Shapes.Geometry;
using Shapes.Misc;
using Sunbow.Util.IO;
using PyramidStax.Modes.Menu.Hud;
using PyramidStax.Modes.Feedback;

namespace PyramidStax.Modes.Pyramids
{
    public class PyramidsGame : PyramidsSolitairMode
    {
        public override Deck Deck { get { return deck; } }
        public Stack TurnedCards { get; private set; }
        public Stack Jokers { get; private set; }
        public PyramidStack Pyramid { get; private set; }
        public override Stack Undos { get { return undos; } }
        Stack undos;
        Deck deck;

        

        TurnTracker tracker;



        public SpecialHighlightManager HighlightManager { get; private set; }
        CardSelectionHighlighter selector = new CardSelectionHighlighter();

#if DEBUG
        PrimitiveDrawer drawer;
#endif

        public PyramidsGame()
        { }

        protected override void Initialize()
        {
            deck = new Deck(1, false, true, new Vector2(100, 715));
            deck.CardMover.DefaultWaitTime = 0.15f;

#if DEBUG
            drawer = new PrimitiveDrawer(GraphicsDevice);
#endif

            TurnedCards = new Stack(deck)
            {
                Position = new Vector2(200, 730),
                PositionOffest = new Vector2(5, 6),
                OrientationOffset = new Angle(0.2f),
                Orderly = false,
                Hidden = false,
            };
            Jokers = new Stack(deck)
            {
                Position = new Vector2(320, 740),
                PositionOffest = new Vector2(1, 4),
                Orderly = true,
                Hidden = false,
                CardOnTopAtPosition = true,
            };
            undos = new Stack(deck)
            {
                Position = new Vector2(400, 740),
                PositionOffest = new Vector2(1, 4),
                Orderly = true,
                Hidden = false,
                CardOnTopAtPosition = true,
            };
            tracker = new TurnTracker(this);

            HighlightManager = new SpecialHighlightManager();

        }

        public override void Reset()
        {
            undos.Clear();
            Jokers.Clear();
            base.Reset();
        }

        protected override void Enter()
        {
        }

        protected override void Leave()
        {
            Card.ForceWaitForTurnTransform = false;
        }

        public override void StartNewLevel()
        {
            base.StartNewLevel();

            Card.ForceWaitForTurnTransform = true;
            LoadLevel(Main.Instance.GameSettings.CurrentLevelData);
            tracker.ClearSeries();
            Card.ForceWaitForTurnTransform = true;

            undos.AddCard(new Card(deck, CardColor.SpecialCardBlack, CardValue.Undo));
        }

        public void LoadLevel(Table levelData)
        {
            UserControlEnabled = false;

            deck.CollectAndShuffle();

            Timer.Reset();
            TurnedCards.Clear();

            Pyramid = new PyramidStack(Deck);
            //deck.CardMover.AddCardEvent(null, (c) =>
            //    {

                    using (new CardMoveTimer(deck.CardMover, false))
                    {
                        //deck.CardMover.AddCardEvent(null, (c1) => 
                        //{ 

                        Pyramid.HideBlockedCards = false;
                        deck.CardMover.RegisterMovingStack(Pyramid);
                        PersistHelper.LoadPyramidStack(levelData, deck, Pyramid);

                        deck.CardMover.AddCardEvent(null,
                            (c2) =>
                            {
                                using (new CardMoveTimer(deck.CardMover, true))
                                {
                                    deck.CardMover.RegisterMovingStack(Pyramid);
                                    Pyramid.HideBlockedCards = true;
                                    Pyramid.UpdateBlockings();
                                    //Deck.CardMover.RegisterMovingStack(Pyramid);
                                    TurnedCards.AddCard(deck.TakeCard());
                                    Timer.Start(Main.Instance.GameSettings.LevelDuration);
                                    UserControlEnabled = true;
                                }
                            }, 1);
                        tracker.Reset();

                        //}, 1);
                    }
                //});
        }

        protected override void Update(GameTime gameTime, InputState inputState)
        {
            base.Update(gameTime, inputState);

            float seconds = gameTime.GetElapsedSeconds();

            HighlightManager.Update(seconds);
            deck.Update(seconds);

            if (IsInBackground || !UserControlEnabled)
                return;

            selector.Update(seconds);

            Timer.Update(seconds);

            if (!isInitialized)
            {
                if (deck.CardMover.IsWorking)
                {
                    //Pyramid.UpdateBlockings();
                    isInitialized = true;
                }
                return;
            }


            if (inputState.CurrentMouseState.LeftButton == ButtonState.Pressed
                && inputState.LastMouseState.LeftButton == ButtonState.Released)
            {
                Vector2 mousePosition = inputState.CurrentMouseState.GetPosition();

                if (deck.MainStack.Bounds.IsPointInside(mousePosition))          // DECK
                {
                    tracker.Do(new NewCardTurn());

                    if (CheckGameOver())
                    {
                        deck.CardMover.AddCardEvent(null, (p) => GameOver(), Globals.LevelOverWaitDuration, true);
                        //Proxy.SetMode<Modes.Menu.MainMenu>();
                        //return;
                    }
                }
                else if (Jokers.Bounds.IsPointInside(mousePosition))             // JOKER
                {
                    tracker.Do(new PlayJokerTurn());
                }
                else if (Undos.Bounds.IsPointInside(mousePosition))              // UNDO
                {
                    tracker.Undo();
                }
                else if (TurnedCards.Bounds.IsPointInside(mousePosition))       // TURNED
                {
                    TurnedCards.MakeOrderly(new Vector2(0.3f, -0.2f), 0);
                }
                else                                                            // PYRAMID
                {
                    Card c = Pyramid.SelectCard(mousePosition);
                    if (c != null)
                    {
                        if (TurnedCards.CardOnTop != null && c.Value.IsNeighbour(TurnedCards.CardOnTop.Value))
                        {
                            tracker.Do(new TakeCardAwayTurn(c));

                            if (CheckWin())
                            {
                                deck.CardMover.AddCardEvent(c, (p) => LevelSolved(), Globals.LevelOverWaitDuration, true);

                                //LoadLevel(Main.Instance.GameSettings.LevelData);
                            }
                            else if (CheckGameOver())
                            {
                                deck.CardMover.AddCardEvent(c, (p) => GameOver(), Globals.LevelOverWaitDuration, true);

                                //Proxy.SetMode<Modes.Menu.MainMenu>();
                                //return;
                            }
                        }
                        else
                        {
                            selector.FadeHighlight(c);
                        }
                    }
                }
            }

            if (inputState.IsNewKeyPress(Keys.R) && inputState.CurrentKeyboardStates[0].IsKeyDown(Keys.LeftControl))
            {
                LoadLevel(Main.Instance.GameSettings.CurrentLevelData);
            }
            
        }

        internal override void LevelSolved()
        {
            Timer.SetPlayState(true);
            Proxy.GetMode<LevelOverScreen>().GameIsOver = !Main.Instance.GameSettings.NextLevel();
            Proxy.GetMode<LevelOverScreen>().LevelSolved = true;
            Proxy.GetMode<LevelOverScreen>().BackgroundMode = this;
            Proxy.SetMode<LevelOverScreen>();

        }

        internal override void GameOver()
        {
            if (!CheckGameOver())
                return;

            Timer.SetPlayState(true);

            Proxy.GetMode<LevelOverScreen>().GameIsOver = true;
            Proxy.GetMode<LevelOverScreen>().LevelSolved = false;
            Proxy.GetMode<LevelOverScreen>().BackgroundMode = this;
            Proxy.SetMode<LevelOverScreen>();
        }

        protected override void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Prepare();

            deck.MainStack.Draw(spriteBatch);
            TurnedCards.Draw(spriteBatch);

            Undos.Draw(spriteBatch);
            Jokers.Draw(spriteBatch);

            Pyramid.Draw(spriteBatch);

            HighlightManager.Draw(spriteBatch);
            tracker.Draw(spriteBatch);
            Timer.Draw(spriteBatch);
            spriteBatch.DrawString(Assets.FontSmall, Score.ToString("### ##0"), new Vector2(80 - scoreSize.X, 2 + Globals.TIMER_POS_Y), Globals.VeryDark);

            if (Deck.CardsLeft > 0 && UserControlEnabled)
            {
                Vector2 size = Assets.FontSmall.MeasureString(Deck.CardsLeft.ToString());
                spriteBatch.DrawString(Assets.FontSmall, Deck.CardsLeft.ToString(), Deck.MainStack.Position - 0.5f * size + new Vector2(3, 3), Color.DarkOrange);
            }

            deck.DrawMovingCards(spriteBatch);

            //spriteBatch.End();

            //spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive);

            selector.Draw(spriteBatch);


            spriteBatch.End();

#if DEBUG
            //foreach (Card c in Pyramid.Cards)
            //    drawer.DrawLineStrip(c.Bounds.ToLineStrip(), Color.Red);
#endif
        }

        public bool CheckGameOver()
        {
            if (Timer.TimeLeft > 0 && (deck.MainStack.CardsLeft > 0 || Jokers.CardsLeft > 0))
                return false;

            bool over = true;

            if (Timer.TimeLeft > 0)
            {
                foreach (Card c in Pyramid.Cards)
                {
                    if (!c.IsBlocked && c.Value.IsNeighbour(TurnedCards.CardOnTop.Value))
                    {
                        over = false;
                        break;
                    }
                }
            }

            if (over)
            {
                Timer.SetPlayState(false);
                UserControlEnabled = false;

                backgroundColor.Interpolator.EndValue = Globals.FailColor;
                backgroundColor.Animator.IsPaused = false;
            }
            return over;
        }

        public bool CheckWin()
        {
            bool win = Pyramid.CardsLeft == 0;

            if (win)
            {
                Timer.SetPlayState(false);
                UserControlEnabled = false;

                backgroundColor.Interpolator.EndValue = Globals.SuccessColor;
                backgroundColor.Animator.IsPaused = false;
            }

            return win;
        }

    }
}
