﻿using System;
using System.Collections.Generic;
using System.Text;
using PyramidStax.Cards;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Util.Sprites;
using PyramidStax.Modes.Feedback;

namespace PyramidStax.Modes.Pyramids
{
    public class TurnTracker : TurnTracker<PyramidsGame>
    {
        static readonly Vector2 KILLED_SPECIAL_CARDS_POS = new Vector2(480 / 2, -100);

        enum SpecialHighlightType
        {
            None = 0,
            Undo = 1,
            Joker = 2,
            TimeBonus = 3,
        }

        public TurnTracker(PyramidsGame game)
            : base(game)
        { }

        internal override void CheckSeries()
        {
            if (lastSeries.Count <= 4)                          // Give Joker
            {
                List<CardColor> colors = new List<CardColor>();
                foreach (Card c in lastSeries)
                {
                    if (c.CardColor == CardColor.SpecialCardBlack || c.CardColor == CardColor.SpecialCardRed)
                        break;

                    if (!colors.Contains(c.CardColor))
                        colors.Add(c.CardColor);
                }

                if (colors.Count != lastSeries.Count)
                    turnPreview[3] = (int)SpecialHighlightType.None;

                if (colors.Count == 4)
                {
                    CardColor color = (Sunbow.Util.Random.Next(2) == 0) ? CardColor.SpecialCardBlack : CardColor.SpecialCardRed;
                    game.HighlightManager.Highlight(new JokerHighlighter(game.HighlightManager, game, color));
                    
                    //Card joker = new Card(game.Deck, color, CardValue.Joker);
                    //joker.Position = KILLED_SPECIAL_CARDS_POS;
                    //game.Jokers.AddCard(joker);
                }
            }
            if (lastSeries.Count >= 2)                          // Time Bonus
            {
                CardColor color1 = lastSeries[lastSeries.Count - 1].CardColor;
                CardColor color2 = lastSeries[lastSeries.Count - 2].CardColor;


                if (color1 == color2)
                {
                    turnPreview[lastSeries.Count] = (int)SpecialHighlightType.TimeBonus;

                    if (lastSeries.Count >= 3)
                    {
                        CardColor color3 = lastSeries[lastSeries.Count - 3].CardColor;
                        if (color2 == color3)
                        {
                            game.HighlightManager.Highlight(new TimeBonusHighlighter(game.HighlightManager));

                            game.Timer.GiveTimeBonus(12);
                        }
                    }
                }
                else if (lastSeries.Count <= turnPreview.Length 
                    && turnPreview[lastSeries.Count - 1] == (int)SpecialHighlightType.TimeBonus)
                {
                    turnPreview[lastSeries.Count - 1] = (int)SpecialHighlightType.None;
                }
            }
            if (lastSeries.Count > 0 && lastSeries.Count % 7 == 0)                      // Undo Card
            {
                CardColor color = (Sunbow.Util.Random.Next(2) == 0) ? CardColor.SpecialCardBlack : CardColor.SpecialCardRed;
                game.HighlightManager.Highlight(new UndoHighlighter(game.HighlightManager, game, color));
                //Card undo = new Card(game.Deck, color, CardValue.Undo);
                //undo.Position = KILLED_SPECIAL_CARDS_POS;
                //game.Undos.AddCard(undo);
            }
        }

        public override void Draw(SpriteBatch batch)
        {
            Assets.GuiSheet.Draw(batch, Assets.TurnBackgroundFrame, new Vector2(0, Globals.TURNER_POS_Y), 0, 1);

            for (int i = 0; i < turnPreview.Length; i++)
            {
                Vector2 pos = new Vector2(Assets.TurnFrameNormal.SourceRectangle.Width * i, Globals.TIMER_POS_Y);
                if (i < lastSeries.Count)
                {
                    pos = new Vector2(pos.X, pos.Y + Globals.TIMER_HEIGHT + 2);
                }

                Frame frame = (turnPreview[i] == (int)SpecialHighlightType.None) ? Assets.TurnFrameNormal : Assets.TurnFrameHighlight;

                Assets.GuiSheet.Draw(batch, frame, pos, 0, 1);

                if (i < lastSeries.Count)
                {
                    CardColor col = lastSeries[i].CardColor;
                    int frameIdx = (lastSeries[i].Value == CardValue.Joker) ? 4 : (int)col;
                    Assets.GuiSheet.Draw(batch, Assets.CardColorSymbols.CalculateFrame(frameIdx), pos, 0, 1, col.GetFillColor());
                    Assets.GuiSheet.Draw(batch, Assets.CardColorSymbols.CalculateFrame(frameIdx + 5), pos, 0, 1, col.GetOutlineColor());
                }

                if (turnPreview[i] == (int)SpecialHighlightType.Joker)
                {
                    List<CardColor> cols = new List<CardColor>() { CardColor.Spades, CardColor.Heart, CardColor.Cross, CardColor.Diamonds,  };
                    foreach (Card c in lastSeries)
                        cols.Remove(c.CardColor);

                    for (int j = lastSeries.Count; j <= i; j++)
                    {
                        int idx = j - lastSeries.Count;
                        Vector2 p = new Vector2(Assets.TurnFrameNormal.SourceRectangle.Width * j, Globals.TIMER_POS_Y + Globals.TIMER_HEIGHT);
                        Assets.GuiSheet.Draw(batch, Assets.CardColorSymbols.CalculateFrame((int)cols[idx] + 5), p, 0, 1, cols[idx].GetFillColor());
                    }
                }
                else if (turnPreview[i] == (int)SpecialHighlightType.TimeBonus && i == lastSeries.Count)
                {
                    CardColor col = lastSeries[i - 1].CardColor;
                    pos = new Vector2(pos.X, pos.Y + Globals.TIMER_HEIGHT);
                    Assets.GuiSheet.Draw(batch, Assets.CardColorSymbols.CalculateFrame((int)col + 5), pos, 0, 1, col.GetFillColor());
                }
            }


        }

        internal override void ResetTurnPreview()
        {
            for (int i = 0; i < turnPreview.Length; i++)
            {
                if (i == 3)
                    turnPreview[i] = (int)SpecialHighlightType.Joker;
                else if ((i + 1) % 7 == 0)
                    turnPreview[i] = (int)SpecialHighlightType.Undo;
                else
                    turnPreview[i] = (int)SpecialHighlightType.None;
            }
        }

        protected override Vector2 KilledUndoCardPosition
        {
            get { return KILLED_SPECIAL_CARDS_POS; }
        }
    }
}
