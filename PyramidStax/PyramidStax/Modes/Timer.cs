﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.Manipulation;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace PyramidStax.Modes
{
    public class Timer
    {
        SingleAnimator updater = new LinearSingleAnimator(0, 1, 0, AnimationRepeatLogic.Pause);

        public float TimeLeft { get { return (1 - updater.Value) * updater.Duration; } }
        PyramidsSolitairMode parentMode;

        public Timer(PyramidsSolitairMode parent)
        {
            this.parentMode = parent;

            updater.Pause();
            updater.EndOfAnimation += TimeOut;
        }

        public void Start(float duration)
        {
            updater.Duration = duration;
            updater.Reset();
            updater.Play();
        }

        public void Reset()
        {
            updater.Reset();
        }

        public void GiveTimeBonus(float seconds)
        {
            updater.Duration += seconds;
        }

        void TimeOut(object sender, EventArgs e)
        {
            parentMode.GameOver();
        }

        public void SetPlayState(bool playing)
        {
            if (playing)
                updater.Play();
            else
                updater.Pause();
        }

        public void Update(float seconds)
        {
            updater.Update(seconds);
        }

        public void Draw(SpriteBatch batch)
        {
            Assets.GuiSheet.Draw(batch, Assets.TimeInnerFrame, new Vector2(100, Globals.TIMER_POS_Y + 5), 0, 1, Color.Red);
            Assets.GuiSheet.Draw(batch, Assets.TimeInnerFrame, new Vector2(100, Globals.TIMER_POS_Y + 5), 0, new Vector2(1 - updater, 1), Color.LightGreen);
            Assets.GuiSheet.Draw(batch, Assets.TimeBorderFrame, new Vector2(0, Globals.TIMER_POS_Y), 0, 1);
        }


    }
}
