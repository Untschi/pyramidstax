﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util;
using Sunbow.Util.Delegation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PyramidStax.Cards;
using Microsoft.Xna.Framework.Input;
using Sunbow.Util.IO;
using PyramidStax.Modes.Menu.Hud;

namespace PyramidStax.Modes.Stax
{
    public class StaxGame : Mode
    {
        public Deck Deck { get; private set; }
        public Stack[] DiscardStacks { get; private set; }
        public Stack SelectedDiscardStack { get; set; }
        public Stack TrashStack { get; private set; }

        public Player PlayerA { get; private set; }
        public Player PlayerB { get; private set; }
        public Player CurrentPlayer { get { return currentPlayer; } }
        Player currentPlayer;
        public bool DiscardStacksOrdered { get; set; }

        public GameSettings GameSettings { get { return Main.Instance.GameSettings; } }

        public void InitializeNextEnter()
        {
            isInitialized = false;
        }

        protected override void Initialize()
        {
            Deck = new Deck(1, false, false, new Vector2(375, Globals.CENTER_HEIGHT));
            Deck.CardMover.MoveImmediately = false;
            Deck.CardMover.DefaultWaitTime = Globals.FAST_CARD_WAIT;
            Deck.MainStack.LastCardTaken += new Action(MainStack_LastCardTaken);

            DiscardStacks = new Stack[3];
            for (int i = 0; i < DiscardStacks.Length; i++)
            {
                DiscardStacks[i] = new Stack(Deck)
                {
                    Position = new Vector2(75 + i * 105, Globals.CENTER_HEIGHT),
                    Orientation = MathHelper.PiOver2,
                    PositionOffest = new Vector2(3, 2),
                    OrientationOffset = 0.05f,
                    Orderly = false,
                    Hidden = false,
                };
                DiscardStacks[i].AddCard(Deck.TakeCard());
            }

            TrashStack = new Stack(Deck)
            {
                Position = new Vector2(480, Globals.CENTER_HEIGHT),
                PositionOffest = new Vector2(15, 15),
                OrientationOffset = MathHelper.Pi,
                Orderly = false,
                Hidden = false,
            };

            if (GameSettings.IsStaxPlayerAHuman)
                PlayerA = new HumanPlayer(this, true);
            else
                PlayerA = new AiPlayer(this, true);

            if (GameSettings.IsStaxPlayerBHuman)
                PlayerB = new HumanPlayer(this, false);
            else
                PlayerB = new AiPlayer(this, false);

            PlayerA.Opponent = PlayerB;
            PlayerB.Opponent = PlayerA;
            currentPlayer = PlayerA;

            Deck.CardMover.DefaultWaitTime = Globals.SLOW_CARD_WAIT;

            Deck.CardMover.AddCardEvent(null, (c) =>
            {
                currentPlayer.TurnStarts();
            }, 1f);
            //for (int i = 0; i < 5; i++)
            //    PlayerA.HandCards.AddCard(Deck.TakeCard());
        }

        protected override void Enter()
        {
        }

        void MainStack_LastCardTaken()
        {
            while (TrashStack.CardsLeft > 0)
                Deck.MainStack.AddCard(TrashStack.TakeCard());

            if (Deck.CardsLeft == 0) // no trash
            {
                foreach (Stack s in DiscardStacks)
                {
                    for (int i = 0; i < s.CardsLeft - 1; i++)
                    {
                        Deck.MainStack.AddCard(s.Cards[i]);
                        s.TakeCard(s.Cards[i]);
                    }
                }
            }

            Deck.MainStack.Shuffle();
            Deck.MainStack.MakeOrderly();
        }

        protected override void Leave()
        {

        }

        protected override void Update(GameTime gameTime, InputState inputState)
        {
            float seconds = gameTime.GetElapsedSeconds();
            Deck.Update(seconds);

            currentPlayer.Update(seconds, inputState);
            
        }

        public void EndOfTurn(Player lastPlayer)
        {
            if (lastPlayer != currentPlayer)
                return;

            DiscardStacksOrdered = false;

            Deck.CardMover.AddCardEvent(null, (c) =>
                {
                    currentPlayer.TurnEnds();
                    if (currentPlayer.Stax.CardsLeft == 0)
                    {
                        DeclareWinner(currentPlayer);
                    }
                    else
                    {
                        currentPlayer = currentPlayer.Opponent;
                        currentPlayer.TurnStarts();
                    }
                }, 1, true);
        }

        private void DeclareWinner(Player winner)
        {
            StaxEndScreen screen = Proxy.GetMode<StaxEndScreen>();
            screen.SetWinner(winner == PlayerA);
            screen.BackgroundMode = this;
            Proxy.SetMode<StaxEndScreen>();
        }


        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Prepare();

            TrashStack.Draw(spriteBatch);
            foreach (Stack s in DiscardStacks)
            {
                if (s == SelectedDiscardStack)
                    continue;

                s.Draw(spriteBatch);
            }

            Deck.MainStack.Draw(spriteBatch);


            PlayerA.Draw(spriteBatch);
            PlayerB.Draw(spriteBatch);


            if (SelectedDiscardStack != null)
                SelectedDiscardStack.Draw(spriteBatch);

            Deck.DrawMovingCards(spriteBatch);

            currentPlayer.DrawOverlays(spriteBatch);
            spriteBatch.End();
        }
    }
}
