﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PyramidStax.Cards;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Util.Delegation;
using Sunbow.Util.IO;

namespace PyramidStax.Modes.Stax
{
    public class Player
    {
        public StaxStack Stax { get; private set; }
        public Stack HandCards { get; private set; }

        public Player Opponent { get; set; }

        public StaxGame Game { get; private set; }

        public bool IsCurrentPlayer { get { return Game.CurrentPlayer == this; } }

        protected bool handCardsFanedOut;
        protected bool isUpperSide;

        public Player(StaxGame game, bool upperSide)
        {
            this.Game = game;
            this.isUpperSide = upperSide;

            Stax = new StaxStack(game.Deck);
            PersistHelper.LoadPyramidStack(game.GameSettings.CurrentLevelData, game.Deck, Stax);
            Stax.UpdateBlockings();

            if(upperSide)
                Stax.Transform(new Vector2(240, Globals.CENTER_HEIGHT - 120), MathHelper.Pi);
            else
                Stax.Transform(new Vector2(240, Globals.CENTER_HEIGHT + 120), 0f);

            HandCards = new Stack(game.Deck)
            {
                Position = new Vector2(430, (upperSide) ? Globals.AD_HEIGHT + 50 : 750),
                Orientation = (isUpperSide) ? MathHelper.Pi : 0,
                Orderly = true,
                Hidden = true,
                OrientationOffset = -0.2f,
            };

        }

        public void AddHandcard(Card card)
        {
            HandCards.AddCard(card);
            Fan();

           //Game.Deck.CardMover.AddCardEvent(null, (c) => { ; }, 0.5f);
        }

        protected void FanHandcards()
        {
            using (new CardMoveTimer(Game.Deck.CardMover, true))
            {
                handCardsFanedOut = !handCardsFanedOut;
                Fan();
            }
        }
        private void Fan()
        {
            if (handCardsFanedOut)
            {
                int sign = (isUpperSide) ? +1 : -1;

                HandCards.FanOut(new Vector2(240, Globals.CENTER_HEIGHT - sign * 500), sign * MathHelper.PiOver2, 200, 50, 8);
                HandCards.ShowAll();
            }
            else
            {
                HandCards.HideAll();
                HandCards.MakeOrderly();
            }

        }

        public virtual void Update(float seconds, InputState inputState)
        {

        }
        public virtual void TurnStarts()
        {
            Game.SelectedDiscardStack = null;
            AddHandcard(Game.Deck.TakeCard());
            Stax.HidePlaceHolders();
        }

        public void TurnEnds()
        {
            if (HandCards.CardsLeft > 5)
            {
                using (new CardMoveTimer(Game.Deck.CardMover, true))
                {
                    while (HandCards.CardsLeft >= 5)
                    {
                        Card c = HandCards.Cards[Sunbow.Util.Random.Next(HandCards.CardsLeft)];
                        HandCards.TakeCard(c);
                        Game.TrashStack.AddCard(c);
                    }
                }

                Fan();
            }
        }

        public void Draw(SpriteBatch batch)
        {
            Stax.Draw(batch);
            HandCards.Draw(batch);

            if(this != Game.CurrentPlayer)
                DrawHandOverlay(batch, 0.75f);
        }


        public virtual void DrawOverlays(SpriteBatch batch)
        {
            DrawHandOverlay(batch, 1);
        }

        public void DrawHandOverlay(SpriteBatch batch, float scale)
        {
            float rotation =  (isUpperSide) ? MathHelper.Pi : 0;
            Color color = (HandCards.CardsLeft < 5) ? Color.White : ((HandCards.CardsLeft == 5) ? Color.Wheat : Color.Goldenrod);

            Assets.GuiSheet.Draw(batch, Assets.HandFrame, HandCards.Position, rotation, scale, color);

            color = (HandCards.CardsLeft < 5) ? Color.White : Color.Black;
            string s = HandCards.CardsLeft.ToString();
            Vector2 offset = (isUpperSide) ? new Vector2(0, -10) : new Vector2(0, 10);

            batch.DrawString(Assets.FontMedium, s, HandCards.Position + offset, color, rotation, 0.5f * Assets.FontMedium.MeasureString(s), scale, SpriteEffects.None, 0);
        }

    }
}
