﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.Delegation;
using Sunbow.Util;
using PyramidStax.Modes.Stax.Actions;
using Microsoft.Xna.Framework;
using PyramidStax.Cards;
using Sunbow.Util.Data;

namespace PyramidStax.Modes.Stax
{
    public class AiPlayer : Player
    {
        static readonly int[] weights = new int[]
        {
            2,          // BurnDiscardStack
            3,          // ChangeStaxCard
            17,         // GrowOpponentStax
            13,         // ShrinkOwnStax
        };

        ProbabilityRandomizer<Actions.Action> probabilityRandomizer = new ProbabilityRandomizer<Actions.Action>();
        ProbabilityRandomizer<Card> handCardRandomizer = new ProbabilityRandomizer<Card>();

        Actions.Action[] actions;

        public AiPlayer(StaxGame game, bool upperSide)
            : base(game, upperSide)
        {
            actions = new Actions.Action[]
            {
                new BurnDiscardStack(this),
                new ChangeStaxCard(this),
                new GrowOpponentStax(this),
                new ShrinkOwnStax(this),
            };
        }

        public override void Update(float seconds, InputState inputState)
        {
            base.Update(seconds, inputState);
        }

        public override void TurnStarts()
        {
            base.TurnStarts();

#if DEBUG
            //HandCards.FanOut(new Vector2(240, Globals.CENTER_HEIGHT), (this == Game.PlayerA) ? MathHelper.PiOver2 : -MathHelper.PiOver2, 350, -50);
            //HandCards.ShowAll();
#endif
            Game.Deck.CardMover.AddCardEvent(null, (c) =>
                {

                    probabilityRandomizer.Clear();

                    for (int i = 0; i < actions.Length; i++)
                        probabilityRandomizer.Add(GetWeight(i), actions[i]);

                    Console.WriteLine(probabilityRandomizer.Print());

                    Actions.Action action = probabilityRandomizer.Next();
                    action.PerformAI();

                    Console.WriteLine(string.Format("--> {0}\n", action));

                }, 1f);
        }

        private int GetWeight(int actionIndex)
        {
            // TODO: difficulty

            int val = actions[actionIndex].EvaluateAI() * weights[actionIndex];

            return val * val * val;
        }

        public void DiscardSomeHandcards(Stack blockedStack, params Card[] blockedHandcards)
        {
            handCardRandomizer.Clear();

            foreach (Card c in HandCards.Cards)
            {
                if (blockedHandcards.Contains(c))
                    continue;

                int weight = (HandCards.CardsLeft - blockedHandcards.Length) - 2;

                foreach (Stack s in Game.DiscardStacks)
                {
                    if (s == blockedStack)
                        continue;

                    if (c.Value.IsNeighbour(s.CardOnTop.Value))
                        weight++;
                }

                foreach (Card o in Opponent.Stax.GetCardsOnTop())
                {
                    if (c.Value.IsNeighbour(o.Value))
                        weight--;
                }

                if (weight > 0)
                    handCardRandomizer.Add(weight, c);
            }

            if (handCardRandomizer.Count > 0)
            {
                handCardRandomizer.Add(2, Globals.CardTemplate);

                Card c = handCardRandomizer.Next();

                if (c != Globals.CardTemplate)
                {
                    int[] indices = Globals.GetRandomStackIndices();
                    foreach (int idx in indices)
                    {
                        if (Game.DiscardStacks[idx] == blockedStack)
                            continue;

                        if (c.Value.IsNeighbour(Game.DiscardStacks[idx].CardOnTop.Value))
                        {
                            HandCards.TakeCard(c);
                            Game.DiscardStacks[idx].AddCard(c);

                            System.Diagnostics.Debug.WriteLine(string.Format("Ai: hand \t {0}", c.ToString()));
                            break;
                        }
                    }

                    DiscardSomeHandcards(blockedStack, blockedHandcards);
                }
            }
        } 
    }
}
