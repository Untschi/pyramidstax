﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util;
using Sunbow.Util.Delegation;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using PyramidStax.Cards;
using Microsoft.Xna.Framework.Graphics;
using PyramidStax.Modes.Feedback;

namespace PyramidStax.Modes.Stax
{
    public enum StaxAction
    {
        undefined,
        BurnDiscardStack,
        ChangeStaxCard,
        GrowOpponentStack,
        ShrinkOwnStax,
    }

    public class HumanPlayer : Player
    {
        Card selectedHandCard;
        Card selectedStaxCard;
        Card selectedDiscardCard;
        StaxAction currentAction;

        CardSelectionHighlighter selector = new CardSelectionHighlighter() { SingleCardHighlightMode = true };

        bool discardStaxFanedOut;

        public HumanPlayer(StaxGame game, bool upperSide)
            : base(game, upperSide)
        {
        }

        public override void TurnStarts()
        {
            base.TurnStarts();
            currentAction = StaxAction.undefined;
            selectedHandCard = null;
            selectedStaxCard = null;
            
            discardStaxFanedOut = false; 
        }

        public override void Update(float seconds, InputState inputState)
        {
            base.Update(seconds, inputState);

            selector.Update(seconds);

            Vector2 mousePosition = inputState.CurrentMouseState.GetPosition();

            if (currentAction == StaxAction.undefined
                && inputState.CurrentMouseState.LeftButton == ButtonState.Pressed
                && inputState.LastMouseState.LeftButton == ButtonState.Released)
            {
                using (new CardMoveTimer(Game.Deck.CardMover, true))
                {
                    Card hand, stax;
                    SelectCard(mousePosition, out hand, out stax);
                    HandcardsFan(mousePosition);

                    BurnDiscardStack(mousePosition);
                    ChangeStaxCard(mousePosition);
                    ShrinkOwnStax(mousePosition);
                    GrowOpponentStax(mousePosition);

                    DiscardCard(mousePosition, ref selectedHandCard, HandCards);


                    selectedHandCard = hand;
                    selectedStaxCard = stax;

                    if (hand != null)
                        Opponent.Stax.ShowPlaceholders(selectedHandCard.Value);
                    else
                        Opponent.Stax.HidePlaceHolders();
                }

                if (currentAction != StaxAction.undefined)
                    Game.EndOfTurn(this);
            }
        }

        private void SelectCard(Vector2 mousePosition, out Card handCard, out Card staxCard)
        {
            //selectedHandCard = null;
            //selectedStaxCard = null;
            handCard = null;
            staxCard = null;

            if (handCardsFanedOut)
            {
                foreach (Card c in HandCards.Cards)
                {
                    if (c.Bounds.IsPointInside(mousePosition))
                    {
                        handCard = c;
                        selector.Highlight(c);
                        return;
                    }
                }
            }

            foreach (Card c in Stax.GetCardsOnTop())
            {
                if (!c.IsPlaceholder() && c.Bounds.IsPointInside(mousePosition))
                {
                    staxCard = c;
                    selector.Highlight(c);
                    return;
                }
            }

            selector.Clear();
        }


        private void HandcardsFan(Vector2 mousePosition)
        {
            if (HandCards.Bounds.IsPointInside(mousePosition))
            {
                FanHandcards();
            }
        }

        private bool DiscardCard(Vector2 mousePosition, ref Card card, CardCollection collection)
        {
            if (card == null)
                return false;

            foreach (Stack s in Game.DiscardStacks)
            {
                if (s.Bounds.IsPointInside(mousePosition))
                {
                    if (card.Value.IsNeighbour(s.CardOnTop.Value))
                    {
                        s.AddCard(card);
                        collection.TakeCard(card);

                        System.Diagnostics.Debug.WriteLine(string.Format("Hu: {1} \t {0}", card.ToString(), (collection == HandCards) ? "hand" : "stax"));
                        card = null;
                        
                        return true;
                    }
                    break;
                }
            }
            return false;
        }

        private void GrowOpponentStax(Vector2 mousePosition)
        {
            if (selectedHandCard == null || currentAction != StaxAction.undefined)
                return;

            Card placeholder = null;
            foreach (Card c in Opponent.Stax.GetCardsOnTop())
            {
                if (!c.IsPlaceholder())
                    continue;

                if (c.Bounds.IsPointInside(mousePosition))
                {
                    bool allowed = true;
                    int cnt = 0;
                    foreach (Card b in Opponent.Stax.GetCardsBeneath(c))
                    {
                        if(b.IsPlaceholder())
                            continue;

                        cnt++;
                        if (!selectedHandCard.Value.IsNeighbour(b.Value))
                        {
                            allowed = false;
                            break;
                        }
                    }

                    if (allowed && cnt > 0)
                    {
                        placeholder = c;
                    }
                    break;
                }
            }

            if (placeholder != null)
            {
                HandCards.TakeCard(selectedHandCard);
                Opponent.Stax.AddCard(selectedHandCard);

                selectedHandCard.Position = placeholder.Position;
                selectedHandCard.Rotation = placeholder.Rotation;

                //Opponent.Stax.HidePlaceHolders();

                currentAction = StaxAction.GrowOpponentStack;
            }
        }

        private void ChangeStaxCard(Vector2 mousePosition)
        {
            if (selectedHandCard == null || currentAction != StaxAction.undefined)
                return;

            Card cardToRemove = null;
            foreach(Card c in Stax.GetCardsOnTop())
            {
                if (c.IsPlaceholder())
                    continue;

                if (c.Bounds.IsPointInside(mousePosition))
                {
                    cardToRemove = c;
                    break;
                }
            }

            if(cardToRemove != null)
            {
                selectedHandCard.Position = cardToRemove.Position;
                selectedHandCard.Rotation = cardToRemove.Rotation;

                Stax.TakeCard(cardToRemove);
                Game.TrashStack.AddCard(cardToRemove);

                HandCards.TakeCard(selectedHandCard);
                Stax.AddCard(selectedHandCard);

                currentAction = StaxAction.ChangeStaxCard;
            }
        }
        private void ShrinkOwnStax(Vector2 mousePosition)
        {
            if (selectedStaxCard == null || currentAction != StaxAction.undefined)
                return;

            if (DiscardCard(mousePosition, ref selectedStaxCard, Stax))
            {
                currentAction = StaxAction.ShrinkOwnStax;

                Stax.UpdateBlockings();
            }
        }

        private void BurnDiscardStack(Vector2 mousePosition)
        {
            if (selectedHandCard != null || selectedStaxCard != null || currentAction != StaxAction.undefined)
                return;

            Stack selectedStack = Game.SelectedDiscardStack;


            if (discardStaxFanedOut && selectedStack != null)
            {
                Card selection = null;
                foreach (Card c in selectedStack.Cards)
                {
                    if (c.Bounds.IsPointInside(mousePosition))
                    {
                        selection = c;
                        break;
                    }
                }

                if (selection != null)
                {
                    if (selectedDiscardCard == selection)
                    {
                        selectedStack.TakeCard(selection);
                        AddHandcard(selection);

                        while (selectedStack.CardsLeft > 0)
                            Game.TrashStack.AddCard(selectedStack.TakeCard());

                        selectedStack.AddCard(Game.Deck.TakeCard());

                        currentAction = StaxAction.BurnDiscardStack;
                        selectedDiscardCard = null;
                    }
                    else
                    {
                        selectedDiscardCard = selection;
                        selector.Highlight(selection);
                    }
                }
                
                return;
            }

            selectedStack = null;
            foreach (Stack s in Game.DiscardStacks)
            {
                if (s.Bounds.IsPointInside(mousePosition))
                {
                    selectedStack = s;
                    break;
                }
            }

            if (selectedStack != null)
            {
                if (!Game.DiscardStacksOrdered)
                {
                    foreach (Stack s in Game.DiscardStacks)
                    {
                        s.MakeOrderly(new Vector2(0.2f, -2f), 0);
                    }
                    Game.DiscardStacksOrdered = true;
                }
                else
                {
                    Card c = selectedStack.TakeCard();
                    Game.TrashStack.AddCard(c);

                    if (selectedStack.CardsLeft > 0)
                    {
                        float center = Globals.CENTER_HEIGHT + ((isUpperSide) ? -600 : +600);
                        float rotation = (isUpperSide) ? +MathHelper.PiOver2 : -MathHelper.PiOver2;

                        selectedStack.FanOut(new Vector2(240, center), rotation, 600, 50, 8);

                        discardStaxFanedOut = true;
                    }
                    else
                    {
                        selectedStack.AddCard(Game.Deck.TakeCard());
                        currentAction = StaxAction.BurnDiscardStack;
                    }
                }

                Game.SelectedDiscardStack = selectedStack;
            }
        }

        public override void DrawOverlays(SpriteBatch batch)
        {
            selector.Draw(batch);

            base.DrawOverlays(batch);

            float rotation = (isUpperSide) ? MathHelper.Pi : 0;
            
            if (selectedHandCard != null)
            {
                foreach (Card c in Stax.GetCardsOnTop())
                    Assets.GuiSheet.Draw(batch, Assets.StaxSwitchFrame, c.Position, rotation, 1);

                foreach (Stack s in Game.DiscardStacks)
                {
                    if (s.CardOnTop.Value.IsNeighbour(selectedHandCard.Value))
                        Assets.GuiSheet.Draw(batch, Assets.RoundCountFrame, s.Position, rotation, 1);
                }

                foreach (Card c in Opponent.Stax.GetCardsOnTop())
                {
                    if(c.IsPlaceholder())
                        Assets.GuiSheet.Draw(batch, Assets.StaxExpandFrame, c.Position, rotation, 1);
                }
            }

            if (selectedStaxCard != null)
            {
                foreach (Stack s in Game.DiscardStacks)
                {
                    if (s.CardOnTop.Value.IsNeighbour(selectedStaxCard.Value))
                        Assets.GuiSheet.Draw(batch, Assets.StaxShrinkFrame, s.Position, rotation, 1);
                }
            }

            if (Game.DiscardStacksOrdered && !discardStaxFanedOut)
            {
                foreach (Stack s in Game.DiscardStacks)
                    Assets.GuiSheet.Draw(batch, Assets.StaxBurnFrame, s.Position, rotation, 1);
            }
        }
        
    }
}
