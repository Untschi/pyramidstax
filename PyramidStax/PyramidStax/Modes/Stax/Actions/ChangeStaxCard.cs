﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PyramidStax.Cards;
using Sunbow.Util;
using Sunbow.Util.Data;

namespace PyramidStax.Modes.Stax.Actions
{
    public class ChangeStaxCard : Action
    {
        ProbabilityRandomizer<Card> probRandHand = new ProbabilityRandomizer<Card>();
        ProbabilityRandomizer<Card> probRandStax = new ProbabilityRandomizer<Card>();

        public ChangeStaxCard(AiPlayer player)
            : base(player)
        {
        }
        public override int EvaluateAI()
        {
            int probabilityHand = 0;
            foreach (Card c in handCards.Cards)
            {
                int val = EvaluateHandcard(c);
                if (probabilityHand < val)
                    probabilityHand = val;
            }

            int probabilityStax = int.MaxValue;
            foreach (Card c in ownStax.GetCardsOnTop())
            {
                int val = GetStaxCardDistance(c);
                if (probabilityStax > val)
                    probabilityStax = val;
            }

            return probabilityHand * probabilityStax;
        }

        public override void PerformAI()
        {
            probRandHand.Clear();
            probRandStax.Clear();

            foreach (Card c in handCards.Cards)
            {
                int val = EvaluateHandcard(c);
                if (val != 0)
                    probRandHand.Add(val, c);
            }
            foreach (Card c in ownStax.GetCardsOnTop())
            {
                int val = GetStaxCardDistance(c);
                if (val != 0)
                    probRandStax.Add(val, c);
            }

            Card handCard = probRandHand.Next();
            Card staxCard = probRandStax.Next();

            owner.DiscardSomeHandcards(null, handCard);

            handCards.TakeCard(handCard);
            handCard.Position = staxCard.Position;
            handCard.Rotation = staxCard.Rotation;


            deck.CardMover.AddCardEvent(null, (nil) =>
                {
                    using (new CardMoveTimer(deck.CardMover, true))
                    {
                        ownStax.TakeCard(staxCard);
                        trash.AddCard(staxCard);
                        ownStax.AddCard(handCard);
                    }
                });
            base.PerformAI();
        }

        private int EvaluateHandcard(Card card)
        {
            int tmp = 0;
            foreach (Stack s in discardStacks)
            {
                if (card.Value.IsNeighbour(s.CardOnTop.Value))
                    tmp += 1;
            }
            return tmp;
        }
    }
}
