﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PyramidStax.Cards;
using Sunbow.Util;
using Sunbow.Util.Data;


namespace PyramidStax.Modes.Stax.Actions
{
    public class ShrinkOwnStax : Action
    {
        ProbabilityRandomizer<Card> probRand = new ProbabilityRandomizer<Card>();

        public ShrinkOwnStax(AiPlayer player)
            : base(player)
        {
        }

        public override int EvaluateAI()
        {
            int probability = 0;
            foreach (Card c in ownStax.GetCardsOnTop())
            {
                probability += Evaluate(c);
            }
            return probability;
        }

        public override void PerformAI()
        {
            probRand.Clear();

            foreach (Card c in ownStax.GetCardsOnTop())
            {
                int prob = Evaluate(c);
                if (prob != 0)
                    probRand.Add(prob, c);
            }

            Card card = probRand.Next();
            List<Card> handCardsToUse = new List<Card>();
            Stack stack = null;
            Evaluate(card, handCardsToUse, ref stack);

            owner.DiscardSomeHandcards(stack, handCardsToUse.ToArray());

            foreach (Card c in handCardsToUse)
            {
                handCards.TakeCard(c);
                stack.AddCard(c);
            }

            deck.CardMover.AddCardEvent(card, (c) =>
               {
                   using (new CardMoveTimer(deck.CardMover, true))
                   {
                       ownStax.TakeCard(c);
                       stack.AddCard(c);
                   }
               }, 1);
            System.Diagnostics.Debug.WriteLine(string.Format("Ai: stax \t {0}", card.ToString())); 

            base.PerformAI();

            owner.Stax.UpdateBlockings();
        }

        private int Evaluate(Card card)
        {
            List<Card> tmp = new List<Card>();
            Stack s = null;
            return Evaluate(card, tmp, ref s);
        }

        private int Evaluate(Card card, List<Card> handCardsToUse, ref Stack stack)
        {
            int probability = 0;
            int[] indices = Globals.GetRandomStackIndices();

            foreach (int idx in indices)
            {
                stack = discardStacks[idx];
                handCardsToUse.Clear();

                if (card.Value.IsNeighbour(stack.CardOnTop.Value))
                {
                    probability = 1;
                    break;
                }
                else
                {
                    List<Card> tmpHand = handCards.Cards.ToList();
                    Card topCard = stack.CardOnTop;
                    bool match = true;
                    while (match)
                    {
                        match = false;
                        for (int i = 0; i < tmpHand.Count; i++)
                        {
                            if (tmpHand[i].Value.IsNeighbour(topCard.Value))
                            {
                                topCard = tmpHand[i];
                                handCardsToUse.Add(tmpHand[i]);
                                tmpHand.RemoveAt(i);
                                match = true;
                                break;
                            }
                        }

                        if (match)
                        {
                            if (card.Value.IsNeighbour(topCard.Value))
                            {
                                probability = Math.Max(1, handCards.CardsLeft - (Globals.MAX_HAND_CARDS - 3));
                                break;
                            }
                        }
                    }
                    if (match)
                        break;
                }
            }

            if (probability > 0)
            {
                // check own stax for neighbours (+)
                foreach (Card c in ownStax.GetCardsOnTopWhenCardIsRemoved(card))
                {
                    if(card.Value.IsNeighbour(c.Value))
                        probability += 1;
                }

                // check opponent stax for neighbours (-) or same card value (+)
                foreach (Card c in opponentStax.GetCardsOnTop())
                {
                    if (c.Value == card.Value)
                        probability += 1;
                    else if (c.Value.IsNeighbour(card.Value))
                        probability -= 1;
                }

                if (probability <= 0)
                    probability = 1;
            }

            return probability;
        }

    }
}
