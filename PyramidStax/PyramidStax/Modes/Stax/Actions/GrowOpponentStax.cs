﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PyramidStax.Cards;
using Sunbow.Util;
using Sunbow.Util.Data;

namespace PyramidStax.Modes.Stax.Actions
{
    public class GrowOpponentStax : Action
    {
        ProbabilityRandomizer<Card> probRand = new ProbabilityRandomizer<Card>();

        public GrowOpponentStax(AiPlayer player)
            : base(player)
        {
        }
        public override int EvaluateAI()
        {
            opponentStax.ShowPlaceholders();

            int min = 0;
            foreach (Card c in handCards.Cards)
            {
                int val = Evaluate(c);
                if (min < val)
                    min = val;
            }

            opponentStax.HidePlaceHolders();

            if(min > 0)
                min = Math.Min(1, (ownStax.CardsLeft - opponentStax.CardsLeft) * min);


            return min;
        }

        public override void PerformAI()
        {
            probRand.Clear();

            opponentStax.ShowPlaceholders();

            foreach (Card c in handCards.Cards)
            {
                int prob = Evaluate(c);
                if (prob != 0)
                    probRand.Add(prob, c);
            }

            Card card = probRand.Next();
            StaxStack.PositionInfo posInfo = new StaxStack.PositionInfo();
            Evaluate(card, ref posInfo);

            owner.DiscardSomeHandcards(null, card);

            handCards.TakeCard(card);
            opponentStax.AddCard(card);
            card.Position = posInfo.Position;
            card.Rotation = posInfo.Rotation;

            opponentStax.HidePlaceHolders();

            base.PerformAI();
        }

        private int Evaluate(Card card)
        {
            StaxStack.PositionInfo posInfo = new StaxStack.PositionInfo();
            return Evaluate(card, ref posInfo);
        }

        private int Evaluate(Card card, ref StaxStack.PositionInfo posInfo)
        {
            int propability = 0;
            foreach (Card p in opponentStax.GetCardsOnTop())
            {
                if (p.IsPlaceholder())
                {
                    int matches = 0;
                    foreach (Card c in opponentStax.GetCardsBeneath(p))
                    {
                        if (!c.IsPlaceholder())
                        {
                            if (card.Value.IsNeighbour(c.Value))
                            {
                                matches++;
                            }
                            else
                            {
                                matches = 0;
                                break;
                            }
                        }
                    }

                    if (propability < matches)
                    {
                        propability = matches;
                        posInfo = new StaxStack.PositionInfo() { Position = p.Position, Rotation = p.Rotation.Radians };
                    }
                }
            }
            return propability;
        }

    }
}
