﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PyramidStax.Cards;

namespace PyramidStax.Modes.Stax.Actions
{
    public abstract class Action
    {
        // shortcuts
        protected AiPlayer owner;
        protected Stack handCards { get { return owner.HandCards; } }
        protected StaxStack ownStax { get { return owner.Stax; } }
        protected StaxStack opponentStax { get { return owner.Opponent.Stax; } }
        protected Stack[] discardStacks { get { return owner.Game.DiscardStacks; } }
        protected Stack trash { get { return owner.Game.TrashStack; } }
        protected Deck deck { get { return owner.Game.Deck; } }

        protected Action(AiPlayer player)
        {
            this.owner = player;
        }

        public abstract int EvaluateAI();
        public virtual void PerformAI()
        {
            owner.Game.EndOfTurn(owner);
        }

        protected int GetCardDistance(Card cardA, Card cardB)
        {
            if (cardB.Value.IsNeighbour(cardA.Value))
                return 0;

            int wayA = Math.Abs((int)cardB.Value - (int)cardA.Value);
            int wayB = Math.Abs((int)cardB.Value - ((int)CardValue._Count - (int)cardA.Value));

            return (wayA < wayB) ? wayA : wayB;
        }
        protected int GetStaxCardDistance(Card card, PyramidStack stax)
        {
            int smallestDifference = int.MaxValue;
            foreach (Card c in stax.GetCardsOnTop())
            {
                int distance = GetCardDistance(card, c);
                if (smallestDifference > distance)
                    smallestDifference = distance;
            }
            return smallestDifference;
        }
        protected int GetStaxCardDistance(Card card)
        {
            int smallestDifference = int.MaxValue;
            foreach (Stack s in discardStacks)
            {
                int distance = GetCardDistance(card, s.CardOnTop);
                if (smallestDifference > distance)
                    smallestDifference = distance;
            }

            return smallestDifference;
        }

        protected int FittingCardsCount(Stack discardStack, CardCollection cards, params Card[] blockedHandcards)
        {
            int val = 0;
            foreach (Card c in cards.Cards)
            {
                if (blockedHandcards.Contains(c))
                    continue;

                if (c.Value.IsNeighbour(discardStack.CardOnTop.Value))
                {
                    val++;
                }
            }
            return val;
        }

    }
}
