﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using PyramidStax.Cards;
using Sunbow.Util;
using Sunbow.Util.Data;

namespace PyramidStax.Modes.Stax.Actions
{
    public class BurnDiscardStack : Action
    {
        ProbabilityRandomizer<Stack> stackProbRand = new ProbabilityRandomizer<Stack>();
        ProbabilityRandomizer<Card> cardProbRand = new ProbabilityRandomizer<Card>();

        public BurnDiscardStack(AiPlayer player)
            : base(player)
        {
        }
        public override int EvaluateAI()
        {
            int prob = 0;
            foreach (Stack s in discardStacks)
            {
                int val = Evaluate(s);
                if (prob < val)
                    prob = val;
            }
            return prob;
        }
        public override void PerformAI()
        {
            using (new CardMoveTimer(deck.CardMover, true))
            {
                stackProbRand.Clear();
                cardProbRand.Clear();

                foreach (Stack s in discardStacks)
                {
                    int val = Evaluate(s);
                    if (val > 0)
                        stackProbRand.Add(val, s);
                }

                Stack stack = stackProbRand.Next();

                owner.DiscardSomeHandcards(stack);

                foreach (Card c in stack.Cards)
                {
                    if (c == stack.CardOnTop)
                        continue;

                    int val = (int)CardValue._Count - GetStaxCardDistance(c, ownStax);
                    cardProbRand.Add(val * val * val, c);
                }
                if (stack.CardsLeft > 1)
                {
                    Card card = cardProbRand.Next();

                    stack.TakeCard(card);
                    owner.AddHandcard(card);

                    System.Diagnostics.Debug.WriteLine("Ai: burn \t {0}", card.ToString());
                }
                foreach (Card c in stack.Cards)
                    trash.AddCard(c);

                stack.Clear();
                stack.AddCard(deck.TakeCard());
            }

            base.PerformAI();
        }

        private int Evaluate(Stack s)
        {
            int probability = 1;

            probability += 3 * FittingCardsCount(s, opponentStax);
            probability -= 2 * FittingCardsCount(s, ownStax);

            return Math.Max(1, probability);
        }

    }
}
