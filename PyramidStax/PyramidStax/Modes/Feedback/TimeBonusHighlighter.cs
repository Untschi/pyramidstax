﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PyramidStax.Modes.Feedback
{
    public class TimeBonusHighlighter : SpecialHighlighter
    {

        public TimeBonusHighlighter(SpecialHighlightManager manager)
            : base(manager, Assets.SpecialHighlightTimeBonus)
        {
        }

    }
}
