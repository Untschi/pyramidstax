using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.Xna.Framework.Graphics;
using PyramidStax.Cards;
using Sunbow.Util.Manipulation;
using Sunbow.Util.Data;
using Microsoft.Xna.Framework;

namespace PyramidStax.Modes.Feedback
{
    public class CardSelectionHighlighter
    {
        static readonly Color HIGHLIGHT_COLOR = Color.Green;
        static readonly Color LOCK_COLOR = Color.DarkRed;

        const float UNSELECT_DURATION = 0.25f;

        public bool SingleCardHighlightMode { get; set; }
      //  public List<Card> SelectedCards { get { return selectedCards; } }
        List<Card> selectedCards = new List<Card>();

        public Queue<Tuple<Card, SingleAnimator, Color>> lockHighlighters = new Queue<Tuple<Card, SingleAnimator, Color>>();

        public void Highlight(Card card)
        {
            if (SingleCardHighlightMode)
                Clear();

            selectedCards.Add(card);
        }

        public void RemoveHighlight(Card card)
        {
            if (selectedCards.Remove(card))
                FadeHighlight(card, UNSELECT_DURATION, false);
        }


        public void FadeHighlight(Card card)
        {
            FadeHighlight(card, 1.5f, true);
        }
        public void FadeHighlight(Card card, float duration, bool isLocked)
        {
            SinusSingleAnimator animator = new SinusSingleAnimator(duration);
            animator.EndOfAnimation += animator_EndOfAnimation;

            lockHighlighters.Enqueue(new Tuple<Card,SingleAnimator, Color>(card, animator, (isLocked) ? LOCK_COLOR : HIGHLIGHT_COLOR));
        }

        void animator_EndOfAnimation(object sender, EventArgs e)
        {
            lockHighlighters.Dequeue();
        }

        public void Update(float seconds)
        {
            for(int i = 0; i < lockHighlighters.Count; i++)
            {
                lockHighlighters.ElementAt(i).Item2.Update(seconds);
            }
        }

        public void Draw(SpriteBatch batch)
        {
            foreach (Card c in selectedCards)
            {
                Assets.Cards.Draw(batch, Assets.CardFrames.CalculateFrame(Card.HIGHLIGHTER_IDX), c.Position, c.Rotation.ClockwiseRadians, c.Scale, HIGHLIGHT_COLOR);
            }

            foreach (var l in lockHighlighters)
            {
                Card c = l.Item1;
                float alpha = 1 - l.Item2;

                Assets.Cards.Draw(batch, Assets.CardFrames.CalculateFrame(Card.HIGHLIGHTER_IDX), c.Position, c.Rotation.ClockwiseRadians, c.Scale, l.Item3 * alpha);
            }
        }



        public void Clear()
        {
            foreach (Card c in selectedCards)
            {
                FadeHighlight(c, UNSELECT_DURATION, false);
            }

            selectedCards.Clear();
        }
    }
}
