﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.Sprites;
using Sunbow.Util.Manipulation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PyramidStax.Modes.Feedback
{
    public abstract class SpecialHighlighter
    {
        protected const float MOVE_DURATION = 0.75f;
        const float SCALE = 320;
        const float OFFSET = SCALE - 240;

        public Frame Frame { get; private set; }

        SinusSingleAnimator entranceAnimation = new SinusSingleAnimator(0, -MathHelper.PiOver2, SCALE, 2 * SCALE - OFFSET, MOVE_DURATION, AnimationRepeatLogic.Pause);
        SinusSingleAnimator exitAnimation = new SinusSingleAnimator(MathHelper.PiOver2, MathHelper.PiOver2, SCALE, -OFFSET, MOVE_DURATION, AnimationRepeatLogic.Pause);
        protected SinusSingleAnimator currentAnimation;

        protected SpecialHighlightManager manager;

        protected SpecialHighlighter(SpecialHighlightManager manager, Frame frame)
        {
            this.manager = manager;
            this.Frame = frame;

            entranceAnimation.EndOfAnimation += (o, a) => AnimationMid();
            exitAnimation.EndOfAnimation += exitAnimation_EndOfAnimation;
        }

        protected virtual void AnimationMid()
        {
            exitAnimation.Play();
            currentAnimation = exitAnimation;
        }

        void exitAnimation_EndOfAnimation(object sender, EventArgs e)
        {
            manager.HighlightingFinished();
        }

        public void Start()
        {
            exitAnimation.Reset();
            exitAnimation.Pause();
            entranceAnimation.Reset();
            entranceAnimation.Play();

            currentAnimation = entranceAnimation;
        }

        public virtual void Update(float seconds)
        {
            currentAnimation.Update(seconds);
        }

        public virtual void Draw(SpriteBatch batch)
        {
            Assets.GuiSheet.Draw(batch, Frame, new Vector2(currentAnimation, Globals.TIMER_POS_Y), 0, 1);
        }

    }
}
