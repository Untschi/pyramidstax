﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.Sprites;
using Sunbow.Util.Manipulation;
using Microsoft.Xna.Framework.Graphics;
using PyramidStax.Cards;
using Microsoft.Xna.Framework;
using Shapes.Geometry;

namespace PyramidStax.Modes.Feedback
{
    public abstract class ExtraCardHighlighter : SpecialHighlighter
    {
        protected Line movePath;

        Frame cardFrame;
        SinusSingleAnimator cardMover = new SinusSingleAnimator(0.5f * MOVE_DURATION - 0.05f);

        protected ExtraCardHighlighter(SpecialHighlightManager manager, Frame highlightFrame, Frame cardFrame)
            : base(manager, highlightFrame)
        {
            this.cardFrame = cardFrame;
            cardMover.Pause();
            cardMover.EndOfAnimation += (o, a) => AddCardToStack();

            movePath = new Line(new Vector2(240, Globals.TIMER_POS_Y - 20), new Vector2(240, Globals.TIMER_POS_Y - 160));
        }

        void AddCardToStack()
        {
            Card card;
            CardMover mover;
            Stack destination;
            GetData(out card, out mover, out destination);

            using (new TempValue<float>(mover.DefaultWaitTime, Globals.SLOW_CARD_WAIT, (val) => mover.DefaultWaitTime = val))
            {
                using (new TempValue<bool>(Card.ForceTransform, true, (val) => Card.ForceTransform = val))
                {
                    card.Position = movePath.EndPoint;
                    card.IsHidden = false;

                }

                destination.AddCard(card);

            }
        }
        protected abstract void GetData(out Card card, out CardMover mover, out Stack destination);


        protected override void AnimationMid()
        {
            cardMover.Play();
            base.AnimationMid();
        }

        public override void Update(float seconds)
        {
            cardMover.Update(seconds);

            base.Update(seconds);
        }

        public override void Draw(SpriteBatch batch)
        {
            if (cardMover.IsPaused)
            {
                if(currentAnimation > 240)
                    Assets.Cards.Draw(batch, Assets.CardFrames.CalculateFrame(Card.CARD_BACK_IDX), new Vector2(base.currentAnimation, movePath.StartPoint.Y), 0, 1);
            }
            else
            {
                float scale = Math.Abs(2 * (cardMover - 0.5f));
                Frame frame = (cardMover < 0.5f) ? Assets.CardFrames.CalculateFrame(Card.CARD_BACK_IDX) : cardFrame;
                Vector2 pos = movePath.GetPositionFromEdgePath(cardMover);

                Assets.Cards.Draw(batch, frame, pos, 0, new Vector2(1, scale));
            }

            base.Draw(batch);
        }
    }
}
