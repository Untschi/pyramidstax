﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using PyramidStax.Cards;
using Microsoft.Xna.Framework;

namespace PyramidStax.Modes.Feedback
{
    public class DeflectorHighlighter : SpecialHighlighter
    {
        public CardColor CardColor { get; set; }

        public DeflectorHighlighter(SpecialHighlightManager manager, CardColor color)
            : base(manager, Assets.SpecialHighlightDeflector)
        {
            this.CardColor = color;
        }

        public override void Draw(SpriteBatch batch)
        {
            base.Draw(batch);

            Color color = (CardColor == CardColor.Heart || CardColor == CardColor.Diamonds) ? Color.Red : Color.Black;
            Sunbow.Util.Sprites.Frame frame = Assets.CardColorSymbols.CalculateFrame((int)CardColor);
            Assets.GuiSheet.Draw(batch, frame, new Microsoft.Xna.Framework.Vector2(currentAnimation - 0.5f * frame.SourceRectangle.Width, Globals.TIMER_POS_Y - 20), 0, 1, color);
        }
    }
}
