using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Util.Manipulation;
using Microsoft.Xna.Framework;

namespace PyramidStax.Modes.Feedback
{
    public class SpecialHighlightManager
    {
        const float MOVE_DURATION = 0.2f;

        Queue<SpecialHighlighter> lastSpecials = new Queue<SpecialHighlighter>();

        protected SinusSingleAnimator entranceAnimation = new SinusSingleAnimator(-MathHelper.PiOver2, MathHelper.Pi, 0.5f * Assets.SpecialHighlightBackground.SourceRectangle.Height, 0.5f * Assets.SpecialHighlightBackground.SourceRectangle.Height, MOVE_DURATION, AnimationRepeatLogic.Pause);
        protected SinusSingleAnimator exitAnimation = new SinusSingleAnimator(MathHelper.PiOver2, MathHelper.Pi, 0.5f * Assets.SpecialHighlightBackground.SourceRectangle.Height, 0.5f * Assets.SpecialHighlightBackground.SourceRectangle.Height, MOVE_DURATION, AnimationRepeatLogic.Pause);
        SinusSingleAnimator currentAnimation;

        bool animationReady;

        public SpecialHighlightManager()
        {

            exitAnimation.Pause();

            entranceAnimation.Pause();
            entranceAnimation.EndOfAnimation += entranceAnimation_EndOfAnimation;

            currentAnimation = entranceAnimation;
        }

        void entranceAnimation_EndOfAnimation(object sender, EventArgs e)
        {
            animationReady = true;
            lastSpecials.Peek().Start();
        }


        public void Highlight(SpecialHighlighter highlight)
        {
            lastSpecials.Enqueue(highlight);

            if (!animationReady)
            {
                entranceAnimation.Reset();
                entranceAnimation.Play();
                currentAnimation = entranceAnimation;
            }
        }

        public void Update(float seconds)
        {
            currentAnimation.Update(seconds);
            if(animationReady)
                lastSpecials.Peek().Update(seconds);
        }

        public void Draw(SpriteBatch batch)
        {
            Assets.GuiSheet.Draw(batch, Assets.SpecialHighlightBackground, new Vector2(0, Globals.TIMER_POS_Y - currentAnimation), 0, 1);

            if(animationReady)
                lastSpecials.Peek().Draw(batch);
        }


        internal void HighlightingFinished()
        {
            lastSpecials.Dequeue();

            if (lastSpecials.Count == 0)
            {
                animationReady = false;
                exitAnimation.Reset();
                exitAnimation.Play();
                currentAnimation = exitAnimation;
            }
            else
            {
                lastSpecials.Peek().Start();
            }
        }
    }
}
