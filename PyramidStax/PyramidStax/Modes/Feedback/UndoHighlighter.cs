﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PyramidStax.Cards;
using Sunbow.Util.Manipulation;
using Microsoft.Xna.Framework;

namespace PyramidStax.Modes.Feedback
{
    public class UndoHighlighter : ExtraCardHighlighter
    {
        CardColor color;
        PyramidsSolitairMode gameMode;

        public UndoHighlighter(SpecialHighlightManager manager, PyramidsSolitairMode gameMode, CardColor color)
            : base(manager, Assets.SpecialHighlightUndo,
                    Assets.CardFrames.CalculateFrame((color == CardColor.SpecialCardRed)
                                                    ? Card.UNDO_RED_IDX 
                                                    : Card.UNDO_BLACK_IDX))
        {
            this.color = color;
            this.gameMode = gameMode;
            this.movePath.EndPoint += new Vector2(60, 0);
        }


        protected override void GetData(out Card card, out CardMover mover, out Stack destination)
        {
            card = new Card(gameMode.Deck, color, CardValue.Undo);
            mover = gameMode.Deck.CardMover;
            destination = gameMode.Undos;
        }
    }
}
