﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PyramidStax.Cards;
using Microsoft.Xna.Framework;

namespace PyramidStax.Modes.Feedback
{
    public class JokerHighlighter : ExtraCardHighlighter
    {
        CardColor color;
        Pyramids.PyramidsGame gameMode;

        public JokerHighlighter(SpecialHighlightManager manager, Pyramids.PyramidsGame gameMode, CardColor color)
            : base(manager, Assets.SpecialHighlightJoker,
                    Assets.CardFrames.CalculateFrame((color == CardColor.SpecialCardRed)
                                                    ? Card.JOKER_RED_IDX 
                                                    : Card.JOKER_BLACK_IDX))
        {
            this.color = color;
            this.gameMode = gameMode;
            this.movePath.EndPoint += new Vector2(25, 0);
        }



        protected override void GetData(out Card card, out CardMover mover, out Stack destination)
        {
            card = new Card(gameMode.Deck, color, CardValue.Joker);
            mover = gameMode.Deck.CardMover;
            destination = gameMode.Jokers;
        }

    }
}
