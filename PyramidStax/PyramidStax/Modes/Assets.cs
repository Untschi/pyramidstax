﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.Sprites;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace PyramidStax
{
    public static class Assets
    {
        public static Curve TimeLossCurve { get; private set; }

        public static SpriteSheet Cards { get; private set; }
        public static FixedFrameSet CardFrames { get; private set; }

        //public static SpriteSheet GameSheet { get; private set; }
        public static FixedFrameSet CardColorSymbols { get; private set; }

        public static Frame MenuUpperFrame { get; private set; }
        public static Frame SunbowLogo { get; private set; }
        public static Frame ScorePanel { get; private set; }

        public static Frame GradientColor { get; private set; }
        public static Frame ColorDefault { get; private set; }
        public static Frame ColorDark { get; private set; }

        public static Frame TurnFrameHighlight { get; private set; }
        public static Frame TurnFrameNormal { get; private set; }
        public static Frame TurnBackgroundFrame { get; private set; }
        public static Frame TimeBorderFrame { get; private set; }
        public static Frame TimeInnerFrame { get; private set; }

        public static Frame SpecialHighlightDeflector { get; private set; }
        public static Frame SpecialHighlightTimeBonus { get; private set; }
        public static Frame SpecialHighlightUndo { get; private set; }
        public static Frame SpecialHighlightJoker { get; private set; }

        public static Frame SpecialHighlightBackground { get; private set; }

        public static Frame HandFrame { get; private set; }
        public static Frame RoundCountFrame { get; private set; }
        public static Frame StaxShrinkFrame { get; private set; }
        public static Frame StaxExpandFrame { get; private set; }
        public static Frame StaxBurnFrame { get; private set; }
        public static Frame StaxSwitchFrame { get; private set; }

        public static Frame DeflectorFrame { get; private set; }

        public static Frame EditorHelpFrame1 { get; private set; }
        public static Frame EditorHelpFrame2 { get; private set; }

        public static SpriteFont FontBig { get; private set; }
        public static SpriteFont FontMedium { get; private set; }
        public static SpriteFont FontSmall { get; private set; }

        public static SpriteSheet GuiSheet { get; private set; }

        public static Texture2D LineTexture { get; private set; }

        static Assets()
        {
            TimeLossCurve = Main.Instance.Content.Load<Curve>("marathon_time_loss");

            Cards = new SpriteSheet(Main.Instance.Content.Load<Texture2D>("standard_cards"));
            CardFrames = new FixedFrameSet("default", 74, 96, 1, 1, Point.Zero, 13, 0, 59, 
                ReadDirection.LeftToRight_NextLineBelow, new Vector2(36, 49));
            Cards.RegisterFrameSet(CardFrames);


            //GameSheet = new SpriteSheet(Main.Instance.Content.Load<Texture2D>("game_sheet"));

            GuiSheet = new SpriteSheet(Main.Instance.Content.Load<Texture2D>("gui_sheet"));

            CardColorSymbols = new FixedFrameSet("card_color_symbols", 16, 13, new Point(41, 998), 5, 0, 10, ReadDirection.LeftToRight_NextLineBelow);
            CardColorSymbols.FrameOrigin = new Vector2(-2, -2);

            MenuUpperFrame = new Frame(new Rectangle(360, 0, 480, 210), new Vector2());
            SunbowLogo = new Frame(new Rectangle(780, 600, 240, 120), new Vector2(120, 60));
            ScorePanel = new Frame(new Rectangle(360, 213, 385, 245), new Vector2()); 
            
            GradientColor = new Frame(new Rectangle(855, 120, 150, 60), new Vector2());
            ColorDefault = new Frame(new Rectangle(855, 15, 60, 60), new Vector2());
            ColorDark = new Frame(new Rectangle(945, 15, 60, 60), new Vector2());


            TurnFrameHighlight = new Frame(new Rectangle(0, 998, 20, 26), new Vector2(0, 4));
            TurnFrameNormal = new Frame(new Rectangle(20, 998, 20, 26), new Vector2(0, 4));
            TimeInnerFrame = new Frame(new Rectangle(649, 979, 375, 7), Vector2.Zero);
            TimeBorderFrame = new Frame(new Rectangle(544, 987, 480, 17), Vector2.Zero);
            TurnBackgroundFrame = new Frame(new Rectangle(544, 1004, 480, 20), Vector2.Zero);

            SpecialHighlightDeflector = new Frame(new Rectangle(717, 907, 76, 36), new Vector2(38, 36));
            SpecialHighlightTimeBonus = new Frame(new Rectangle(794, 907, 76, 36), new Vector2(38, 36));
            SpecialHighlightUndo = new Frame(new Rectangle(871, 907, 76, 36), new Vector2(38, 36));
            SpecialHighlightJoker = new Frame(new Rectangle(948, 907, 76, 36), new Vector2(38, 36));

            SpecialHighlightBackground = new Frame(new Rectangle(544, 944, 480, 36), new Vector2(0, 0));

            DeflectorFrame = new Frame(new Rectangle(936, 761, 84, 139), new Vector2(40, 87));

            HandFrame = new Frame(new Rectangle(0, 930, 75, 60), new Vector2(37, 30));
            RoundCountFrame = new Frame(new Rectangle(135, 975, 45, 45), new Vector2(22, 22));
            StaxShrinkFrame = new Frame(new Rectangle(180, 975, 45, 45), new Vector2(22, 22));
            StaxExpandFrame = new Frame(new Rectangle(225, 975, 45, 45), new Vector2(22, 22));
            StaxBurnFrame = new Frame(new Rectangle(270, 975, 45, 45), new Vector2(22, 22));
            StaxSwitchFrame = new Frame(new Rectangle(315, 975, 45, 45), new Vector2(22, 22));

            EditorHelpFrame1 = new Frame(new Rectangle(360, 450, 385, 150), new Vector2());
            EditorHelpFrame2 = new Frame(new Rectangle(360, 600, 385, 150), new Vector2());

            LineTexture = Main.Instance.Content.Load<Texture2D>("line");

            FontBig = Main.Instance.Content.Load<SpriteFont>("Fonts/Vera_30");
            FontMedium = Main.Instance.Content.Load<SpriteFont>("Fonts/Vera_18");
            FontSmall = Main.Instance.Content.Load<SpriteFont>("Fonts/Vera_13");

        }



    }
}
