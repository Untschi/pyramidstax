﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using PyramidStax.Cards;
using Microsoft.Xna.Framework.Graphics;
using starLiGHT.GUI.Widgets;

namespace PyramidStax.Modes.Menu.Help
{
    public class PeaksCardSample : AnimatedCardSample
    {
        PyramidStack pyStack;
        Stack discardPile;

        Card topCard, leftCard, rightCard, firstBottom, secondBottom;


        public PeaksCardSample(Vector2 position, CheckButton pauseButton)
            : base(position, pauseButton)
        {
            pyStack = new PyramidStack(deck, true);
            discardPile = new Stack(deck) { Position = position + new Vector2(100, 0) };

            topCard = deck.TakeCard(CardValue.Ace, CardColor.Spades);
            leftCard = deck.TakeCard(CardValue.King, CardColor.Cross);
            rightCard = deck.TakeCard(CardValue.Queen, CardColor.Heart);

            firstBottom = deck.TakeCard(CardValue.Ace, CardColor.Diamonds);
            secondBottom = deck.TakeCard(CardValue.Two, CardColor.Heart);

            Loop();
        }

        public void Loop()
        {
            deck.CollectAndShuffle();
            deck.CardMover.Clear();

            deck.MainStack.TakeCard(topCard);
            pyStack.AddCard(topCard);
            topCard.Position = deck.MainStack.Position + new Vector2(60, -150);

            deck.MainStack.TakeCard(leftCard);
            pyStack.AddCard(leftCard);
            leftCard.Position = deck.MainStack.Position + new Vector2(20, -120);

            deck.MainStack.TakeCard(rightCard);
            pyStack.AddCard(rightCard);
            rightCard.Position = deck.MainStack.Position + new Vector2(100, -120);

            pyStack.UpdateBlockings();

            deck.MainStack.TakeCard(firstBottom);
            discardPile.AddCard(firstBottom);

            deck.MainStack.TakeCard(secondBottom); // place it on top
            deck.MainStack.AddCard(secondBottom);

            deck.CardMover.AddCardEvent(leftCard, (c1) =>
                {
                    pyStack.TakeCard(c1);
                    discardPile.AddCard(c1);

                    deck.CardMover.AddCardEvent(rightCard, (c2) =>
                        {
                            pyStack.TakeCard(c2);
                            discardPile.AddCard(c2);

                            deck.CardMover.AddCardEvent(secondBottom, (c3) =>
                                {
                                    deck.MainStack.TakeCard(c3);
                                    discardPile.AddCard(c3);

                                    deck.CardMover.AddCardEvent(topCard, (c4) =>
                                        {
                                            pyStack.TakeCard(c4);
                                            discardPile.AddCard(c4);

                                            deck.CardMover.AddCardEvent(null, (c0) => Loop(), 2);
                                        }, 1);
                                }, 1.5f);
                        }, 1);
                }, 1.5f);
            
        }

        public override void Draw(SpriteBatch batch)
        {
            deck.MainStack.Draw(batch);
            discardPile.Draw(batch);
            pyStack.Draw(batch);

            deck.DrawMovingCards(batch);
        }
    }
}
