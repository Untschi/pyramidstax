﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PyramidStax.Cards;
using Microsoft.Xna.Framework;
using starLiGHT.GUI.Widgets;
using Microsoft.Xna.Framework.Graphics;

namespace PyramidStax.Modes.Menu.Help
{
    public abstract class AnimatedCardSample
    {
        protected Deck deck;
        CheckButton pauseButton;

        protected AnimatedCardSample(Vector2 deckPosition, CheckButton pauseButton)
        {
            deck = new Deck(1, false, false, deckPosition);
            deck.MainStack.CardOnTopAtPosition = true;
            deck.MainStack.PositionOffest = new Vector2(-0.2f, 0.1f);

            this.pauseButton = pauseButton;
            if(pauseButton != null)
                pauseButton.Location = deckPosition - new Vector2(0.5f * pauseButton.Size.X - 2, -5);
        }


        public virtual void Update(float seconds)
        {
            if(pauseButton != null && !pauseButton.Checked)
                deck.Update(seconds);
        }

        public abstract void Draw(SpriteBatch batch);
    }
}
