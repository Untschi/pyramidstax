﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using PyramidStax.Cards;
using Sunbow.Util;

namespace PyramidStax.Modes.Menu.Help
{
    public class Py13PairsSample : AnimatedCardSample
    {
        PyramidStack pyramid;

        public Py13PairsSample(Vector2 position)
            : base(position, null)
        {
            pyramid = new PyramidStack(deck, false);

            Card.ForceTransform = true;
            for (int i = 1; i <= 6; i++)
            {
                CardValue val1 = (CardValue)i;
                CardValue val2 = (CardValue)(13 - i);

                CardColor col1 = (CardColor)Sunbow.Util.Random.Next(4);
                CardColor col2 = (CardColor)Sunbow.Util.Random.Next(4);

                Card card1 = deck.TakeCard(val1, col1);
                Card card2 = deck.TakeCard(val2, col2);

                pyramid.AddCard(card1);
                pyramid.AddCard(card2);

                card1.Position = position + new Vector2(40, i * 110);
                card2.Position = position + new Vector2(-40, i * 110);
            }

            Card.ForceTransform = false;

        }


        public override void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch batch)
        {
            pyramid.Draw(batch);
        }
    }
}
