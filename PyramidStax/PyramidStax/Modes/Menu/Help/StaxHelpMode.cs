﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Sunbow.Util;

namespace PyramidStax.Modes.Menu.Help
{
    public class StaxHelpMode : HelpMode
    {
        CardWheelSample cardWheel;

        public StaxHelpMode()
            : base(null, Main.Instance.GuiManager, "StaxHelp")
        {
        }

        protected override void Initialize()
        {
            base.Initialize();

            PreviousMenu = Proxy.GetMode<LevelSelection.LevelSelectionMode>();

            CreateHelpHeader("Overview");
            CreateHelpText("Overview", 200, 400, 400);

            CreateHelpHeader("TurnStart");
            CreateHelpText("TurnStart", 10, 160, 150);

            CreateHelpHeader("Option1_ShrinkStax");
            CreateHelpText("Option1_ShrinkStax", 80, 280, 150);

            CreateHelpHeader("Option2_ExpandStax");
            CreateHelpText("Option2_ExpandStax", 80, 280, 150);

            CreateHelpHeader("Option3_ChangeCard");
            CreateHelpText("Option3_ChangeCard", 200, 400, 150);

            CreateHelpHeader("Option_BurnPile");
            CreateHelpText("Option_BurnPile", 200, 400, 150);

            CreateHelpHeader("AdditionalInfo");
            CreateHelpText("AdditionalInfo", 80, 280, 150);

            CreateHelpHeader("CardWheel");
            CreateEmptySpace(400);
            CreateHelpText("CardWheel", 80, 280, 150);
        }

        protected override void Enter()
        {
            cardWheel = new CardWheelSample(new Vector2(240, 2400), CreatePauseButton());

            base.Enter();
        }

        protected override void Leave()
        {
            cardWheel = null;

            base.Leave();
        }

        protected override void Update(GameTime gameTime, Sunbow.Util.Delegation.InputState inputState)
        {
            cardWheel.Update(gameTime.GetElapsedSeconds());
            base.Update(gameTime, inputState);
        }

        protected override void DrawStuff(Microsoft.Xna.Framework.Graphics.SpriteBatch batch)
        {
            cardWheel.Draw(batch);

            float pos = 800;
            Assets.GuiSheet.Draw(batch, Assets.HandFrame, new Vector2(50, pos), 0, 1);
            pos += 75;
            Assets.GuiSheet.Draw(batch, Assets.RoundCountFrame, new Vector2(50, pos), 0, 1);

            pos += 200;
            Assets.GuiSheet.Draw(batch, Assets.StaxShrinkFrame, new Vector2(50, pos), 0, 1);

            pos += 240;
            Assets.GuiSheet.Draw(batch, Assets.StaxExpandFrame, new Vector2(50, pos), 0, 1);

            pos += 240;
            Assets.GuiSheet.Draw(batch, Assets.StaxSwitchFrame, new Vector2(50, pos), 0, 1);

            pos += 240;
            Assets.GuiSheet.Draw(batch, Assets.StaxBurnFrame, new Vector2(50, pos), 0, 1);

            base.DrawStuff(batch);
        }
    }
}
