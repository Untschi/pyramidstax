﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI.Widgets;
using Microsoft.Xna.Framework;
using Sunbow.Util;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Util.Delegation;

namespace PyramidStax.Modes.Menu.Help
{
    public class PeaksHelpMode : HelpMode
    {
        AnimatedCardSample sample1;
        CardWheelSample sample2;

        public PeaksHelpMode()
            : base(null, Main.Instance.GuiManager, "PeaksHelp")
        {
        }

        protected override void Initialize()
        {
            base.Initialize();
            PreviousMenu = Proxy.GetMode<LevelSelection.LevelSelectionMode>();

            //for(int i = 0; i < 30; i++)
            //{
            //    CreateLabel("test" + i, new Vector2(), new Vector2(480, 50), Assets.FontBig, starLiGHT.GUI.TextAlignment.Center, false, Globals.Dark, panel);
            //}

            CreateHelpHeader("Rules");
            CreateHelpText("Rules", 200, 400, 250);
            CreateHelpHeader("ColorWheel");
            CreateEmptySpace(350);
            CreateHelpText("CardWheel", 40, 400, 100);
            CreateHelpHeader("Undo");
            CreateHelpText("Undo", 200, 400, 120);
            CreateHelpHeader("Joker");
            CreateHelpText("Joker", 80, 280, 150);
            CreateHelpHeader("TimeBonus");
            CreateHelpText("TimeBonus", 200, 400, 150);
        }
        protected override void Enter()
        {
            
            sample1 = new PeaksCardSample(new Vector2(50, 470), CreatePauseButton());
            sample2 = new CardWheelSample(new Vector2(240, 800), CreatePauseButton());

            base.Enter();
        }

        protected override void Leave()
        {
            sample1 = null;
            sample2 = null;
            
            base.Leave();
        }

        protected override void Update(GameTime gameTime, InputState inputState)
        {
            float seconds = gameTime.GetElapsedSeconds();
            sample1.Update(seconds);
            sample2.Update(seconds);

            base.Update(gameTime, inputState);
        }

        protected override void DrawStuff(SpriteBatch spriteBatch)
        {
            sample1.Draw(spriteBatch);
            sample2.Draw(spriteBatch);


            // UNDO

            int pos = 1220;
            Assets.Cards.Draw(spriteBatch, Assets.CardFrames.CalculateFrame(Cards.Card.UNDO_RED_IDX),
                new Vector2(100, pos), 0, 1, Color.White);
            pos += 90;
            Assets.GuiSheet.Draw(spriteBatch, Assets.SpecialHighlightUndo, new Vector2(100, pos), 0, 1);


            // JOKER

            pos += 70;
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(1), new Vector2(340, pos), 0, 1, Color.Black);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(2), new Vector2(360, pos), 0, 1, Color.Red);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(0), new Vector2(380, pos), 0, 1, Color.Black);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(3), new Vector2(400, pos), 0, 1, Color.Red);

            pos += 70;
            Assets.Cards.Draw(spriteBatch, Assets.CardFrames.CalculateFrame(Cards.Card.JOKER_BLACK_IDX),
                new Vector2(380, pos), 0, 1, Color.White);

            pos += 90;
            Assets.GuiSheet.Draw(spriteBatch, Assets.SpecialHighlightJoker, new Vector2(380, pos), 0, 1);


            // TIME BONUS

            pos += 70;
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(1), new Vector2(60, pos), 0, 1, Color.Black);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(1), new Vector2(80, pos), 0, 1, Color.Black);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(1), new Vector2(100, pos), 0, 1, Color.Black);
            pos += 20;
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(2), new Vector2(60, pos), 0, 1, Color.Red);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(2), new Vector2(80, pos), 0, 1, Color.Red);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(2), new Vector2(100, pos), 0, 1, Color.Red);
            pos += 20;
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(0), new Vector2(60, pos), 0, 1, Color.Black);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(0), new Vector2(80, pos), 0, 1, Color.Black);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(0), new Vector2(100, pos), 0, 1, Color.Black);
            pos += 20;
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(3), new Vector2(60, pos), 0, 1, Color.Red);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(3), new Vector2(80, pos), 0, 1, Color.Red);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(3), new Vector2(100, pos), 0, 1, Color.Red);

            pos += 60;
            Assets.GuiSheet.Draw(spriteBatch, Assets.SpecialHighlightTimeBonus, new Vector2(90, pos), 0, 1);


            base.DrawStuff(spriteBatch);
        }
    }
}
