﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using PyramidStax.Cards;
using Microsoft.Xna.Framework.Graphics;
using starLiGHT.GUI.Widgets;
using PyramidStax.Modes.Feedback;

namespace PyramidStax.Modes.Menu.Help
{
    public class Py13CardSample : AnimatedCardSample
    {
        PyramidStack pyStack;
        Stack pile1, pile2, pile3, discardPile;

        Card topCard, leftCard, rightCard, firstBottom, secondBottom, thirdBottom;
        CardSelectionHighlighter highlighter;


        public Py13CardSample(Vector2 position, CheckButton pauseButton)
            : base(position, pauseButton)
        {
            pyStack = new PyramidStack(deck, false);
            pile1 = new Stack(deck) { Position = position + new Vector2(80, 0) };
            pile2 = new Stack(deck) { Position = position + new Vector2(155, 0) };
            pile3 = new Stack(deck) { Position = position + new Vector2(230, 0) };

            discardPile = new Stack(deck) { Position = position + new Vector2(400, 0) };

            topCard = deck.TakeCard(CardValue.Queen, CardColor.Heart);
            leftCard = deck.TakeCard(CardValue.Ten, CardColor.Cross);
            rightCard = deck.TakeCard(CardValue.Eight, CardColor.Spades);

            firstBottom = deck.TakeCard(CardValue.Ace, CardColor.Diamonds);
            secondBottom = deck.TakeCard(CardValue.Three, CardColor.Heart);
            thirdBottom = deck.TakeCard(CardValue.Five, CardColor.Diamonds);

            highlighter = new CardSelectionHighlighter();

            Loop();
        }

        public void Loop()
        {
            deck.CollectAndShuffle();
            deck.CardMover.Clear();
            highlighter.Clear();

            deck.MainStack.TakeCard(topCard);
            pyStack.AddCard(topCard);
            topCard.Position = deck.MainStack.Position + new Vector2(140, -150);

            deck.MainStack.TakeCard(leftCard);
            pyStack.AddCard(leftCard);
            leftCard.Position = deck.MainStack.Position + new Vector2(100, -120);

            deck.MainStack.TakeCard(rightCard);
            pyStack.AddCard(rightCard);
            rightCard.Position = deck.MainStack.Position + new Vector2(180, -120);

            pyStack.UpdateBlockings();

            deck.MainStack.TakeCard(firstBottom);
            pile1.AddCard(firstBottom);

            deck.MainStack.TakeCard(secondBottom);
            pile2.AddCard(secondBottom);

            deck.MainStack.TakeCard(thirdBottom);
            pile3.AddCard(thirdBottom);

            deck.CardMover.AddCardEvent(null, (c1) =>
                {
                    highlighter.Highlight(leftCard);
                    highlighter.Highlight(secondBottom);

                    deck.CardMover.AddCardEvent(null, (c2) =>
                        {
                            pyStack.TakeCard(leftCard);
                            discardPile.AddCard(leftCard);
                            discardPile.AddCard(pile2.TakeCard());
                            highlighter.Clear();

                            deck.CardMover.AddCardEvent(null, (c3) =>
                            {
                                highlighter.Highlight(rightCard);
                                highlighter.Highlight(thirdBottom);

                                deck.CardMover.AddCardEvent(null, (c4) =>
                                {
                                    pyStack.TakeCard(rightCard);
                                    discardPile.AddCard(rightCard);
                                    discardPile.AddCard(pile3.TakeCard());
                                    pyStack.UpdateBlockings();
                                    highlighter.Clear();

                                    deck.CardMover.AddCardEvent(null, (c5) =>
                                    {
                                        highlighter.Highlight(topCard);
                                        highlighter.Highlight(firstBottom);

                                        deck.CardMover.AddCardEvent(null, (c6) =>
                                        {
                                            pyStack.TakeCard(topCard);
                                            discardPile.AddCard(topCard);
                                            discardPile.AddCard(pile1.TakeCard());
                                            highlighter.Clear();

                                            deck.CardMover.AddCardEvent(null, (c0) => Loop(), 2);
                                        }, 1);
                                    }, 0.5f);
                                }, 1);
                            }, 0.5f);
                        }, 1);
                }, 1f);
            
        }

        public override void Update(float seconds)
        {
            highlighter.Update(seconds);
            base.Update(seconds);
        }

        public override void Draw(SpriteBatch batch)
        {
            deck.MainStack.Draw(batch);
            pile1.Draw(batch);
            pile2.Draw(batch);
            pile3.Draw(batch);
            pyStack.Draw(batch);

            deck.DrawMovingCards(batch);

            highlighter.Draw(batch);
        }
    }
}
