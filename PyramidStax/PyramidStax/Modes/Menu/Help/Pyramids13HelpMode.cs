﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Sunbow.Util;
using Microsoft.Xna.Framework.Graphics;

namespace PyramidStax.Modes.Menu.Help
{
    public class Pyramids13HelpMode : HelpMode
    {
        AnimatedCardSample sample;
        Py13PairsSample pairs;

        public Pyramids13HelpMode()
            : base(null, Main.Instance.GuiManager, "Pyramids13Help")
        {
        }

        protected override void Initialize()
        {
            base.Initialize();
            PreviousMenu = Proxy.GetMode<LevelSelection.LevelSelectionMode>();


            CreateHelpHeader("Rules");
            CreateHelpText("Rules", 200, 400, 400);
            CreateEmptySpace(200);
            CreateHelpText("Pairs", 10, 160, 50);
            CreateHelpHeader("Deflector");
            CreateHelpText("Deflector", 80, 280, 150);
            CreateHelpHeader("TimeBonus");
            CreateHelpText("TimeBonus", 200, 400, 150);
            CreateHelpHeader("Undo");
            CreateHelpText("Undo", 80, 280, 150);
        }

        protected override void Enter()
        {
            sample = new Py13CardSample(new Vector2(210, 920), CreatePauseButton());
            pairs = new Py13PairsSample(new Vector2(80, 210));

            base.Enter();
        }

        protected override void Update(GameTime gameTime, Sunbow.Util.Delegation.InputState inputState)
        {
            float seconds = gameTime.GetElapsedSeconds();

            base.Update(gameTime, inputState);

            sample.Update(seconds);
        }

        protected override void DrawStuff(SpriteBatch spriteBatch)
        {
            sample.Draw(spriteBatch);
            pairs.Draw(spriteBatch);



            int pos = 1130;


            // DEFLECTOR


            Assets.GuiSheet.Draw(spriteBatch, Assets.DeflectorFrame, new Vector2(380, pos), 0, 1, Color.White);

            pos -= 82;
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(2), new Vector2(342, pos), 0, 1, Color.Red);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(2), new Vector2(357, pos), 0, 1, Color.Red);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(3), new Vector2(387, pos), 0, 1, Color.Red);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(3), new Vector2(402, pos), 0, 1, Color.Red);

            pos += 15;
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(0), new Vector2(342, pos), 0, 1, Color.Black);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(0), new Vector2(357, pos), 0, 1, Color.Black);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(1), new Vector2(387, pos), 0, 1, Color.Black);
            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(1), new Vector2(402, pos), 0, 1, Color.Black);

            pos += 160;
            Assets.GuiSheet.Draw(spriteBatch, Assets.SpecialHighlightDeflector, new Vector2(380, pos), 0, 1);


            // TIME BONUS

            pos += 120;
            Assets.Cards.Draw(spriteBatch, Assets.CardFrames.CalculateFrame(51), new Vector2(60, pos), 0, 1);
            Assets.Cards.Draw(spriteBatch, Assets.CardFrames.CalculateFrame(25), new Vector2(80, pos), 0, 1);
            Assets.Cards.Draw(spriteBatch, Assets.CardFrames.CalculateFrame(38), new Vector2(100, pos), 0, 1);
            Assets.Cards.Draw(spriteBatch, Assets.CardFrames.CalculateFrame(12), new Vector2(120, pos), 0, 1);

            pos += 90;
            Assets.GuiSheet.Draw(spriteBatch, Assets.SpecialHighlightTimeBonus, new Vector2(90, pos), 0, 1);


            // UNDO

            pos += 150;
            Assets.Cards.Draw(spriteBatch, Assets.CardFrames.CalculateFrame(Cards.Card.UNDO_RED_IDX),
                new Vector2(380, pos), 0, 1, Color.White);
            pos += 90;
            Assets.GuiSheet.Draw(spriteBatch, Assets.SpecialHighlightUndo, new Vector2(380, pos), 0, 1);


            base.DrawStuff(spriteBatch);
        }
    }
}
