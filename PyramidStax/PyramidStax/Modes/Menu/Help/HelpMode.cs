using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using starLiGHT.GUI.Widgets;
using starLiGHT.GUI;
using Microsoft.Xna.Framework.Graphics;

#if WINDOWS_PHONE
using Microsoft.Xna.Framework.Input.Touch;
#endif

namespace PyramidStax.Modes.Menu.Help
{
    public class HelpMode : MenuMode
    {
        const float velocity_deceleration = 2f;

        float velocity;
        float position;

        protected StackPanel panel;
        Button backButton;
        //ContainerWidget mask;

        public HelpMode(MenuMode prevMode, GuiManager manager, string name)
            : base(prevMode, manager, name)
        {
        }

        protected override void Initialize()
        {

            float top = title.Location.Y + title.Size.Y;

            //mask = new ContainerWidget(GuiManager, this.Name + "_mask")
            //{
            //    Location = new Vector2(0, top),
            //    Size = new Vector2(480, 800 - container.Location.Y - top),
            //};
            //container.Widgets.Add(mask);

            panel = new StackPanel(GuiManager, this.Name + "_container_panel")
            {
                Size = new Vector2(400, 800),
                Spacing = 20,
            };
            container.Widgets.Add(panel);

            panel.SizeChanged += (o, a) => container.Size = new Vector2(container.Size.X, panel.Size.Y + panel.Location.Y);

            title.Location = new Vector2(0, container.Location.Y);
            panel.Location = new Vector2(40, title.Location.Y + title.Size.Y + MenuMode.SpacingHeight);
            container.Location = new Vector2();

            backButton = CreateBackButton();

        }

        protected override void Enter()
        {
            base.Enter();

//#if WINDOWS_PHONE
//            TouchPanel.EnabledGestures = GestureType.VerticalDrag | GestureType.DragComplete | GestureType.Flick | GestureType.Tap;
//#endif
            GuiManager.IsScissoringEnabled = false;

            position = 0;

            backButton.BringToFront(container);
        }

        protected override void Leave()
        {
            base.Leave();
//#if WINDOWS_PHONE
//            TouchPanel.EnabledGestures = 0;
//#endif
            GuiManager.ViewMatrix = Matrix.Identity;
            GuiManager.IsScissoringEnabled = true;
            ViewController.CameraMatrix = Matrix.Identity;
        }

        protected override void Update(GameTime gameTime, Sunbow.Util.Delegation.InputState inputState)
        {
            float seconds = gameTime.GetElapsedSeconds();

            base.Update(gameTime, inputState);

//#if WINDOWS_PHONE
//            foreach (var gesture in inputState.Gestures)
//            {
//                switch (gesture.GestureType)
//                {
//                    case GestureType.VerticalDrag:
//                        position += gesture.Delta.Y;
//                        velocity = 0;
//                        break;
//                    case GestureType.DragComplete:
//                    case GestureType.Tap:
//                        velocity = 0;
//                        break;
//                    case GestureType.Flick:
//                        velocity = gesture.Delta.Y;
//                        break;
//                }
//            }
//#else
            if (inputState.LastMouseState.LeftButton == ButtonState.Pressed)
            {
                float delta = inputState.CurrentMouseState.Y - inputState.LastMouseState.Y;

                if (inputState.CurrentMouseState.LeftButton == ButtonState.Pressed) // drag
                {
                    position -= delta;
                    velocity = 0;
                }
                else                                                                // release
                {
                    velocity = -delta * 30;
                }
            }
//#endif
            position += velocity * seconds;
            velocity -= seconds * (velocity_deceleration * velocity);
            if (Math.Abs(velocity) < 5)
                velocity = 0;

            if (position < 0)
                position = 0;
            else if (position + GraphicsDevice.Viewport.Height > panel.Size.Y + panel.Location.Y)
                position = panel.Location.Y + panel.Size.Y - GraphicsDevice.Viewport.Height;

            //panel.Location = new Vector2(0, -position);
            position = (int)position;

            ViewController.CameraMatrix = Matrix.CreateTranslation(0, -position, 0);
            GuiManager.ViewMatrix = ViewController.CameraMatrix;

            backButton.Location = new Vector2(backButton.Location.X, (int)position + 755);
        }

        protected override void Draw(GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            spriteBatch.Prepare();
            DrawMenuBackground(spriteBatch, false, (int)panel.Size.Y);
            DrawStuff(spriteBatch);
            spriteBatch.End();
        }

        protected virtual void DrawStuff(SpriteBatch batch)
        {
        }


        protected Label CreateHelpHeader(string name)
        {
            Label lbl = new Label(GuiManager, name)
            {
                Size = new Vector2(400, 50),
                Text = Loc[this.Name + "_Header_" + name],
                TextAlignment = TextAlignment.Center,
                ForeColor = Globals.Dark,
                Font = Assets.FontBig,
            };
            panel.Widgets.Add(lbl);
            return lbl;
        }

        protected Label CreateHelpText(string name, int left, int right, int height)
        {
            int width = right - left;

            Label lbl = new Label(GuiManager, name)
            {
                Location = new Vector2(left, 0),
                Size = new Vector2(width, height),
                Text = Loc[this.Name + "_Text_" + name],
                TextAlignment = (width <= 220) ? TextAlignment.Justify : TextAlignment.Center,
                WrapText = true,
                ForeColor = Globals.VeryDark,
                Font = Assets.FontSmall,
            };
            panel.Widgets.Add(lbl);
            return lbl;
        }


        protected CheckButton CreatePauseButton()
        {
            CheckButton btn = new CheckButton(GuiManager, Name + "_pause", "PauseButton")
            {
                Size = new Vector2(60, 30),
            };
            container.Widgets.Add(btn);
            return btn;
        }

        protected void CreateEmptySpace(int height)
        {
            Panel space = new Panel(GuiManager, Globals.GetCustomName("empty_panel"))
            {
                Size = new Vector2(480, height),
            };
            panel.Widgets.Add(space);
        }
    }
}
