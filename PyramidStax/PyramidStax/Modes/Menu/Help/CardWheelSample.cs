﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using PyramidStax.Cards;
using Microsoft.Xna.Framework.Graphics;
using starLiGHT.GUI.Widgets;

namespace PyramidStax.Modes.Menu.Help
{
    public class CardWheelSample : AnimatedCardSample
    {
        const float radius = 120;
        Vector2[] positions = new Vector2[13];
        float[] rotations = new float[13];

        PyramidStack pyStack;

        public CardWheelSample(Vector2 position, CheckButton pauseButton)
            : base(position, pauseButton)
        {
            pyStack = new PyramidStack(deck, false);

            //Card.ForceTransform = true;
            for (int i = 0; i < (int)CardColor._Count; i++)
            {
                CardColor col = (i == 0) ? CardColor.Spades 
                    : ((i == 1) ? CardColor.Heart
                    : ((i == 2) ? CardColor.Cross
                    : CardColor.Diamonds));

                for (int k = 1; k < (int)CardValue._Count; k++)
                {
                    int idx = k - 1;
                    float radians = MathHelper.TwoPi * ((float)idx / ((float)CardValue._Count - 1));
                    float shiftedRadians = radians - MathHelper.PiOver2 + (1f / 13) * MathHelper.TwoPi;

                    positions[idx] = position + radius * new Vector2((float)Math.Cos(shiftedRadians), (float)Math.Sin(shiftedRadians));
                    rotations[idx] = MathHelper.Pi - radians - (1f / 13) * MathHelper.TwoPi;

                    Card c = deck.TakeCard((CardValue)k, col);
                    pyStack.AddCard(c);
                    c.Position = positions[idx];
                    c.Rotation = rotations[idx];
                }
            }
            //Card.ForceTransform = false;
            deck.CardMover.AddCardEvent(null, (a) =>
                {
                    pauseButton.Checked = true;
                    deck.CardMover.AddCardEvent(null, (c) => Loop(), 1);
                }, 1);
        }

        private void Loop()
        {
            //deck.CardMover.Clear();

            deck.CardMover.AddCardEvent(pyStack.Cards[pyStack.Cards.Count - 1], (c) =>
                {
                    pyStack.Arrange(c, 0);

                    for (int i = 0; i < pyStack.Cards.Count; i++)
                    {
                        pyStack.Cards[i].Position = positions[i % positions.Length];
                        pyStack.Cards[i].Rotation = rotations[i % positions.Length];
                    }
                    Loop();
                }, 0.9f);
        }

        public override void Draw(SpriteBatch batch)
        {
            pyStack.Draw(batch);

            deck.DrawMovingCards(batch);
        }
    }
}
