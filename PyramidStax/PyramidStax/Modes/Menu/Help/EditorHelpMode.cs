﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Shapes.Geometry;
using Shapes.Sprites;
using starLiGHT.GUI;

namespace PyramidStax.Modes.Menu.Help
{
    public class EditorHelpMode : HelpMode
    {
        static readonly Vector2 pos1 = new Vector2(50, 900); 
        static readonly Vector2 pos2 = new Vector2(50, 1300);

        OutlineSprite lHelp, lDelete, lRotate1, lRotate2, lArrange1, lArrange2, lGrid, lSnap;
        OutlineSprite lName, lNew, lSave, lLoad, lUpload;

        OutlineSprite tab1, tab2;

        public EditorHelpMode()
            : base(null, Main.Instance.GuiManager, "EditorHelp")
        {
        }

        protected override void Initialize()
        {
            base.Initialize();

            PreviousMenu = Proxy.GetMode<Edit.PyramidsEdit>();

            lHelp = new OutlineSprite(Assets.LineTexture, new Line(pos1 + new Vector2(10, -20), pos1 + new Vector2(30, 20))) { Color = Globals.Dark };
            lDelete = new OutlineSprite(Assets.LineTexture, new Line(pos1 + new Vector2(10, 170), pos1 + new Vector2(20, 130))) { Color = Globals.Dark };

            lRotate1 = new OutlineSprite(Assets.LineTexture, new Line(pos1 + new Vector2(110, -50), pos1 + new Vector2(120, 20))) { Color = Globals.Dark };
            lRotate2 = new OutlineSprite(Assets.LineTexture, new Line(pos1 + new Vector2(110, -50), pos1 + new Vector2(100, 90))) { Color = Globals.Dark };

            lArrange1 = new OutlineSprite(Assets.LineTexture, new Line(pos1 + new Vector2(180, 200), pos1 + new Vector2(170, 60))) { Color = Globals.Dark };
            lArrange2 = new OutlineSprite(Assets.LineTexture, new Line(pos1 + new Vector2(180, 200), pos1 + new Vector2(200, 130))) { Color = Globals.Dark };

            lGrid = new OutlineSprite(Assets.LineTexture, new Line(pos1 + new Vector2(280, -10), pos1 + new Vector2(280, 20))) { Color = Globals.Dark };
            lSnap = new OutlineSprite(Assets.LineTexture, new Line(pos1 + new Vector2(280, 160), pos1 + new Vector2(280, 130))) { Color = Globals.Dark };


            lName = new OutlineSprite(Assets.LineTexture, new Line(pos2 + new Vector2(10, -20), pos2 + new Vector2(30, 30))) { Color = Globals.Dark };

            lNew = new OutlineSprite(Assets.LineTexture, new Line(pos2 + new Vector2(10, 200), pos2 + new Vector2(20, 130))) { Color = Globals.Dark };
            lSave = new OutlineSprite(Assets.LineTexture, new Line(pos2 + new Vector2(120, 160), pos2 + new Vector2(120, 130))) { Color = Globals.Dark };
            lLoad = new OutlineSprite(Assets.LineTexture, new Line(pos2 + new Vector2(190, 200), pos2 + new Vector2(190, 130))) { Color = Globals.Dark };
            lUpload = new OutlineSprite(Assets.LineTexture, new Line(pos2 + new Vector2(280, 160), pos2 + new Vector2(280, 130))) { Color = Globals.Dark };


            tab1 = new OutlineSprite(Assets.LineTexture, new LineStrip(
                pos1 + new Vector2(370, 30), 
                pos1 + new Vector2(400, 0),
                pos1 + new Vector2(400, -120),
                pos1 + new Vector2(370, -150),
                pos1 + new Vector2(0, -150))) { Color = Globals.VeryDark };

            tab2 = new OutlineSprite(Assets.LineTexture, new LineStrip(
                pos2 + new Vector2(370, 120),
                pos2 + new Vector2(400, 90),
                pos2 + new Vector2(400, -50),
                pos2 + new Vector2(370, -80),
                pos2 + new Vector2(0, -80))) { Color = Globals.VeryDark };

            CreateLabel("EditHelp_Help", pos1 + new Vector2(-30, -80), new Vector2(80, 60), Assets.FontSmall, TextAlignment.Center, true, Globals.VeryDark);
            CreateLabel("EditHelp_Delete", pos1 + new Vector2(-30, 175), new Vector2(100, 60), Assets.FontSmall, TextAlignment.Center, true, Globals.VeryDark);
            CreateLabel("EditHelp_Rotate", pos1 + new Vector2(80, -100), new Vector2(100, 60), Assets.FontSmall, TextAlignment.Center, true, Globals.VeryDark);
            CreateLabel("EditHelp_Arrange", pos1 + new Vector2(130, 205), new Vector2(100, 60), Assets.FontSmall, TextAlignment.Center, true, Globals.VeryDark);
            CreateLabel("EditHelp_Grid", pos1 + new Vector2(260, -70), new Vector2(100, 60), Assets.FontSmall, TextAlignment.Center, true, Globals.VeryDark);
            CreateLabel("EditHelp_Snap", pos1 + new Vector2(260, 165), new Vector2(100, 60), Assets.FontSmall, TextAlignment.Center, true, Globals.VeryDark);

            CreateLabel("EditHelp_Name", pos2 + new Vector2(-30, -50), new Vector2(350, 60), Assets.FontSmall, TextAlignment.Left, true, Globals.VeryDark);
            CreateLabel("EditHelp_New", pos2 + new Vector2(-30, 205), new Vector2(100, 60), Assets.FontSmall, TextAlignment.Center, true, Globals.VeryDark);
            CreateLabel("EditHelp_Save", pos2 + new Vector2(80, 165), new Vector2(100, 60), Assets.FontSmall, TextAlignment.Center, true, Globals.VeryDark);
            CreateLabel("EditHelp_Load", pos2 + new Vector2(130, 205), new Vector2(100, 60), Assets.FontSmall, TextAlignment.Center, true, Globals.VeryDark);
            CreateLabel("EditHelp_Upload", pos2 + new Vector2(260, 165), new Vector2(100, 60), Assets.FontSmall, TextAlignment.Center, true, Globals.VeryDark);

            CreateHelpHeader("Cards");
            CreateHelpText("Cards", 100, 300, 400);

            CreateHelpHeader("EditTap");
            CreateEmptySpace(380);

            CreateHelpHeader("FileTap");
            CreateEmptySpace(400);


        }

        protected override void DrawStuff(Microsoft.Xna.Framework.Graphics.SpriteBatch batch)
        {
            Assets.GuiSheet.Draw(batch, Assets.EditorHelpFrame1, pos1, 0, 1);

            lHelp.Draw(batch);
            lDelete.Draw(batch);
            lRotate1.Draw(batch);
            lRotate2.Draw(batch);
            lArrange1.Draw(batch);
            lArrange2.Draw(batch);
            lGrid.Draw(batch);
            lSnap.Draw(batch);

            tab1.Draw(batch);

            Assets.GuiSheet.Draw(batch, Assets.EditorHelpFrame2, pos2, 0, 1);

            lName.Draw(batch);
            lNew.Draw(batch);
            lSave.Draw(batch);
            lLoad.Draw(batch);
            lUpload.Draw(batch);

            tab2.Draw(batch);

            base.DrawStuff(batch);
        }

    }
}
