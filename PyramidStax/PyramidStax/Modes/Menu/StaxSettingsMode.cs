using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI.Widgets;
using starLiGHT.GUI;
using Microsoft.Xna.Framework;
using PyramidStax;

namespace PyramidStax.Modes.Menu
{
    public class StaxSettingsMode : MenuMode
    {
        #region nested types

        class PlayerSettingsArea
        {
            internal RadioButtonController RadioControl;

            internal ContainerWidget Container { get { return container; } }
            ContainerWidget container;
            Label topic;
            internal CheckButton HumanPlayerBtn, AiPlayerBtn;

            public PlayerSettingsArea(GuiManager manager, string topic)
            {
                this.container = new ContainerWidget(manager, topic + "_container")
                {
                    Size = new Vector2(400, 200),
                };

                this.topic = new Label(manager, topic + "_topic")
                {
                    Size = new Vector2(400, 40),
                    Text = Main.Instance.Loc[topic],
                };
                container.Widgets.Add(this.topic);

                this.HumanPlayerBtn = new CheckButton(manager, topic + "_human_player", "Button")
                {
                    Size = new Vector2(400, 60),
                    Location = new Vector2(0, 50),
                    Text = Main.Instance.Loc["human_player"],
                };
                container.Widgets.Add(HumanPlayerBtn);

                this.AiPlayerBtn = new CheckButton(manager, topic + "_ai_player", "Button")
                {
                    Size = new Vector2(400, 60),
                    Location = new Vector2(0, 120),
                    Text = Main.Instance.Loc["ai_player"],
                };
                container.Widgets.Add(AiPlayerBtn);

                RadioControl = new RadioButtonController(HumanPlayerBtn, AiPlayerBtn);
            }
        }

        #endregion

        PlayerSettingsArea playerA, playerB;

        public StaxSettingsMode()
            : base(Main.Instance.Proxy.GetMode<LevelSelection.LevelSelectionMode>(), Main.Instance.GuiManager, "StaxSettingsMode")
        {

        }

        protected override void Initialize()
        {
            playerA = new PlayerSettingsArea(GuiManager, "player_a");
            playerA.Container.Location = new Vector2(40, 60);

            if (Main.Instance.GameSettings.IsStaxPlayerAHuman)
                playerA.HumanPlayerBtn.Checked = true;
            else
                playerA.AiPlayerBtn.Checked = true;

            playerA.RadioControl.SelectionChanged += PlayerA_SelectionChanged;

            playerB = new PlayerSettingsArea(GuiManager, "player_b");
            playerB.Container.Location = new Vector2(40, 300);

            if (Main.Instance.GameSettings.IsStaxPlayerBHuman)
                playerB.HumanPlayerBtn.Checked = true;
            else
                playerB.AiPlayerBtn.Checked = true;
            playerB.RadioControl.SelectionChanged += PlayerB_SelectionChanged;

            container.Widgets.Add(playerA.Container);
            container.Widgets.Add(playerB.Container);

            CreateBackButton();
        }

        protected override void Leave()
        {
            Proxy.GetMode<SettingsMode>().Save();
            base.Leave();
        }

        void PlayerA_SelectionChanged(ICheckable obj)
        {
            Main.Instance.GameSettings.IsStaxPlayerAHuman = (obj == playerA.HumanPlayerBtn);
        }
        void PlayerB_SelectionChanged(ICheckable obj)
        {
            Main.Instance.GameSettings.IsStaxPlayerBHuman = (obj == playerB.HumanPlayerBtn);
        }

        protected override void Draw(GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            spriteBatch.Prepare();
            DrawMenuBackground(spriteBatch, true);
            spriteBatch.End();
        }
    }
}
