using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI;
using starLiGHT.GUI.Widgets;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using PyramidStax;

namespace PyramidStax.Modes.Menu
{
    public class MainMenu : MenuMode
    {

        //TextBox nameBox;

        public MainMenu(GuiManager gui)
            : base(null, gui, "main_menu")
        { 
        }

        protected override void  Initialize()
        {
            int pos = 50;
            CreateButton("editor", new Vector2(40, pos), new Vector2(260, 60), (s, a) => Proxy.SetMode<Edit.PyramidsEdit>(), "Button");

            Button creditsBtn = new Button(GuiManager, Name + "_credits_button", "CreditsButton")
            {
                Size = new Vector2(60, 60),
                Location = new Vector2(310, pos),
            };
            //creditsBtn.Click += (s, a) => Proxy.SetMode<SettingsMode>();
            container.Widgets.Add(creditsBtn);

            //Button helpBtn = new Button(GuiManager, Name + "_help_button", "HelpButton")
            //{
            //    Size = new Vector2(60, 60),
            //    Location = new Vector2(310, pos),
            //};
            ////helpBtn.Click += (s, a) => Proxy.SetMode<SettingsMode>();
            //container.Widgets.Add(helpBtn);


            Button settingsBtn = new Button(GuiManager, Name + "_settings_button", "SettingsButton")
            {
                Size = new Vector2(60, 60),
                Location = new Vector2(380, pos),
            };
            settingsBtn.Click += (s, a) => Proxy.SetMode<SettingsMode>();
            container.Widgets.Add(settingsBtn);


            pos = 160;
            AddButton("pyramid_peaks", GameSelected, ref pos);
            AddButton("pyramids_13", GameSelected, ref pos);
            AddButton("stax", GameSelected, ref pos);


            pos = 430;

            Button marketBtn = new Button(GuiManager, Name + "_market_button", "MarketButton")
            {
                Size = new Vector2(60, 60),
                Location = new Vector2(95, pos),
            };
            //marketBtn.Click += (s, a) => Proxy.SetMode<SettingsMode>();
            container.Widgets.Add(marketBtn);

            Button ratingBtn = new Button(GuiManager, Name + "_rating_button", "RatingButton")
            {
                Size = new Vector2(60, 60),
                Location = new Vector2(210, pos),
            };
            //ratingBtn.Click += (s, a) => Proxy.SetMode<SettingsMode>();
            container.Widgets.Add(ratingBtn);


            Button InternetBtn = new Button(GuiManager, Name + "_internet_button", "InternetButton")
            {
                Size = new Vector2(60, 60),
                Location = new Vector2(325, pos),
            };
            //InternetBtn.Click += (s, a) => Proxy.SetMode<SettingsMode>();
            container.Widgets.Add(InternetBtn);

            CreateBackButton(0, "ExitButton");

            //nameBox = new TextBox(GuiManager, "PlayerNameBox")
            //{
            //    Font = Assets.FontSmall,
            //    Size = new Vector2(300, 40),
            //    Location = new Vector2(70, 10),            
            //};
            //container.Widgets.Add(nameBox);
        }

        protected override void Enter()
        {
            base.Enter();

            //nameBox.Text = Main.Instance.CurrentProfile.Name;
        }

        protected override void Leave()
        {
            base.Leave();

            //Main.Instance.CurrentProfile.Name = nameBox.Text;
        }

        private void GameSelected(object sender, EventArgs args)
        {
            switch ((sender as Button).Name)
            {
                case "main_menu_pyramid_peaks":
                    Main.Instance.GameSettings.GameType = GameType.PyramidPeaks;
                    break;

                case "main_menu_pyramids_13":
                    Main.Instance.GameSettings.GameType = GameType.Pyramids13;
                    break;

                case "main_menu_stax":
                    Main.Instance.GameSettings.GameType = GameType.Stax;
                    break;

                default:
                    Debugger.Break();
                    break;
            }
            Proxy.GetMode<LevelSelection.LevelSelectionMode>().PreviousMenu = this;
            Proxy.SetMode<LevelSelection.LevelSelectionMode>();
        }

        protected override void Draw(GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            spriteBatch.Prepare();
            DrawMenuBackground(spriteBatch, true);
            spriteBatch.End();
        }

        protected override void BackButtonPressed()
        {
            Game.Exit();
        }
    }
}
