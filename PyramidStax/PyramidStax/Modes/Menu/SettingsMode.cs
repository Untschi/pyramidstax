﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI.Widgets;
using Microsoft.Xna.Framework;
using PyramidStax;
using starLiGHT.GUI;
using System.IO.IsolatedStorage;
using Sunbow.Util;
using Sunbow.Util.IO;

namespace PyramidStax.Modes.Menu
{
    public class SettingsMode : MenuMode
    {
        const string SETTINGS_FILENAME = "settings.csv";

        Label musicVolLbl, sfxVolLbl, nameLbl, languageLbl;
        HScrollBar musicVolScroll, sfxVolScroll;
        TextBox playerNameBox;

        CheckButton langEN, langDE, langFR, langES, langPT;
        RadioButtonController radioButtonCtrl;

        Table table;

        public SettingsMode()
            : base(Main.Instance.Proxy.GetMode<MainMenu>(), Main.Instance.GuiManager, "SettingsMode")
        {
        }

        protected override void Initialize()
        {
            int posY = 50;
            nameLbl = AddLabel("profileLbl", Assets.FontMedium, TextAlignment.Left, ref posY);
            playerNameBox = new TextBox(GuiManager, "player_namebox"
#if WINDOWS_PHONE
            , new GuideKeyboardInfo(Loc["enter_your_nick"], Loc["enter_nick_description"])
#endif
                )
            {
                Size = new Vector2(400, 40),
                Font = Assets.FontMedium,
                Location = new Vector2(40, 75),
            };
            container.Widgets.Add(playerNameBox);


           
            posY += 15;
            languageLbl = AddLabel("language", Assets.FontMedium, TextAlignment.Left, ref posY);
            posY -= 50;
            langEN = CreateCheckButton("lang_en", new Vector2(40, posY), new Vector2(60, 60), null, "LangEnCheckBox", container);
            langDE = CreateCheckButton("lang_de", new Vector2(124, posY), new Vector2(60, 60), null, "LangDeCheckBox", container);
            langFR = CreateCheckButton("lang_fr", new Vector2(208, posY), new Vector2(60, 60), null, "LangFrCheckBox", container);
            langES = CreateCheckButton("lang_es", new Vector2(292, posY), new Vector2(60, 60), null, "LangEsCheckBox", container);
            langPT = CreateCheckButton("lang_pt", new Vector2(376, posY), new Vector2(60, 60), null, "LangPtCheckBox", container);

            radioButtonCtrl = new RadioButtonController(langEN, langDE, langFR, langES, langPT);
            radioButtonCtrl.SelectionChanged += new Action<ICheckable>(radioButtonCtrl_SelectionChanged);
            SelectCurrentLanguage();
            
            
            posY += 85;
            musicVolLbl = AddLabel("music_vol", Assets.FontMedium, TextAlignment.Left, ref posY);
            posY -= 50;
            musicVolScroll = new HScrollBar(GuiManager, "music_vol_scroll")
            {
                Size = new Vector2(400, 40),
                Location = new Vector2(40, posY),
                Minimum = 0,
                Maximum = 100,
            };
            container.Widgets.Add(musicVolScroll);

            posY += 55;
            sfxVolLbl = AddLabel("sfx_vol", Assets.FontMedium, TextAlignment.Left, ref posY);
            posY -= 50;
            sfxVolScroll = new HScrollBar(GuiManager, "sfx_vol_scroll")
            {
                Size = new Vector2(400, 40),
                Location = new Vector2(40, posY),
                Minimum = 0,
                Maximum = 100,
            };
            container.Widgets.Add(sfxVolScroll);

            posY = 430;
            AddButton("done", (o, a) => Proxy.SetMode<MainMenu>(), ref posY);
        }


        protected override void Enter()
        {
            Load();

            base.Enter();
        }

        protected override void Leave()
        {
            Save();

            base.Leave();
        }

        private void SelectCurrentLanguage()
        {
            switch (Loc.Language)
            {
                case "en-US":
                    langEN.Checked = true;
                    break;
                case "de-DE":
                    langDE.Checked = true;
                    break;
                case "fr-FR":
                    langFR.Checked = true;
                    break;
                case "es-ES":
                    langES.Checked = true;
                    break;
                case "pt-PT":
                    langPT.Checked = true;
                    break;
            }
        }
        void radioButtonCtrl_SelectionChanged(ICheckable obj)
        {
            if (obj == langEN)
                Loc.Language = "en-US";
            else if (obj == langDE)
                Loc.Language = "de-DE";
            else if (obj == langFR)
                Loc.Language = "fr-FR";
            else if (obj == langES)
                Loc.Language = "es-ES";
            else if (obj == langPT)
                Loc.Language = "pt-PT";

            UpdateLocTexts();
        }

        protected override void Draw(GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            spriteBatch.Prepare();
            DrawMenuBackground(spriteBatch, true);
            spriteBatch.End();
        }



        public void Save()
        {
            using (IsolatedStorageFile store = Helper.GetUserStoreForAppDomain())
            {
                if (store.FileExists(SETTINGS_FILENAME))
                {
                    Main.Instance.GameSettings.PlayerName = playerNameBox.Text;
                    Main.Instance.GameSettings.MusicVolume = musicVolScroll.Value / 100f;
                    Main.Instance.GameSettings.SfxVolume = sfxVolScroll.Value / 100f;

                    table["value", "language"] = Loc.Language;

                    table["value", "player_name"] = Main.Instance.GameSettings.PlayerName;
                    table["value", "music_vol"] = Main.Instance.GameSettings.MusicVolume.ToString();
                    table["value", "sfx_vol"] = Main.Instance.GameSettings.SfxVolume.ToString();
                    table["value", "stax_player_a_human"] = Main.Instance.GameSettings.IsStaxPlayerAHuman.ToString();
                    table["value", "stax_player_b_human"] = Main.Instance.GameSettings.IsStaxPlayerBHuman.ToString();
                }
                else
                {
                    table = new Table(SETTINGS_FILENAME, 2, 1);
                    table[0, 0] = "id";
                    table[1, 0] = "value";

                    table.AppendRow("language", Loc.Language);

                    table.AppendRow("player_name", Main.Instance.GameSettings.PlayerName);
                    table.AppendRow("music_vol", Main.Instance.GameSettings.MusicVolume.ToString());
                    table.AppendRow("sfx_vol", Main.Instance.GameSettings.SfxVolume.ToString());
                    table.AppendRow("stax_player_a_human", Main.Instance.GameSettings.IsStaxPlayerAHuman.ToString());
                    table.AppendRow("stax_player_b_human", Main.Instance.GameSettings.IsStaxPlayerBHuman.ToString());
                }

                using(IsolatedStorageFileStream stream = store.OpenFile(SETTINGS_FILENAME, System.IO.FileMode.OpenOrCreate))
                {
                    table.Save(stream, CSVSetting.UniqueHeaders);
                    stream.Close();
                }
            }
        }

        public void Load()
        {
            using (IsolatedStorageFile store = Helper.GetUserStoreForAppDomain())
            {
                if (store.FileExists(SETTINGS_FILENAME))
                {
                    using (IsolatedStorageFileStream stream = store.OpenFile(SETTINGS_FILENAME, System.IO.FileMode.Open))
                    {
                        table = new Table(stream, SETTINGS_FILENAME);

                        Loc.Language = table["value", "language"];
                        Main.Instance.GameSettings.PlayerName = table["value", "player_name"];
                        Main.Instance.GameSettings.MusicVolume = table.Get<float>("value", "music_vol");
                        Main.Instance.GameSettings.SfxVolume = table.Get<float>("value", "sfx_vol");
                        Main.Instance.GameSettings.IsStaxPlayerAHuman = table.Get<bool>("value", "stax_player_a_human");
                        Main.Instance.GameSettings.IsStaxPlayerBHuman = table.Get<bool>("value", "stax_player_b_human");
                        
                    }
                }
                else
                {
                    musicVolScroll.Value = 80;
                    sfxVolScroll.Value = 70;

                    playerNameBox.OnMouseDown(container, new MouseEventArgs(true, false, playerNameBox.Location, true));
                }
            }

            playerNameBox.Text = Main.Instance.GameSettings.PlayerName;
            musicVolScroll.Value = (int)(Main.Instance.GameSettings.MusicVolume * 100);
            sfxVolScroll.Value = (int)(Main.Instance.GameSettings.SfxVolume * 100);

            SelectCurrentLanguage();
        }
    }
}
