﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.IO;
using PyramidStax.Modes.Pyramids;
using PyramidStax.Modes.Pyramids13;
using PyramidStax.Modes.Stax;
using starLiGHT.GUI.Widgets;
using Sunbow.Util.Delegation;
using Microsoft.Xna.Framework;
using PyramidStax.Modes.Menu.Hud;
using PyramidStax.Modes.Menu.Help;

namespace PyramidStax.Modes.Menu.LevelSelection
{
    public class LevelSelectionMode : MenuMode
    {
        internal enum ListingMode
        {
            Campaigns,
            Levels,
            SelfMade,
        }

        internal ListingMode FilterMode { get; set; }

        LevelList list;

        public GameSettings GameSettings { get { return Main.Instance.GameSettings; } }

        Button campaignLevelSwitch;
        Button settingsBtn, helpBtn;


        public LevelSelectionMode()
            : base(null, Main.Instance.GuiManager, "LevelSelection")
        {
        }

        protected override void Initialize()
        {
            PreviousMenu = Proxy.GetMode<MainMenu>();
            int pos = 580;

            campaignLevelSwitch = new Button(GuiManager, Name + "_campaign_level_switch", "SwitchButton")
            {
                Size = new Vector2(330, 60),
                Location = new Vector2(40, 50),
                Text = Loc[((ListingMode)0).ToString()],
                TextAlignment = starLiGHT.GUI.TextAlignment.Center,
            };
            campaignLevelSwitch.Click += new EventHandler(campaignLevelSwitch_CheckChanged);
            container.Widgets.Add(campaignLevelSwitch);

            settingsBtn = new Button(GuiManager, Name + "_settings_button", "SettingsButton")
            {
                Size = new Vector2(60, 60),
                Location = new Vector2(310, 50),
            };
            settingsBtn.Click += new EventHandler(settingsBtn_Click);
            container.Widgets.Add(settingsBtn);

            helpBtn = new Button(GuiManager, Name + "_help_button", "HelpButton")
            {
                Size = new Vector2(60, 60),
                Location = new Vector2(380, 50),
            };
            helpBtn.Click += new EventHandler(helpBtn_Click);
            container.Widgets.Add(helpBtn);

            //list = new LevelList("level_list", container);
            //list.PageContainer.Location = new Microsoft.Xna.Framework.Vector2(40, 140);
            //list.FillWithStandardLevels();
            //list.LevelSelected += list_LevelSelected;


            CreateBackButton();

            //container.Widgets.Add(list.Overview);
        }



        protected override void Enter()
        {
            base.Enter();

            if (list != null)
            {
                list.LevelSelected -= list_LevelSelected;
                container.Widgets.Remove(list.PageContainer);
            }

            list = new LevelList("level_list", container);
            list.PageContainer.Location = new Microsoft.Xna.Framework.Vector2(40, 120);
            list.LevelSelected += list_LevelSelected;

            campaignLevelSwitch.Visible = (GameSettings.GameType != GameType.Stax);
            settingsBtn.Visible = GameSettings.GameType == GameType.Stax;

            FilterSelection();
        }

        private void FilterSelection()
        {
            switch(this.FilterMode)
            {
                case LevelSelectionMode.ListingMode.SelfMade:
                    list.FillWithOwnLevels(GameSettings.GameType != GameType.Stax, GameSettings.GameType == GameType.Stax);
                    break;
            
                default:
                    list.ShowCampaigns = FilterMode == LevelSelectionMode.ListingMode.Campaigns;
                    list.FillWithStandardLevels();
                    break;
            }
        }


        void settingsBtn_Click(object sender, EventArgs e)
        {
            // TODO: switch to settings mode
            switch (GameSettings.GameType)
            {
                case GameType.PyramidPeaks:
                    break;

                case GameType.Pyramids13:
                    break;

                case GameType.Stax:
                    Proxy.SetMode<StaxSettingsMode>();
                    break;
            }
        }
        void helpBtn_Click(object sender, EventArgs e)
        {
            // TODO: switch to help mode
            switch (GameSettings.GameType)
            {
                case GameType.PyramidPeaks:
                    Proxy.SetMode<PeaksHelpMode>();
                    break;

                case GameType.Pyramids13:
                    Proxy.SetMode<Pyramids13HelpMode>();
                    break;

                case GameType.Stax:
                    Proxy.SetMode<StaxHelpMode>();
                    break;
            }
        }

        void campaignLevelSwitch_CheckChanged(object sender, EventArgs e)
        {
            FilterMode = (ListingMode)(((int)FilterMode + 1) % 3);
            campaignLevelSwitch.Text = Loc[FilterMode.ToString()];
            FilterSelection();
        }

        void list_LevelSelected(Table levelData, string id)
        {
            if (levelData != null)
                GameSettings.SetSingleLevel(levelData, id);
            else
                GameSettings.InitPyramidLevels(list.CampaignMapping, id);

            switch (GameSettings.GameType)
            {
                case GameType.PyramidPeaks:

                    Main.Instance.GameSettings.LevelDuration = Globals.PeaksLevelDuration;

                    Proxy.GetMode<GameGui>().BackgroundMode = Proxy.GetMode<PyramidsGame>();
                    Proxy.SetMode<GameGui>();

                    if(!(PreviousMenu is LevelOverScreen))
                        Proxy.GetMode<PyramidsGame>().Reset();

                    Proxy.GetMode<PyramidsGame>().StartNewLevel();
                    break;

                case GameType.Pyramids13:

                    Main.Instance.GameSettings.LevelDuration = Globals.Py13LevelDuration;

                    Proxy.GetMode<GameGui>().BackgroundMode = Proxy.GetMode<Pyramids13Game>();
                    Proxy.SetMode<GameGui>();

                    if (!(PreviousMenu is LevelOverScreen))
                        Proxy.GetMode<Pyramids13Game>().Reset();

                    Proxy.GetMode<Pyramids13Game>().StartNewLevel();
                    break;

                case GameType.Stax:

                    Proxy.GetMode<StaxGame>().InitializeNextEnter();
                    Proxy.GetMode<GameGui>().BackgroundMode = Proxy.GetMode<StaxGame>();
                    Proxy.SetMode<GameGui>();

                    break;
            }

        }


        protected override void Draw(GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            spriteBatch.Prepare();
            DrawMenuBackground(spriteBatch, true);
            spriteBatch.End();
        }

    }
}
