﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using PyramidStax.Cards;
using Microsoft.Xna.Framework;
using Sunbow.Util.Manipulation;
using Sunbow.Util.IO;

namespace PyramidStax.Modes.Menu.LevelSelection
{
    public class ThumbnailPool
    {
        public const float SCALE = 0.2f;
        const float BLUR_SCALE = 0.9f;

        Dictionary<string, Texture2D> thumbs = new Dictionary<string, Texture2D>();

        Deck deck;
        PyramidStack pyramid;

        GraphicsDevice device;

        public ThumbnailPool(GraphicsDevice device)
        {
            this.device = device;

            deck = new Deck(1, false, false, new Vector2(-500,-500));
            pyramid = new PyramidStack(deck);
        }

        public Texture2D GetTexture(string id, Table level, SpriteBatch batch)
        {
            if (thumbs.ContainsKey(id))
                return thumbs[id];

            using (new TempValue<bool>(Card.ForceTransform, true, (val) => Card.ForceTransform = val))
            {
                pyramid.Clear();
                deck.CollectAndShuffle();

                PersistHelper.LoadPyramidStack(level, deck, pyramid, true);

               // pyramid.UpdateBlockings();
            }

            RenderTarget2D rt = new RenderTarget2D(device, (int)(BLUR_SCALE * SCALE * 480), (int)(BLUR_SCALE * SCALE * (Globals.TIMER_POS_Y - 80)));
            device.SetRenderTarget(rt);
            device.Clear(Color.Transparent);

            ViewController.CameraMatrix = Matrix.CreateTranslation(0, -80, 0) * Matrix.CreateScale(BLUR_SCALE * SCALE);

            batch.Prepare();
            pyramid.Draw(batch);
            batch.End();

            device.SetRenderTarget(null);
            ViewController.CameraMatrix = Matrix.Identity;

            thumbs.Add(id, rt);
            return rt;
        }
    }
}
