﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI.Widgets;
using starLiGHT.GUI;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using Sunbow.Util.IO;
using System.IO;
using Sunbow.Util;
using System.IO.IsolatedStorage;
using PyramidStax.Gui;
using Sunbow.Util.Data;
using Sunbow.Util.Web;
using Microsoft.Xna.Framework.Graphics;

namespace PyramidStax.Modes.Menu.LevelSelection
{
    public class LevelList
    {
        //ContainerWidget scrollPanel;
        //public StackPanel Overview { get; private set; }

        public bool ShowCampaigns { get; set; }

        public LevelPageContainer PageContainer { get { return listPageContainer; } }
        LevelPageContainer listPageContainer;
        Table pyramidsTable, staxTable;
        public Table CampaignMapping { get { return pyramidsTable; } }

        public event Action<Table, string> LevelSelected;

        private string contentPrefix { get { return Main.Instance.GameSettings.GameType.GetContentRelatedName(); } }

        ThumbnailPool thumbs;

        public LevelList(string parentName, Widget parent)
        {
            ShowCampaigns = true;

            listPageContainer = new LevelPageContainer(Main.Instance.GuiManager, parentName + "_level_list", new Vector2(60, 60), new Vector2(400, 400))
            {
                Location = new Vector2(40, 0),
            };
            parent.Widgets.Add(listPageContainer);

            //scrollPanel = new ContainerWidget(Main.Instance.GuiManager, parentName + "_LevelList_ScrollPanel");
            //scrollPanel.Size = new Vector2(400);
            //scrollPanel.Location = new Vector2(10, 100);
            //parent.Widgets.Add(scrollPanel);

            //Overview = new StackPanel(Main.Instance.GuiManager, parentName + "_LevelList");
            //scrollPanel.Widgets.Add(Overview);


            pyramidsTable = Main.Instance.Content.Load<Table>(Path.Combine("Levels", "Pyramids_mapping"));
            staxTable = Main.Instance.Content.Load<Table>(Path.Combine("Levels", "Stax_mapping"));

            thumbs = new ThumbnailPool(Main.Instance.GraphicsDevice);
        }

        public void FillWithStandardLevels()
        {
            listPageContainer.Clear();

            if (Main.Instance.GameSettings.GameType == GameType.Stax)   // STAX
            {
                for (int i = 1; i < staxTable.Rows; i++)
                {
                    string filename = staxTable["filename", i];
                    string levelname = staxTable["levelname", i];

                    Table table = (Main.Instance.Content as ThreadedContentManager).LoadTable(Path.Combine("Levels/Stax", filename));
                    LevelButtonWidget btn = new LevelButtonWidget(Main.Instance.GuiManager, filename, levelname);
                    btn.LevelTexture = thumbs.GetTexture(levelname, table, Main.Instance.SpriteBatch);

                    listPageContainer.AddEntry(btn);

                    int tmp = i;
                    btn.Click += (s, a) => StaxLevelSelcted(Path.Combine("Levels", Path.Combine("Stax", filename)));
                }

                listPageContainer.SetPageIndex(0);
                listPageContainer.Commit();
            }
            else
            {
                if (ShowCampaigns)
                {
                    for (int i = 1; i < pyramidsTable.Rows; i++)
                    {
                        bool isCampaign;
                        if (pyramidsTable.TryGet<bool>("is_campaign", i, out isCampaign) && isCampaign)
                        {
                            if (!pyramidsTable.Get<bool>("peaks", i) && !pyramidsTable.Get<bool>("py13", i))
                                continue;

                            string id = pyramidsTable["representation", i];
                            Table table = (Main.Instance.Content as ThreadedContentManager).LoadTable(Path.Combine("Levels/Pyramids", id));

                            LevelButtonWidget btn = new LevelButtonWidget(Main.Instance.GuiManager, "campaign" + i, pyramidsTable[Main.Instance.Loc.Language, i]);
                            btn.LevelTexture = thumbs.GetTexture(id, table, Main.Instance.SpriteBatch);
                            listPageContainer.AddEntry(btn);

                            int tmp = i;
                            btn.Click += (s, a) => CampaignSelection(pyramidsTable["id", tmp]);
                        }
                    }
                }
                else
                {
                    string currentCampaign = "";
                    string mode;
                    switch (Main.Instance.GameSettings.GameType)
                    {
                        case GameType.PyramidPeaks:
                            mode = "peaks";
                            break;
                        case GameType.Pyramids13:
                            mode = "py13";
                            break;
                        default:
                            mode = null;
                            break;
                    }

                    for (int i = 1; i < pyramidsTable.Rows; i++)
                    {
                        bool isCampaign;
                        bool isActive;
                        if (pyramidsTable.TryGet<bool>("is_campaign", i, out isCampaign) && isCampaign)
                        {
                            currentCampaign = pyramidsTable[Main.Instance.Loc.Language, i];
                        }
                        else if (pyramidsTable.TryGet<bool>(mode, i, out isActive) && isActive)
                        {
                            string id = pyramidsTable["id", i];
                            Table table = (Main.Instance.Content as ThreadedContentManager).LoadTable(Path.Combine("Levels/Pyramids", id));
                            LevelButtonWidget btn = new LevelButtonWidget(Main.Instance.GuiManager, id, pyramidsTable[Main.Instance.Loc.Language, i]);

                            btn.LevelTexture = thumbs.GetTexture(id, table, Main.Instance.SpriteBatch);

                            listPageContainer.AddEntry(btn);

                            int temp = i;
                            btn.Click += (s, a) => StandardLevelSelection(temp);
                        }
                    }
                } 
                listPageContainer.SetPageIndex(0);
                listPageContainer.Commit();
            }
        }

        private void StaxLevelSelcted(string p)
        {
            Table level = Main.Instance.Content.Load<Table>(p);

            OnLevelSelected(level, null);
        }
        private void CampaignSelection(string id)
        {
            OnLevelSelected(null, id);
        }

        void StandardLevelSelection(int row)
        {
            try
            {
                string file = pyramidsTable["id", row];
                Table level = Main.Instance.Content.Load<Table>(Path.Combine("Levels", Path.Combine(contentPrefix, file)));
                OnLevelSelected(level, file);
            }
            catch (Exception ex)
            {
                Debugger.Break();
            }
        }

        public void FillWithOwnLevels(bool pyramidLevels, bool staxLevels)
        {
            listPageContainer.Clear();

            try
            {
                using (IsolatedStorageFile storage = Helper.GetUserStoreForAppDomain())
                {
                    //foreach (string dir in storage.GetDirectoryNames("Levels\\*"))
                    //{
                        if (pyramidLevels)
                        {
                            string pyramidsPath = Path.Combine("Levels", "Pyramids");// Path.Combine(dir, "Pyramids"));

                            if (storage.DirectoryExists(pyramidsPath))
                            {
                                foreach (string file in storage.GetFileNames(pyramidsPath + "\\*.csv"))
                                {
                                    //string author = dir;


                                    string name = file.Remove(file.Length - ".csv".Length);

                                    string filePath = Path.Combine(pyramidsPath, file);
                                    Table level;
                                    using (IsolatedStorageFileStream stream = storage.OpenFile(filePath, FileMode.OpenOrCreate))
                                    {
                                        level = new Table(stream, file);
                                    }
                                    LevelButtonWidget btn = new LevelButtonWidget(Main.Instance.GuiManager, name, name);
                                    btn.LevelTexture = thumbs.GetTexture(name, level, Main.Instance.SpriteBatch);

                                    listPageContainer.AddEntry(btn);
                                    btn.Click += (o, a) => OwnLevelSelection(filePath);
                                }
                            }
                        }
                        if (staxLevels)
                        {
                            string staxPath = Path.Combine("Levels", "Stax");//Path.Combine(dir, "Stax"));

                            if (storage.DirectoryExists(staxPath))
                            {

                                foreach (string file in storage.GetFileNames(staxPath + "\\*.csv"))
                                {
                                   // string author = dir;
                                    string name = file.Remove(file.Length - ".csv".Length);

                                    LevelButtonWidget btn = new LevelButtonWidget(Main.Instance.GuiManager, name, name);
                                    listPageContainer.AddEntry(btn);
                                    string tmp = Path.Combine(staxPath, file);
                                    btn.Click += (o, a) => OwnLevelSelection(tmp);
                                }
                            }
                        }
                    //}
                }
            }
            catch (Exception ex)
            {
                Debugger.Break();
            }

            listPageContainer.SetPageIndex(0);
            listPageContainer.Commit();
        }

        private void OwnLevelSelection(string path)
        {
            using (IsolatedStorageFile file = Helper.GetUserStoreForAppDomain())
            {
                using (IsolatedStorageFileStream stream = file.OpenFile(path, FileMode.Open))
                {
                    Table level = new Table(stream, path);
                    OnLevelSelected(level, path);

                    stream.Close();
                }

            }
        }

        public void FillWithOnlineLevels(WebRequester requester)
        {
            listPageContainer.Clear();

            string r = requester.Response.Replace("\\", "");
            string[] parts = r.Split(new char[] {'?'}, StringSplitOptions.RemoveEmptyEntries);

            SpriteBatch batch = new SpriteBatch(Main.Instance.GraphicsDevice);

            foreach (string s in parts)
            {
                WebQuery response = new WebQuery();
                response.AddEntries(s);

                string name = response.Entries[0].Value + "_by_" + response.Entries[1].Value;

                Table level = new Table(response.Entries[2].Value, name);

                LevelButtonWidget btn = new LevelButtonWidget(Main.Instance.GuiManager, name, name);
                btn.LevelTexture = thumbs.GetTexture(name, level, batch);

                listPageContainer.AddEntry(btn);
                btn.Click += (o, a) => OnLevelSelected(level, name);
            }
            listPageContainer.SetPageIndex(0);
            listPageContainer.Commit();
        }


        private void OnLevelSelected(Table level, string id)
        {
            if (LevelSelected != null)
                LevelSelected(level, id);
        }

    }
}
