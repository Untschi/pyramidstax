using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI;
using starLiGHT.GUI.Widgets;
using Microsoft.Xna.Framework;
using Sunbow.Highscore;
using PyramidStax.Gui;

namespace PyramidStax.Modes.Menu
{
    public class HighscoreListMode : MenuMode
    {
        StackPanel upperPanel, lowerPanel, scopeSelectionUp, scopeSelectionDown;
        CheckButton localBtn, dayBtn, weekBtn, monthBtn, allBtn;

        RadioButtonController radioBtnCtrl;

        LocalHighscore localScore;
        ScoreRequester onlineScores;

        public HighscoreListMode(GuiManager manager)
            : base(null, manager, "HighscoreList")
        {

        }

        protected override void Initialize()
        {
            PreviousMenu = Proxy.GetMode<MainMenu>();

            CreateBackButton();

            scopeSelectionUp = new StackPanel(GuiManager, Name + "_score_scopes1")
            {
                Size = new Vector2(400, 60),
                Location = new Vector2(40, 40),
                Orientation = Orientation.Horizontal,
                Spacing = 9,
            };
            container.Widgets.Add(scopeSelectionUp);

            scopeSelectionDown = new StackPanel(GuiManager, Name + "_score_scopes2")
            {
                Size = new Vector2(400, 60),
                Location = new Vector2(40, 85),
                Orientation = Orientation.Horizontal,
                Spacing = 10,
            };
            container.Widgets.Add(scopeSelectionDown);

            AddCheckButton(ref localBtn, "_localBtn", true);
            AddCheckButton(ref allBtn, "_allBtn", true);

            AddCheckButton(ref dayBtn, "_dayBtn", false);
            AddCheckButton(ref weekBtn, "_weekBtn", false);
            AddCheckButton(ref monthBtn, "_monthBtn", false);


            radioBtnCtrl = new RadioButtonController(localBtn, dayBtn, weekBtn, monthBtn, allBtn);
            radioBtnCtrl.SelectionChanged += radioBtnCtrl_SelectionChanged;

            upperPanel = new StackPanel(GuiManager, Name + "_upper_panel")
            {
                Size = new Vector2(300, 200),
                Location = new Vector2(90, 169),
            };
            container.Widgets.Add(upperPanel);

            lowerPanel = new StackPanel(GuiManager, Name + "_lower_panel")
            {
                Size = new Vector2(300, 200),
                Location = new Vector2(90, 413),
            };
            container.Widgets.Add(lowerPanel);

            int posY = 138;
            AddLabel("scores_top10", Assets.FontMedium, TextAlignment.Center, ref posY);
            posY = 382;
            AddLabel("scores_ranking", Assets.FontMedium, TextAlignment.Center, ref posY);

        }


        private void AddCheckButton(ref CheckButton btn, string name, bool isUpperScope)
        {
            btn = new CheckButton(GuiManager, Name + name, "Button")
            {
                Size = new Vector2((isUpperScope) ? 190 : 123, 45),
                Text = Loc["Score" + name],
                Font = Assets.FontMedium,
            };

            if (isUpperScope)
                scopeSelectionUp.Widgets.Add(btn);
            else
                scopeSelectionDown.Widgets.Add(btn);
        }

        protected override void Enter()
        {
            base.Enter();

            dayBtn.Checked = true;
        }

        public void SetHighscores(LocalHighscore localScore, ScoreRequester onlineScores)
        {
            this.localScore = localScore;
            this.onlineScores = onlineScores;
        }

        void radioBtnCtrl_SelectionChanged(ICheckable obj)
        {
            upperPanel.Clear();
            lowerPanel.Clear();

            ScoreList scoresTop = null;
            ScoreList scoresRank = null;

            if (obj == localBtn)
            {
                if (localScore == null)
                    return;

                foreach (string[] s in localScore.GetScoreIterator(1, 10))
                    upperPanel.Widgets.Add(MakeScoreLabel(s, localScore.PlayerRank));

                foreach (string[] s in localScore.GetScoreIterator(int.Parse(localScore.PlayerRank) - 5, 10))
                    lowerPanel.Widgets.Add(MakeScoreLabel(s, localScore.PlayerRank));

                return;
            }
            else if (onlineScores == null)
            {
                localBtn.Checked = true;
                return;
            }
            else if (obj == allBtn)
            {
                scoresTop = onlineScores.GetScoreList("top_all");
                scoresRank = onlineScores.GetScoreList("rank_all");
            }
            else if (obj == dayBtn)
            {
                scoresTop = onlineScores.GetScoreList("top_day");
                scoresRank = onlineScores.GetScoreList("rank_day");
            }
            else if (obj == weekBtn)
            {
                scoresTop = onlineScores.GetScoreList("top_week");
                scoresRank = onlineScores.GetScoreList("rank_week");
            }
            else if (obj == monthBtn)
            {
                scoresTop = onlineScores.GetScoreList("top_month");
                scoresRank = onlineScores.GetScoreList("rank_month");
            }

            if (scoresTop != null && scoresRank != null)
            {
                foreach (string[] s in scoresTop.GetScoreIterator())
                    upperPanel.Widgets.Add(MakeScoreLabel(s, scoresTop.PlayerRank));

                foreach (string[] s in scoresRank.GetScoreIterator())
                    lowerPanel.Widgets.Add(MakeScoreLabel(s, scoresRank.PlayerRank));
            }
            else
            {
                localBtn.Checked = true;
            }
        }

        private ScoreLabel MakeScoreLabel(string[] s, string playerRank)
        {
            return 
                new ScoreLabel(GuiManager, Globals.GetCustomName(Name + "_scoreEntry_"))
                {
                    Size = new Vector2(300, 20),
                    Title = s[0] + ". " + s[1],
                    Score = int.Parse(s[2].Replace(" ", "")),
                    Font = Assets.FontSmall,
                    ForeColor = (s[0] == playerRank) ? Globals.VeryDark : Globals.Dark
                };
        }

        protected override void Draw(GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);


            spriteBatch.Prepare();

            DrawMenuBackground(spriteBatch, false);
            Assets.GuiSheet.Draw(spriteBatch, Assets.ScorePanel, new Vector2(42, 305), 0, 1);
            Assets.GuiSheet.Draw(spriteBatch, Assets.ScorePanel, new Vector2(42, 549), 0, 1);

            spriteBatch.End();
        }
    }
}
