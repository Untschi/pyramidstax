﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.Delegation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PyramidStax.Modes.Menu.old
{
    public class MenuMode : Mode
    {


        protected override void Initialize()
        {
        }
        protected override void Enter()
        {
        }

        protected override void Leave()
        {
        }

        protected override void Update(GameTime gameTime, InputState inputState)
        {
            if (inputState.IsNewKeyPress(Keys.Enter))
            {
                Proxy.SetMode<Modes.Pyramids.PyramidsGame>();
            }
            else if (inputState.IsNewKeyPress(Keys.Back))
            {
                Proxy.SetMode<Pyramids13.Pyramids13Game>();
            }
            else if (inputState.IsNewKeyPress(Keys.Space))
            {
                Proxy.SetMode<Modes.Stax.StaxGame>();
            }
            else if (inputState.IsNewKeyPress(Keys.F9))
            {
                Proxy.SetMode<Modes.Edit.PyramidsEdit>();
            }

        }

        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {

        }

    }
}
