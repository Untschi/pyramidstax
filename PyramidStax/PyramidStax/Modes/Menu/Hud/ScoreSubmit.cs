using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Highscore;
using Sunbow.Util.Data;
using Sunbow.Util.Web;

namespace PyramidStax.Modes.Menu.Hud
{
    public class ScoreSubmit
    {
        #region product info

        public const string Version = "1.0";
        public const ProductInfo ProductReleaseInfo =

#if WINDOWS_PHONE
#if LITE
  ProductInfo.WP7_free;
#else
 ProductInfo.WP7_sell;
#endif


#elif IOS
#if LITE
  ProductInfo.iPhone_free;
#else
  ProductInfo.iPhone_sell;
#endif


#elif ANDROID
#if LITE
  ProductInfo.Android_free;
#else
  ProductInfo.Android_sell;
#endif


#else
  ProductInfo.undefined;
#endif
        #endregion


        public ScoreRequester Requester { get; private set; }
        public LocalHighscore LocalScores { get; private set; }


        public ScoreSubmit()
        {
        }

        internal void SubmitScore(string playerName, long score, string levelId, Action<WebRequester, Exception, RequestErrorCause> fail, Action<WebRequester> success)
        {
            ScoreQuery query = new ScoreQuery((Main.Instance.Content as ThreadedContentManager).LoadTable("Numbers"), playerName, score, ProductReleaseInfo, Version);
            query.AddEntries("level", levelId);

            Requester = new ScoreRequester("http://www.liquidfirearts.com/sunbow/games/pyramid_stax/highscore.php", query);
            Requester.RequestFailed += fail;
            Requester.RequestSucceeded += success;

            Requester.CreateWebRequest();
        }

        internal void Retry()
        {
            Requester.CreateWebRequest();
        }

        internal void SubmitLocalScore(string playerName, int score, string levelId)
        {
            LocalScores = new LocalHighscore(levelId);
            LocalScores.AddNewScore(playerName, score);
            LocalScores.Save();
        }
    }
}
