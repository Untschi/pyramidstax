using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.Delegation;
using starLiGHT.GUI.Widgets;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Util;
using Sunbow.Util.Web;
using Sunbow.Util.Manipulation;

namespace PyramidStax.Modes.Menu.Hud
{
    public class LevelOverScreen : MenuMode
    {
        
        bool gameIsOver;
        public bool GameIsOver { get { return gameIsOver; } set { gameIsOver = value; UpdateText(); } }
        public bool LevelSolved { get; set; }

        Label header, subtitle, submitLbl;
        Button proceedBtn, backToMenuBtn; //selectLevelBtn;
        Button retryBtn, skipBtn;

        ScoreSubmit submitter;

        ScoreBoard scoreBoard;

        SingleAnimator fadeIn = new SinusSingleAnimator(1f);

        public LevelOverScreen()
            : base(null, Main.Instance.GuiManager, "LevelOver")
        { }

        protected override void Initialize()
        {
            container.Location = new Vector2(0, Globals.AD_HEIGHT);

            scoreBoard = new ScoreBoard(GuiManager);
            scoreBoard.Panel.Location = new Vector2(90, 50);

            container.Widgets.Add(scoreBoard.Panel);

            //header = new Label(GuiManager, Name + "header")
            //{
            //    Size = new Vector2(480, 50),
            //    TextAlignment = starLiGHT.GUI.TextAlignment.Center,
            //};
            //container.Widgets.Add(header);

            subtitle = new Label(GuiManager, Name + "subtitle")
            {
                Size = new Vector2(480, 50),
                TextAlignment = starLiGHT.GUI.TextAlignment.Center,
                Location = new Vector2(0, 30),
            };
            container.Widgets.Add(subtitle);

            submitLbl = new Label(GuiManager, Name + "submit_label")
            {
                Size = new Vector2(400, 100),
                Location = new Vector2(40, 280),
                TextAlignment = starLiGHT.GUI.TextAlignment.Center,
                Font = Assets.FontMedium,
            };
            container.Widgets.Add(submitLbl);

            // TODO: Initialize Labels
            int pos = 380;

            //retryBtn = AddButton("retry", (o, a) => Retry(), ref pos);
             
            proceedBtn = AddButton("proceed", (o, a) => Proceed(), ref pos);
            //selectLevelBtn = AddButton("select_level", (o, a) => ShowLevelList(), ref pos);

            pos -= 75;
            retryBtn = AddButton("retry_submit", (o, a) => RetrySubmit(), ref pos);
            retryBtn.Size = new Vector2(190, 60);

            pos -= 70;
            skipBtn = AddButton("skip_submit", (o, a) => SkipSubmit(), ref pos);
            skipBtn.Size = new Vector2(190, 60);
            skipBtn.Location = new Vector2(250, skipBtn.Location.Y);

            pos += 20;

            backToMenuBtn = AddButton("backToMenu", (o, a) => BackToMenu(), ref pos);
            
        }

        protected override void  Enter()
        {
            fadeIn.Reset();
            fadeIn.Play();

            scoreBoard.GameMode = BackgroundMode as PyramidsSolitairMode;
            scoreBoard.SetupScoreEntries(Main.Instance.GameSettings.GameType, gameIsOver, LevelSolved);

            scoreBoard.GameMode.Timer.SetPlayState(true);

            (BackgroundMode as PyramidsSolitairMode).IsInBackground = true;

            int lvlNum = (GameIsOver) ? (Main.Instance.GameSettings.CurrentLevelIndex + 1) : Main.Instance.GameSettings.CurrentLevelIndex;
            title.Text = Loc["Level"] + " " + lvlNum + ": " 
                + ((GameIsOver) ? Loc["GameOver"] : Loc["LevelSolved"]);

           // retryBtn.Visible = gameIsOver;
            proceedBtn.Visible = true;
            //selectLevelBtn.Visible = !gameIsOver && Main.Instance.GameSettings.IsSingleLevelMode;
            backToMenuBtn.Visible = true;

            submitLbl.Visible = false;
            retryBtn.Visible = false;
            skipBtn.Visible = false;

            UpdateText();
        
 	        base.Enter();
        }


        private void ShowLevelList()
        {
            Proxy.GetMode<Modes.Menu.LevelSelection.LevelSelectionMode>().PreviousMenu = this;
            Proxy.SetMode<Modes.Menu.LevelSelection.LevelSelectionMode>();
        }


        protected override void Leave()
        {
            (BackgroundMode as PyramidsSolitairMode).IsInBackground = false;
            base.Leave();

            submitter = null;
        }

        private void UpdateText()
        {
            if (gameIsOver)
            {
                // TODO: header
                RegisterWidgetLocId(proceedBtn, "submit");
            }
            else
            {
                // TODO: header
                RegisterWidgetLocId(proceedBtn, "proceed");
            }
        }

        private void Proceed()
        {
            scoreBoard.SkipScoreAnimation();

            if (gameIsOver)
            {
                proceedBtn.Visible = false;
                //selectLevelBtn.Visible = false;
                backToMenuBtn.Visible = false;

                submitLbl.Visible = true;
                submitLbl.Text = Loc["submitting_score"];

                submitter = new ScoreSubmit();
                GameSettings settings = Main.Instance.GameSettings;
                submitter.SubmitScore(Main.Instance.GameSettings.PlayerName, (BackgroundMode as PyramidsSolitairMode).Score, settings.GameType.GetShortcut() + "_" + settings.GameId, (a, b, c) => SubmitFailed(), (w) => SubmitSucceeded());
            }
            else
            {
                Proxy.SetMode<GameGui>();
                (this.BackgroundMode as PyramidsSolitairMode).StartNewLevel();
            }
        }

        private void BackToMenu()
        {
            (BackgroundMode as PyramidsSolitairMode).Reset();
            Proxy.SetMode<MainMenu>();
        }

        protected override void Update(GameTime gameTime, InputState inputState)
        {
            base.Update(gameTime, inputState);

            float seconds = gameTime.GetElapsedSeconds();

            fadeIn.Update(seconds);

            scoreBoard.Update(seconds);
        }

        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            spriteBatch.Prepare();
            spriteBatch.Draw(Assets.GuiSheet.Texture, new Rectangle(0, 0, 480, Globals.TIMER_POS_Y), Assets.ColorDefault.SourceRectangle, Color.White * (fadeIn * 0.9f));
            spriteBatch.End();
        }


        private void RetrySubmit()
        {
            proceedBtn.Visible = false;
            //selectLevelBtn.Visible = false;
            backToMenuBtn.Visible = false;
            retryBtn.Visible = false;
            skipBtn.Visible = false;

            submitLbl.Visible = true;
            submitLbl.Text = Loc["submitting_score"];

            submitter.Retry();
        }

        private void SkipSubmit()
        {
            SubmitLocalScore();
            Proxy.GetMode<HighscoreListMode>().SetHighscores(submitter.LocalScores, null);
            Proxy.SetMode<HighscoreListMode>();
        }

        private void SubmitFailed() //WebRequester requester, Exception exception, RequestErrorCause cause)
        {
            submitLbl.Text = Loc["submit_score_failed"];

            retryBtn.Visible = true;
            skipBtn.Visible = true;
            backToMenuBtn.Visible = true;
        }

        private void SubmitSucceeded()
        {
            SubmitLocalScore();
            Proxy.GetMode<HighscoreListMode>().SetHighscores(submitter.LocalScores, submitter.Requester);
            Proxy.SetMode<HighscoreListMode>();
        }

        private void SubmitLocalScore()
        {
            GameSettings settings = Main.Instance.GameSettings;
            submitter.SubmitLocalScore(Main.Instance.GameSettings.PlayerName, (BackgroundMode as PyramidsSolitairMode).Score, settings.GameType.GetShortcut() + "_" + settings.GameId);
        }
   
    }
}
