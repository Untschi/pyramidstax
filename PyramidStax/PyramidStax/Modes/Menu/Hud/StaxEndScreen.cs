﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.Delegation;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using starLiGHT.GUI.Widgets;

namespace PyramidStax.Modes.Menu.Hud
{
    public class StaxEndScreen : MenuMode
    {
        Button backToMenuBtn;
        string playerAText, playerBText;
        Vector2 playerATextOrigin, playerBTextOrigin;

        public StaxEndScreen()
            : base(null, Main.Instance.GuiManager, "StaxEndScreen")
        {
        }

        protected override void Initialize()
        {
            int pos = 220;
            backToMenuBtn = AddButton("backToMenu", (o, a) => BackToMenu(), ref pos);
            title.Visible = false;
            //UpdateBackground = false;
        }

        public void SetWinner(bool playerAWins)
        {
            string win = Loc["youWin"];
            string lose = Loc["youLose"];

            playerAText = (playerAWins) ? win : lose;
            playerBText = (playerAWins) ? lose : win;

            playerATextOrigin = 0.5f * Assets.FontMedium.MeasureString(playerAText);
            playerBTextOrigin = 0.5f * Assets.FontMedium.MeasureString(playerBText);
        }

        private void BackToMenu()
        {
            Proxy.SetMode<MainMenu>();
        }

        protected override void Leave()
        {
            base.Leave();
        }

        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            spriteBatch.Prepare();

            spriteBatch.Draw(Assets.GuiSheet.Texture, new Rectangle(0, 0, 480, 800), Assets.ColorDefault.SourceRectangle, Color.White * 0.9f);

            spriteBatch.DrawString(Assets.FontMedium, playerAText, new Vector2(240, 200), Color.White, MathHelper.Pi, playerBTextOrigin, 1, SpriteEffects.None, 0);
            spriteBatch.DrawString(Assets.FontMedium, playerBText, new Vector2(240, 600), Color.White, 0, playerATextOrigin, 1, SpriteEffects.None, 0);
            spriteBatch.End();

        }
    }
}
