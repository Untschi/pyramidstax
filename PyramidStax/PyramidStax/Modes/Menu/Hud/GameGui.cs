﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI.Widgets;
using Microsoft.Xna.Framework;
using Sunbow.Util.Delegation;

namespace PyramidStax.Modes.Menu.Hud
{
    public class GameGui : MenuMode
    {
        CheckButton pauseBtn;

        Mode gameMode;

        public GameGui()
            : base(null, Main.Instance.GuiManager, "MenuMode")
        {
        }

        protected override void Initialize()
        {

            title.Text = string.Empty;

            container.Size = new Vector2(60, 30);
            container.Location = new Vector2(15, 765);

            pauseBtn = new CheckButton(GuiManager, Name + "_pause", "PauseButton")
            {
                Size = new Vector2(60, 30),
                //Location = new Vector2(15, 685),
            };
            container.Widgets.Add(pauseBtn);

            pauseBtn.CheckChanged += pauseBtn_CheckChanged;
            
        }

        void pauseBtn_CheckChanged(object sender, EventArgs e)
        {
            if (pauseBtn.Checked)
            {
                gameMode = BackgroundMode;

                Proxy.GetMode<PauseMenu>().BackgroundMode = gameMode;
                BackgroundMode = Proxy.GetMode<PauseMenu>();
            }
            else
            {
                BackgroundMode = gameMode;
            }

            if (gameMode is PyramidsSolitairMode)
                (gameMode as PyramidsSolitairMode).IsInBackground = pauseBtn.Checked;
        }

        public void Continue()
        {
            pauseBtn.Checked = false;
        }
    }
}
