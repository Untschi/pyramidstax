using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI.Widgets;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PyramidStax.Modes.Menu.Hud
{
    public class PauseMenu : MenuMode
    {
        Button continueBtn, restartBtn, backToMenuBtn;

        public PauseMenu()
            : base(null, Main.Instance.GuiManager, "PauseMenu")
        {
            container.Size = new Vector2(480, 650);
        }

        protected override void Initialize()
        {
            container.Location = new Vector2(0, Globals.AD_HEIGHT);

            int pos = 400;
            continueBtn = AddButton("continue", (o, a) => Continue(), ref pos);

            //restartBtn = AddButton("restart", null, ref pos);

            backToMenuBtn = AddButton("backToMenu", (o, a) => BackToMenu(), ref pos);
        }

        private void Continue()
        {
            Proxy.GetMode<GameGui>().Continue();
        }

        private void Restart()
        {

        }

        private void BackToMenu()
        {
            Continue();
            Proxy.GetMode<GameGui>().BackgroundMode = null;

            if (BackgroundMode is PyramidsSolitairMode)
                (BackgroundMode as PyramidsSolitairMode).Reset();
            //else
            //    (BackgroundMode as Stax.StaxGame).Reset();

            Proxy.SetMode<MainMenu>();
        }

        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

            spriteBatch.Prepare();

            int height = (BackgroundMode is Stax.StaxGame) ? 800 : Globals.TIMER_POS_Y;
            spriteBatch.Draw(Assets.GuiSheet.Texture, new Rectangle(0, 0, 480, height), Assets.ColorDefault.SourceRectangle, Color.White * 0.9f);
            spriteBatch.End();
        }
   

    }
}
