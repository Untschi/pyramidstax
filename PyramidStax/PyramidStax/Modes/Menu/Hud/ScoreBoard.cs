﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI.Widgets;
using starLiGHT.GUI;
using Microsoft.Xna.Framework;
using PyramidStax.Gui;
using PyramidStax.Cards;
using System.Diagnostics;

namespace PyramidStax.Modes.Menu.Hud
{
    public enum ScoreEntryType
    {
        PlayScore,
        CardsLeft,
        TimeBonus,
        Undos,
        Joker,
        Deflector,
        AllCardsFreed,
        Total,
    }

    public class ScoreBoard
    {
        const float LONG_WAIT = 0.5f;
        const float SHORT_WAIT = 0.1f;

        const int SCORE_UNDO = 20;
        const int SCORE_JOKER = 80;
        const int SCORE_DEFLECTOR = 10;
        const int SCORE_CARD_LEFT = 15;
        const int SCORE_SECOND = 2;
        const int SCORE_ALL_FREED = 125;

        PyramidsSolitairMode gameMode;
        public PyramidsSolitairMode GameMode { get { return gameMode; } set { gameMode = value; } }

        public StackPanel Panel { get { return panel; } }
        StackPanel panel;

        Dictionary<ScoreEntryType, ScoreLabel> lookup = new Dictionary<ScoreEntryType, ScoreLabel>();

        ScoreEntryType currentUpdateType;

        float timer;

        public ScoreBoard(GuiManager manager)
        {

            panel = new StackPanel(manager, "ScoreBoard_panel")
            {
                Size = new Vector2(300, 300),
            };
        }

        public void SetupScoreEntries(GameType gameType, bool isGameOver, bool isLevelSolved)
        {
            lookup.Clear();
            panel.Clear();
            currentUpdateType = ScoreEntryType.PlayScore;

            AddScoreLabel(ScoreEntryType.PlayScore, gameMode.Score);

            Panel space1 = new starLiGHT.GUI.Widgets.Panel(panel.Manager, panel.Name + "space1")
            {
                Size = new Vector2(350, 10)
            };
            panel.Widgets.Add(space1);


            if (isLevelSolved)
            {
                AddScoreLabel(ScoreEntryType.CardsLeft, 0, GameMode.Deck.MainStack.CardsLeft.ToString());
                AddScoreLabel(ScoreEntryType.TimeBonus, 0, new TimeSpan(0, 0, (int)GameMode.Timer.TimeLeft).ToString().Remove(0, 4));
            }

            switch (gameType)
            {
                case GameType.PyramidPeaks:
                    if (isGameOver)
                    {

                        AddScoreLabel(ScoreEntryType.Undos, 0, GameMode.Undos.CardsLeft.ToString());

                        //if (isLevelSolved)
                        {
                            AddScoreLabel(ScoreEntryType.Joker, 0, (GameMode as Pyramids.PyramidsGame).Jokers.CardsLeft.ToString());
                        }
                    }
                    break;

                case GameType.Pyramids13:

                    if(isLevelSolved)
                    {
                        AddScoreLabel(ScoreEntryType.AllCardsFreed, 0, (CheckAllCardsFreed()) ? "+" : "-");
                    }

                    if (isGameOver)
                    {
                        AddScoreLabel(ScoreEntryType.Undos, 0, GameMode.Undos.CardsLeft.ToString());

                        //if (isLevelSolved)
                        {
                            AddScoreLabel(ScoreEntryType.Deflector, 0, (GameMode as Pyramids13.Pyramids13Game).GetDeflectorCount().ToString());
                        }
                    }

                    break;

                case GameType.Stax:
                    Debugger.Break();
                    break;
            }

            Panel space2 = new starLiGHT.GUI.Widgets.Panel(panel.Manager, panel.Name + "space2")
            {
                Size = new Vector2(300, 20)
            };
            panel.Widgets.Add(space2);

            AddScoreLabel(ScoreEntryType.Total, gameMode.Score);

        }

        private void AddScoreLabel(ScoreEntryType type, int score)
        {
            AddScoreLabel(type, score, null);
        }

        private void AddScoreLabel(ScoreEntryType type, int score, string prefix)
        {
            ScoreLabel sl = new ScoreLabel(panel.Manager, panel.Name + type.ToString())
            {
                Title = Main.Instance.Loc[type.ToString()],
                Score = score,
                Prefix = prefix,
                Size = new Vector2(300, 30),
            };
            panel.Widgets.Add(sl);

            lookup.Add(type, sl);
        }

        public void IncreaseScore(ScoreEntryType scoretype, int amount)
        {
            if (lookup.ContainsKey(scoretype))
            {
                lookup[scoretype].Score += amount;

                lookup[ScoreEntryType.Total].Score += amount;

                gameMode.Score += amount;
            }
        }

        internal void Update(float t)
        {
            if (currentUpdateType == ScoreEntryType.Total)
                return;

            if (!lookup.ContainsKey(currentUpdateType))
            {
                currentUpdateType = (ScoreEntryType)((int)currentUpdateType + 1);
                return;
            }

            switch (currentUpdateType)
            {
                case ScoreEntryType.PlayScore:
                    timer = LONG_WAIT;
                    currentUpdateType = ScoreEntryType.CardsLeft;
                    break;
                case ScoreEntryType.CardsLeft:

                    if (gameMode.Deck.CardsLeft == 0)
                    {
                        currentUpdateType = ScoreEntryType.TimeBonus;
                        timer = LONG_WAIT;
                    }
                    else if (UpdateTimer(t))
                    {
                        Card c = gameMode.Deck.TakeCard();
                        c.Position = new Vector2(500, c.Position.Y);

                        IncreaseScore(ScoreEntryType.CardsLeft, SCORE_CARD_LEFT);
                    }

                    break;
                case ScoreEntryType.TimeBonus:

                    if (gameMode.Timer.TimeLeft > 0.5f)
                    {
                        gameMode.Timer.Update(0.5f);
                        IncreaseScore(ScoreEntryType.TimeBonus, (int)(0.5f * SCORE_SECOND));
                    }
                    else
                    {
                        currentUpdateType = ScoreEntryType.Undos;
                        timer = LONG_WAIT;
                    }

                    break;
                case ScoreEntryType.Undos:
                    if (gameMode.Undos.CardsLeft == 0)
                    {
                        currentUpdateType = ScoreEntryType.Joker;
                        timer = LONG_WAIT;
                    }
                    else if (UpdateTimer(t))
                    {
                        Card c = gameMode.Undos.TakeCard();
                        c.Position = new Vector2(-100, c.Position.Y);

                        IncreaseScore(ScoreEntryType.Undos, SCORE_UNDO);
                    }
                    break;

                case ScoreEntryType.Joker:                                    
                    if (gameMode is Pyramids.PyramidsGame)
                    {
                        Pyramids.PyramidsGame py = gameMode as Pyramids.PyramidsGame;

                        if (py.Jokers.CardsLeft == 0)
                        {
                            currentUpdateType = ScoreEntryType.Deflector;
                            timer = LONG_WAIT;
                        }
                        else if (UpdateTimer(t))
                        {
                            Card c = py.Jokers.TakeCard();
                            c.Position = new Vector2(-100, c.Position.Y);

                            IncreaseScore(ScoreEntryType.Joker, SCORE_JOKER);
                        }
                    }
                    else
                    {
                        currentUpdateType = ScoreEntryType.Deflector;
                        timer = LONG_WAIT;
                    }
                    break;

                case ScoreEntryType.Deflector:

                    if (UpdateTimer(t))
                    {
                        if (gameMode is Pyramids13.Pyramids13Game)
                        {
                            Pyramids13.Pyramids13Game py13 = gameMode as Pyramids13.Pyramids13Game;

                            CardColor col = (CardColor)0;
                            while (!py13.RemoveDeflector(col) && (int)col < (int)CardColor._Count)
                            {
                                col = (CardColor)((int)col + 1);
                            }

                            if (col != CardColor._Count)
                            {
                                IncreaseScore(ScoreEntryType.Deflector, SCORE_DEFLECTOR);
                            }
                            else
                            {
                                currentUpdateType = ScoreEntryType.AllCardsFreed;
                                timer = LONG_WAIT;
                            }
                        }
                        else
                        {
                            currentUpdateType = ScoreEntryType.AllCardsFreed;
                            timer = LONG_WAIT;
                        }
                    }

                    break;
                case ScoreEntryType.AllCardsFreed:

                    if (CheckAllCardsFreed())
                    {
                        IncreaseScore(ScoreEntryType.AllCardsFreed, SCORE_ALL_FREED);
                        currentUpdateType = ScoreEntryType.Total;
                    }

                    break;
            }
        }

        private bool UpdateTimer(float seconds)
        {
            timer -= seconds;

            if (timer <= 0)
            {
                timer += SHORT_WAIT;
                return true;
            }

            return false;
        }

        public void SkipScoreAnimation()
        {
            while (currentUpdateType != ScoreEntryType.Total)
            {
                if (lookup.ContainsKey(currentUpdateType))
                {
                    switch (currentUpdateType)
                    {
                        case ScoreEntryType.CardsLeft:

                            IncreaseScore(ScoreEntryType.CardsLeft, SCORE_CARD_LEFT * gameMode.Deck.CardsLeft);

                            break;
                        case ScoreEntryType.TimeBonus:

                            IncreaseScore(ScoreEntryType.TimeBonus, (int)(SCORE_SECOND * gameMode.Timer.TimeLeft));
                  
                            break;
                        case ScoreEntryType.Undos:

                            IncreaseScore(ScoreEntryType.Undos, SCORE_UNDO * gameMode.Undos.CardsLeft);

                            break;
                        case ScoreEntryType.Joker:

                            if (gameMode is Pyramids.PyramidsGame)
                            {
                                Pyramids.PyramidsGame py = gameMode as Pyramids.PyramidsGame;
                                IncreaseScore(ScoreEntryType.Joker, SCORE_JOKER * py.Jokers.CardsLeft);
                            }

                            break;

                        case ScoreEntryType.Deflector:

                            if (gameMode is Pyramids13.Pyramids13Game)
                            {
                                Pyramids13.Pyramids13Game py13 = gameMode as Pyramids13.Pyramids13Game;
                                IncreaseScore(ScoreEntryType.Deflector, SCORE_DEFLECTOR * py13.GetDeflectorCount());
                            }

                            break;
                        case ScoreEntryType.AllCardsFreed:

                            if (CheckAllCardsFreed())
                            {
                                IncreaseScore(ScoreEntryType.AllCardsFreed, SCORE_ALL_FREED);
                            }

                            break;
                    }
                }
                currentUpdateType = (ScoreEntryType)((int)currentUpdateType + 1);
            }

        }

        private bool CheckAllCardsFreed()
        {
            if (!(gameMode is Pyramids13.Pyramids13Game))
                return false;

            Pyramids13.Pyramids13Game py13 = gameMode as Pyramids13.Pyramids13Game;

            return py13.Deck.MainStack.CardsLeft == 0
                && py13.DeflectorCardPlace.CardsLeft == 0
                && py13.Pyramid.CardsLeft == 0
                && py13.TurnedCardStacks[0].CardsLeft == 0
                && py13.TurnedCardStacks[1].CardsLeft == 0
                && py13.TurnedCardStacks[2].CardsLeft == 0;
        }
    }
}
