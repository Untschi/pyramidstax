﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI.Widgets;
using starLiGHT.GUI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Util;
using Sunbow.Util.Sprites;
using Sunbow.Util.Data;
using PyramidStax;
using Sunbow.Util.Delegation;

namespace PyramidStax.Modes.Menu
{
    public abstract class MenuMode : OverlayMode
    {
        internal static SpriteSheet menuSheet;

        Dictionary<Widget, string> locTexts = new Dictionary<Widget, string>();

        const int BannerHeight = 170;
        public static int SpacingHeight = 10;
        public static Vector2 DefaultButtonSize = new Vector2(400, 60);

        protected Label title;


        protected ContainerWidget container;

        public string Name { get { return container.Name; } }
        public GuiManager GuiManager { get; private set; }
        public Localization Loc { get { return Main.Instance.Loc; } }

        public Mode PreviousMenu { get; set; }

        protected MenuMode(MenuMode previousMenu, GuiManager guiManager, string panelName)
        {
            this.GuiManager = guiManager;
            this.PreviousMenu = previousMenu;

            container = new ContainerWidget(guiManager, panelName);
            container.Size = new Vector2(guiManager.GraphicsDevice.Viewport.Width, guiManager.GraphicsDevice.Viewport.Height);
            container.Visible = false;
            container.Location = new Vector2(0, BannerHeight);

            title = new Label(guiManager, Name + "_title")
            {
                Text = Loc[Name],
                Size = new Vector2(480, 35),
                TextAlignment = TextAlignment.Center,
                ForeColor = Globals.Dark,
            };
            container.Widgets.Add(title);


            guiManager.Widgets.Add(container);

        }

        protected Button AddButton(string name, EventHandler action, ref int posY)
        {
            int screenWidth = Main.Instance.GraphicsDevice.Viewport.Width;
            int posX = (int)(0.5f * (screenWidth - DefaultButtonSize.X));

            Button btn = CreateButton(name, new Vector2(posX, posY), DefaultButtonSize, action);
            posY += (int)DefaultButtonSize.Y + SpacingHeight;

            return btn;
        }
        protected CheckButton AddCheckButton(string name, EventHandler action, ref int posY)
        {
            int screenWidth = Main.Instance.GraphicsDevice.Viewport.Width;
            int posX = (int)(0.5f * (screenWidth - DefaultButtonSize.X));

            CheckButton btn = CreateCheckButton(name, new Vector2(posX, posY), DefaultButtonSize, action);
            posY += (int)DefaultButtonSize.Y + SpacingHeight;

            return btn;
        }
        protected Label AddLabel(string name, SpriteFont font, TextAlignment align, ref int posY)
        {
            int screenWidth = Main.Instance.GraphicsDevice.Viewport.Width;
            int posX = (int)(0.5f * (screenWidth - DefaultButtonSize.X));

            Label lbl = CreateLabel(name, new Vector2(posX, posY), DefaultButtonSize, font, align, true, Color.White);
            posY += (int)DefaultButtonSize.Y + SpacingHeight;

            return lbl;
        }
        

        protected CheckButton CreateCheckButton(string name, Vector2 position, Vector2 size, EventHandler action)
        {
            return CreateCheckButton(name, position, size, action, "Button", container);
        }
        protected CheckButton CreateCheckButton(string name, Vector2 position, Vector2 size, EventHandler action, string layout, ContainerWidget parent)
        {
            CheckButton btn = new CheckButton(GuiManager, string.Format("{0}_{1}", container.Name, name), layout);
                //btn.Text = Loc[name];
            if (layout == "Button")
            {
                locTexts.Add(btn, name);
                btn.Text = Loc[name];
            }

            btn.Location = position;
            btn.Size = size;
            btn.TextAlignment = TextAlignment.Center;

            //btn.Click += PlayCheckButtonSound;
            btn.CheckChanged += action;
            parent.Widgets.Add(btn);

            return btn;
        }


        protected Button CreateButton(string name, Vector2 position, Vector2 size, EventHandler action)
        {
            return CreateButton(name, position, size, action, "Button");
        }
        protected Button CreateButton(string name, Vector2 position, Vector2 size, EventHandler action, string layout)
        {
            return CreateButton(name, position, size, action, layout, container);
        }
        protected Button CreateButton(string name, Vector2 position, Vector2 size, EventHandler action, string layout, ContainerWidget parent)
        {
            Button btn = new Button(GuiManager, string.Format("{0}_{1}", container.Name, name), layout);
            if (layout == "Button")
            {
                locTexts.Add(btn, name);
                btn.Text = Loc[name];
            }
            btn.Location = position;
            btn.Size = size;
            btn.TextAlignment = TextAlignment.Center;

            //btn.Click += PlayButtonSound;
            btn.Click += action;
            parent.Widgets.Add(btn);

            return btn;
        }
        protected void RegisterWidgetLocId(Widget widget, string locId)
        {
            if (widget == null)
                return;

            if (locTexts.ContainsKey(widget))
                locTexts[widget] = locId;
            else
                locTexts.Add(widget, locId);

            widget.Text = Loc[locId];
        }

        //protected void PlayCheckButtonSound(object sender, EventArgs args)
        //{
        //    BubblebreakSound.Play(BubblebreakSound.LoadBar);
        //}
        //private void PlayButtonSound(object sender, EventArgs args)
        //{
        //    BubblebreakSound.Play(BubblebreakSound.ShootBar);
        //}

        protected Button CreateBackButton()
        {
            return CreateBackButton(0, "BackButton");
        }
        protected Button CreateBackButton(float vShift)
        {
            return CreateBackButton(vShift, "BackButton");
        }
        protected Button CreateBackButton(float vShift, string layout)
        {
            return CreateButton("back", new Vector2(15, 585 + vShift), new Vector2(60, 30), (s, a) => BackButtonPressed(), layout);
        }

        protected Label CreateLabel(string name, Vector2 position, Vector2 size, SpriteFont font, TextAlignment align, bool wrap)
        {
            return CreateLabel(name, position, size, font, align, wrap, Color.White, container);
        }
        protected Label CreateLabel(string name, Vector2 position, Vector2 size, SpriteFont font, TextAlignment align, bool wrap, Color foreColor)
        {
            return CreateLabel(name, position, size, font, align, wrap, foreColor, container);
        }
        protected Label CreateLabel(string name, Vector2 position, Vector2 size, SpriteFont font, TextAlignment align, bool wrap, Color foreColor, ContainerWidget parent)
        {
            Label lbl = new Label(GuiManager, string.Format("{0}_{1}", Name, name))
            {
                Location = position,
                Size = size,
                Font = font,
                TextAlignment = align,
                WrapText = wrap,
                ForeColor = foreColor,
            };
            lbl.Text = Loc[name];
            locTexts.Add(lbl, name);
            parent.Widgets.Add(lbl);
            return lbl;
        }

        protected override void Enter()
        {
            UpdateLocTexts();

            container.Visible = true;

            base.Enter();
        }

        protected void UpdateLocTexts()
        {
            foreach (KeyValuePair<Widget, string> loc in locTexts)
            {
                loc.Key.Text = Loc[loc.Value];
            }
        }

        protected override void Leave()
        {
            container.Visible = false;

        }

        protected override void Update(GameTime gameTime, InputState inputState)
        {
            float sec = gameTime.GetElapsedSeconds();

            PlayerIndex player;
            if (inputState.IsNewButtonPress(Microsoft.Xna.Framework.Input.Buttons.Back, null, out player))
                BackButtonPressed();

            base.Update(gameTime, inputState);
        }

        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);

        }
        protected void DrawMenuBackground(SpriteBatch spriteBatch, bool drawLogo)
        {
            DrawMenuBackground(spriteBatch, drawLogo, 800 - Assets.MenuUpperFrame.SourceRectangle.Height);
        }
        protected void DrawMenuBackground(SpriteBatch spriteBatch, bool drawLogo, int gradiantHeight)
        {
            Assets.GuiSheet.Draw(spriteBatch, Assets.MenuUpperFrame, new Vector2(), 0, 1);

            int h = Assets.MenuUpperFrame.SourceRectangle.Height;
            spriteBatch.Draw(Assets.GuiSheet.Texture, new Rectangle(0, h, 480, gradiantHeight), Assets.GradientColor.SourceRectangle, Color.White);

            if(drawLogo)
                Assets.GuiSheet.Draw(spriteBatch, Assets.SunbowLogo, new Vector2(240, 730), 0, 1);
        }

        protected virtual void BackButtonPressed()
        {
            if(PreviousMenu != null)
                Proxy.SetMode(PreviousMenu.GetType());
        }


    }
}
