﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PyramidStax.Cards;
using PyramidStax.Modes.Feedback;

namespace PyramidStax.Modes.Pyramids13
{
    public class DeflectCardTurn : Turn<Pyramids13Game>
    {
        CardStackInfo cardInfo;

        internal DeflectCardTurn(CardStackInfo card)
            : base()
        {
            this.cardInfo = card;
        }

        public override bool Do(Pyramids13Game game)
        {
            cardInfo.Stack.TakeCard(cardInfo.Card);
            game.DeflectorCardPlace.AddCard(cardInfo.Card);

            game.RemoveDeflector(cardInfo.Card.CardColor);

            return true;
        }

        public override void Undo(Pyramids13Game game)
        {
            game.DeflectorCardPlace.TakeCard(cardInfo.Card);
            cardInfo.Stack.AddCard(cardInfo.Card);

            game.AddDeflector(cardInfo.Card.CardColor);
        }
    }


    public class FreeCardsTurn : Turn<Pyramids13Game>
    {
        const float KING_TIME_BONUS = 30;

        CardStackInfo cardA, cardB;

        internal FreeCardsTurn(CardStackInfo cardInfoA, CardStackInfo cardInfoB)
            : base()
        {
            cardA = cardInfoA;
            cardB = cardInfoB;
        }

        public override bool Do(Pyramids13Game game)
        {
            Do(game, cardA);
            Do(game, cardB);
            game.Pyramid.UpdateBlockings();

            if (cardA.Card != null && cardB.Card != null && cardA.Card.CardColor == cardB.Card.CardColor)
            {
                game.AddDeflector(cardA.Card.CardColor);
            }

            if (cardA.Card != null && cardA.Card.Value == CardValue.King)
            {
                game.HighlightManager.Highlight(new TimeBonusHighlighter(game.HighlightManager));
                game.Timer.GiveTimeBonus(KING_TIME_BONUS);
            }

            return true;
        }

        private void Do(Pyramids13Game game, CardStackInfo cardInfo)
        {
            if (cardInfo.Card != null)
            {
                cardInfo.Stack.TakeCard(cardInfo.Card);
                game.FreedCardStack.AddCard(cardInfo.Card);

                TurnTracker.AddToSeries(cardInfo.Card);
            }
        }

        public override void Undo(Pyramids13Game game)
        {
            Undo(game, cardA);
            Undo(game, cardB);
            game.Pyramid.UpdateBlockings();

            if (cardA.Card != null && cardB.Card != null && cardA.Card.CardColor == cardB.Card.CardColor)
            {
                game.RemoveDeflector(cardA.Card.CardColor);
            }

            if (cardA.Card != null && cardA.Card.Value == CardValue.King)
            {
                game.Timer.GiveTimeBonus(-KING_TIME_BONUS);
            }
        }

        private void Undo(Pyramids13Game game, CardStackInfo cardInfo)
        {
            if (cardInfo.Card != null)
            {
                game.FreedCardStack.TakeCard(cardInfo.Card);
                cardInfo.Stack.AddCard(cardInfo.Card);
                if (cardInfo.Stack is PyramidStack)
                {
                    cardInfo.Card.Position = cardInfo.Position;
                    cardInfo.Card.Rotation = cardInfo.Rotation;
                }

                TurnTracker.RemoveLastFromSeries();
            }
        }
    }


    public class TurnCardsTurn : Turn<Pyramids13Game>
    {
        public override bool Do(Pyramids13Game game)
        {
            if (game.Deck.MainStack.CardsLeft == 0)
                return false;

            TurnTracker.ResetTurnPreview();

            for (int i = 0; i < game.TurnedCardStacks.Length; i++)
            {
                if (game.Deck.MainStack.CardsLeft == 0)
                    break;

                Card c = game.Deck.TakeCard();
                game.TurnedCardStacks[i].AddCard(c);

            }

            TurnTracker.ClearSeries();
            return true;
        }

        public override void Undo(Pyramids13Game game)
        {
            
            for (int i = 0; i < game.TurnedCardStacks.Length; i++)
            {
                if (game.Deck.MainStack.CardsLeft == 0)
                    break;

                Card c = game.TurnedCardStacks[i].TakeCard();
                game.Deck.MainStack.AddCard(c);
            }
        }
    }

}
