﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PyramidStax.Cards;
using Microsoft.Xna.Framework;
using Sunbow.Util.Sprites;
using PyramidStax.Modes.Feedback;

namespace PyramidStax.Modes.Pyramids13
{
    public class TurnTracker : TurnTracker<Pyramids13Game>
    {
        enum SpecialHighlightType
        {
            None = 0,
            Undo = 1,
            SimpleDeflector = 2,
            TimeBonus = 4,
        }


        public TurnTracker(Pyramids13Game game)
            : base(game)
        {
        }
        internal override void ResetTurnPreview()
        {
            for (int i = 0; i < turnPreview.Length; i++)
            {
                //if (i == 1)
                //    turnPreview[i] = (int)SpecialHighlightType.SimpleDeflector;
                //else
                if ((i + 1) % 10 == 0)
                    turnPreview[i] = (int)SpecialHighlightType.Undo;
                else
                    turnPreview[i] = (int)SpecialHighlightType.None;
            }
        }

        internal override void CheckSeries()
        {
            if(lastSeries.Count <= 1)
                return;

            //if (!game.IsSimpleDeflectorActive && lastSeries.Count >= 4)             // Simple Deflector
            //{
            //    List<CardColor> colors = new List<CardColor>();
            //    int firstIndex;
            //    for (firstIndex = lastSeries.Count - 1; firstIndex >= lastSeries.Count - 4; firstIndex--)
            //    {
            //        Card c = lastSeries[firstIndex];
            //        if (c.CardColor == CardColor.SpecialCardBlack || c.CardColor == CardColor.SpecialCardRed)
            //            break;

            //        if (turnPreview[firstIndex] == (int)SpecialHighlightType.SimpleDeflector)
            //            turnPreview[firstIndex] = (int)SpecialHighlightType.None;

            //        if (!colors.Contains(c.CardColor))
            //            colors.Add(c.CardColor);
            //        else
            //            break;
            //    }

            //    turnPreview[firstIndex + 4] = (int)SpecialHighlightType.SimpleDeflector;

            //    if (firstIndex == lastSeries.Count - 4)
            //    {
            //        game.IsSimpleDeflectorActive = true;    
            //    }
            //}

            //if (lastSeries.Last().Value != CardValue.King)
            //{
            //    if (lastSeries[lastSeries.Count - 1].CardColor == lastSeries[lastSeries.Count - 2].CardColor)
            //    {
            //        game.IsSimpleDeflectorActive = true;
            //    }
            //}


            if (lastSeries.Count % 10 == 0)                      // Undo Card
            {
                CardColor color = (Sunbow.Util.Random.Next(2) == 0) ? CardColor.SpecialCardBlack : CardColor.SpecialCardRed;
                game.HighlightManager.Highlight(new UndoHighlighter(game.HighlightManager, game, color));

                //Card undo = new Card(game.Deck, color, CardValue.Undo);
                //undo.Position = KilledUndoCardPosition;
                //game.Undos.AddCard(undo);
            }
        }

        public override void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch batch)
        {
            Assets.GuiSheet.Draw(batch, Assets.TurnBackgroundFrame, new Vector2(0, Globals.TURNER_POS_Y), 0, 1);

            for (int i = 0; i < turnPreview.Length; i++)
            {
                Vector2 pos = new Vector2(Assets.TurnFrameNormal.SourceRectangle.Width * i, Globals.TIMER_POS_Y);

                if (i < lastSeries.Count)
                {
                    pos = new Vector2(pos.X, pos.Y + Globals.TIMER_HEIGHT);
                }

                Frame frame = (turnPreview[i] == (int)SpecialHighlightType.None) ? Assets.TurnFrameNormal : Assets.TurnFrameHighlight;
                Assets.GuiSheet.Draw(batch, frame, pos, 0, 1);

                if (i < lastSeries.Count)
                {
                    CardColor col = lastSeries[i].CardColor;
                    int frameIdx = (lastSeries[i].Value == CardValue.Joker) ? 4 : (int)col;
                    Assets.GuiSheet.Draw(batch, Assets.CardColorSymbols.CalculateFrame(frameIdx), pos, 0, 1, col.GetFillColor());
                    Assets.GuiSheet.Draw(batch, Assets.CardColorSymbols.CalculateFrame(frameIdx + 5), pos, 0, 1, col.GetOutlineColor());
                }

                if (turnPreview[i] == (int)SpecialHighlightType.SimpleDeflector)
                {
                    List<CardColor> cols = new List<CardColor>() { CardColor.Spades, CardColor.Heart, CardColor.Cross, CardColor.Diamonds, };
                    for (int j = lastSeries.Count - 1; j >= lastSeries.Count - 4 && j >= 0; j--)
                    {
                        if(cols.Contains(lastSeries[j].CardColor))
                            break;
                        else
                            cols.Remove(lastSeries[j].CardColor);
                    }
                    if (i <= lastSeries.Count + cols.Count)
                    {
                        for (int j = lastSeries.Count; j <= i; j++)
                        {
                            int idx = j - lastSeries.Count;
                            Vector2 p = new Vector2(Assets.TurnFrameNormal.SourceRectangle.Width * j, Globals.TIMER_POS_Y + Globals.TIMER_HEIGHT + 5);
                            Assets.GuiSheet.Draw(batch, Assets.CardColorSymbols.CalculateFrame((int)cols[idx] + 5), p, 0, 1, cols[idx].GetFillColor());
                        }
                    }
                }
                else if (turnPreview[i] == (int)SpecialHighlightType.TimeBonus && i == lastSeries.Count)
                {
                    CardColor col = lastSeries[i - 1].CardColor;
                    pos = new Vector2(pos.X, pos.Y + Globals.TIMER_HEIGHT + 5);
                    Assets.GuiSheet.Draw(batch, Assets.CardColorSymbols.CalculateFrame((int)col + 5), pos, 0, 1, col.GetFillColor());
                }
            }
        }

        protected override Microsoft.Xna.Framework.Vector2 KilledUndoCardPosition { get { return game.FreedCardStack.Position; } }
    }
}
