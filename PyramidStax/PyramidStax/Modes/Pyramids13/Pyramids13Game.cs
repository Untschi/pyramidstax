﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util;
using Sunbow.Util.Delegation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PyramidStax.Cards;
using Microsoft.Xna.Framework.Input;
using Sunbow.Util.IO;
using PyramidStax.Modes.Menu.Hud;
using PyramidStax.Modes.Pyramids;
using PyramidStax.Modes.Feedback;

namespace PyramidStax.Modes.Pyramids13
{
    internal struct CardStackInfo
    {
        internal bool IsSelected;
        internal Card Card;
        internal CardCollection Stack;
        internal Vector2 Position;
        public Shapes.Geometry.Angle Rotation;


        internal CardStackInfo(CardCollection stack, Card card)
        {
            this.Stack = stack;
            this.Card = card;
            this.Position = card.Position;
            this.Rotation = card.Rotation;
            this.IsSelected = true;
        }

    }

    public class Pyramids13Game : PyramidsSolitairMode
    {
        public override Deck Deck { get { return deck; } }
        public PyramidStack Pyramid { get; private set; }
        public Stack[] TurnedCardStacks { get; private set; }
        public Stack DeflectorCardPlace { get; private set; }
        public Stack FreedCardStack { get; private set; }

        public override Stack Undos { get { return undos; } }
        Stack undos;
        Deck deck;


        public SpecialHighlightManager HighlightManager { get; private set; }
        CardSelectionHighlighter selector = new CardSelectionHighlighter();

        //public bool IsSimpleDeflectorActive { get { return isSimpleDeflectorActive; } set { isSimpleDeflectorActive = value; } }
        //bool isSimpleDeflectorActive;
        //bool isExtendedDeflectorActive;

        Dictionary<CardColor, int> deflectors = new Dictionary<CardColor, int>();

        internal CardStackInfo SelectedCard { get; private set; }

        TurnTracker tracker;

        public Pyramids13Game()
        { }

        protected override void Initialize()
        {
            deflectors.Add(CardColor.Cross, 0);
            deflectors.Add(CardColor.Diamonds, 0);
            deflectors.Add(CardColor.Heart, 0);
            deflectors.Add(CardColor.Spades, 0);
                

            deck = new Deck(1, false, true, new Vector2(50, 715));
            deck.CardMover.DefaultWaitTime = 0.15f;

            FreedCardStack = new Stack(deck) { Position = new Vector2(-100, 700) };

            TurnedCardStacks = new Stack[]
                {
                    new Stack(deck) { Position = new Vector2(130, 750), Hidden = false, Orderly = true, CardOnTopAtPosition = true, PositionOffest = new Vector2(0, -20), MinPositionOffset = new Vector2(0, -40), },
                    new Stack(deck) { Position = new Vector2(205, 750), Hidden = false, Orderly = true, CardOnTopAtPosition = true, PositionOffest = new Vector2(0, -20), MinPositionOffset = new Vector2(0, -40), },
                    new Stack(deck) { Position = new Vector2(280, 750), Hidden = false, Orderly = true, CardOnTopAtPosition = true, PositionOffest = new Vector2(0, -20), MinPositionOffset = new Vector2(0, -40), },
                };
            DeflectorCardPlace = new Stack(deck) { Position = new Vector2(360, 749) };
            undos = new Stack(deck)
            {
                Position = new Vector2(440, 720),
                PositionOffest = new Vector2(1, 4),
                CardOnTopAtPosition = true,
                Orderly = true,
                Hidden = false,
            };

            Pyramid = new PyramidStack(deck) { HideBlockedCards = false };

            SelectedCard = new CardStackInfo() { IsSelected = false };

            tracker = new TurnTracker(this);

            HighlightManager = new SpecialHighlightManager();
        }
        public override void Reset()
        {
            deflectors[CardColor.Cross] = 0;
            deflectors[CardColor.Diamonds] = 0;
            deflectors[CardColor.Heart] = 0;
            deflectors[CardColor.Spades] = 0;

            undos.Clear();

            base.Reset();
        }

        protected override void Enter()
        {
        }

        protected override void Leave()
        {

        }

        public override void StartNewLevel()
        {
            base.StartNewLevel();

            LoadLevel(Main.Instance.GameSettings.CurrentLevelData);
            tracker.ClearSeries();

            undos.AddCard(new Card(deck, CardColor.SpecialCardBlack, CardValue.Undo));
        }

        protected override void Update(GameTime gameTime, InputState inputState)
        {
            base.Update(gameTime, inputState);
            
            float seconds = gameTime.GetElapsedSeconds();

            HighlightManager.Update(seconds);
            deck.Update(seconds);

            if (IsInBackground || !UserControlEnabled)
                return;

            selector.Update(seconds);

            Timer.Update(seconds);

            if (inputState.CurrentMouseState.LeftButton == ButtonState.Pressed
                && inputState.LastMouseState.LeftButton == ButtonState.Released)
            {
                Vector2 mousePosition = inputState.CurrentMouseState.GetPosition();

                if (deck.MainStack.Bounds.IsPointInside(mousePosition))          // DECK
                {
                    TurnCards();
                }
                else
                {
                    Card c = Pyramid.SelectCard(mousePosition);                                 // PYRAMID
                    if (c != null)
                    {
                        SelectCard(Pyramid, c);
                    }
                    else
                    {
                        foreach (Stack s in TurnedCardStacks)                                   // Stacks
                        {
                            if (s.CardsLeft > 0 && s.Bounds.IsPointInside(mousePosition))
                            {
                                c = s.CardOnTop;
                                SelectCard(s, c);
                                break;
                            }
                        }

                        if (DeflectorCardPlace.Bounds.IsPointInside(mousePosition))             // Temp Place
                        {
                            if (DeflectorCardPlace.CardsLeft > 0)
                            {
                                SelectCard(DeflectorCardPlace, DeflectorCardPlace.CardOnTop);
                            }
                            else if(SelectedCard.IsSelected)
                            {
                                if (deflectors[SelectedCard.Card.CardColor] > 0)
                                {
                                    tracker.Do(new DeflectCardTurn(SelectedCard));

                                    selector.RemoveHighlight(SelectedCard.Card);

                                    SelectedCard = new CardStackInfo() { IsSelected = false };
                                    Pyramid.UpdateBlockings();
                                }

                            }
                        }
                        else if (undos.Bounds.IsPointInside(mousePosition))
                        {
                            tracker.Undo();
                        }
                    }

                    if (CheckWin())
                    {
                        deck.CardMover.AddCardEvent(c, (p) => LevelSolved(), Globals.LevelOverWaitDuration, true);
                    }
                    else if (CheckGameOver())
                    {
                        deck.CardMover.AddCardEvent(c, (p) => GameOver(), Globals.LevelOverWaitDuration, true);
                    }

                }
            }

            if (inputState.IsNewKeyPress(Keys.R) && inputState.CurrentKeyboardStates[0].IsKeyDown(Keys.LeftControl))
            {
                LoadLevel(Main.Instance.GameSettings.CurrentLevelData);
            }

        }

        private void TurnCards()
        {
            selector.Clear();
            SelectedCard = new CardStackInfo() { IsSelected = false };

            tracker.Do(new TurnCardsTurn());
        }

        private void SelectCard(CardCollection stack, Card card)
        {
            if (card.Value == CardValue.King)
            {
                //stack.TakeCard(card);
                //FreedCardStack.AddCard(card);
                //Pyramid.UpdateBlockings();
                tracker.Do(new FreeCardsTurn(new CardStackInfo(stack, card), new CardStackInfo()));
                return;
            }

            if (SelectedCard.IsSelected)
            {
                if (SelectedCard.Card == card)
                {
                    selector.RemoveHighlight(SelectedCard.Card);
                    SelectedCard = new CardStackInfo() { IsSelected = false };
                }
                else if ((int)SelectedCard.Card.Value + (int)card.Value == 13)
                {
                    //stack.TakeCard(card);
                    //FreedCardStack.AddCard(card);

                    //SelectedCard.Stack.TakeCard(SelectedCard.Card);
                    //FreedCardStack.AddCard(SelectedCard.Card);
                    tracker.Do(new FreeCardsTurn(SelectedCard, new CardStackInfo(stack, card)));

                    selector.RemoveHighlight(SelectedCard.Card);
                    SelectedCard = new CardStackInfo() { IsSelected = false };

                    Pyramid.UpdateBlockings();
                }
                else
                {
                    selector.RemoveHighlight(SelectedCard.Card);
                    selector.FadeHighlight(SelectedCard.Card, 0.333f, false);
                    selector.Highlight(card);
                    SelectedCard = new CardStackInfo(stack, card);
                }
            }
            else
            {
                selector.RemoveHighlight(SelectedCard.Card);
                selector.Highlight(card);
                SelectedCard = new CardStackInfo(stack, card);
            }
        }

        protected override void Draw(SpriteBatch spriteBatch)
        {
    
            spriteBatch.Prepare();

            Assets.GuiSheet.Draw(spriteBatch, Assets.DeflectorFrame, DeflectorCardPlace.Position, 0, 1);

            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(0), DeflectorCardPlace.Position - new Vector2(40, 65), 0, 1, Color.Black);
            spriteBatch.DrawString(Assets.FontMedium, deflectors[CardColor.Cross].ToString(), DeflectorCardPlace.Position - new Vector2(20, 70), Color.Black);

            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(1), DeflectorCardPlace.Position - new Vector2(0, 65), 0, 1, Color.Black); 
            spriteBatch.DrawString(Assets.FontMedium, deflectors[CardColor.Spades].ToString(), DeflectorCardPlace.Position - new Vector2(-20, 70), Color.Black);

            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(2), DeflectorCardPlace.Position - new Vector2(40, 83), 0, 1, Color.Red);
            spriteBatch.DrawString(Assets.FontMedium, deflectors[CardColor.Heart].ToString(), DeflectorCardPlace.Position - new Vector2(20, 88), Color.Red);

            Assets.GuiSheet.Draw(spriteBatch, Assets.CardColorSymbols.CalculateFrame(3), DeflectorCardPlace.Position - new Vector2(0, 83), 0, 1, Color.Red);
            spriteBatch.DrawString(Assets.FontMedium, deflectors[CardColor.Diamonds].ToString(), DeflectorCardPlace.Position - new Vector2(-20, 88), Color.Red);

            deck.MainStack.Draw(spriteBatch);

            undos.Draw(spriteBatch);

            foreach(Stack s in TurnedCardStacks)
                s.Draw(spriteBatch);

            DeflectorCardPlace.Draw(spriteBatch);

            Pyramid.Draw(spriteBatch);

            HighlightManager.Draw(spriteBatch);
            tracker.Draw(spriteBatch);
            Timer.Draw(spriteBatch);

            spriteBatch.DrawString(Assets.FontSmall, Score.ToString("### ##0"), new Vector2(80 - scoreSize.X, 2 + Globals.TIMER_POS_Y), Globals.VeryDark);


            if (Deck.CardsLeft > 0 && UserControlEnabled)
            {
                Vector2 size = Assets.FontSmall.MeasureString(Deck.CardsLeft.ToString());
                spriteBatch.DrawString(Assets.FontSmall, Deck.CardsLeft.ToString(), Deck.MainStack.Position - 0.5f * size + new Vector2(3, 2), Color.DarkOrange);
            }

            deck.DrawMovingCards(spriteBatch);

            DrawTurnedStackInfo(spriteBatch);

            selector.Draw(spriteBatch);

            spriteBatch.End();
        }


        public void LoadLevel(Table levelData)
        {
            UserControlEnabled = false;

            deck.CollectAndShuffle();

            foreach(Stack s in TurnedCardStacks)
                s.Clear();

            DeflectorCardPlace.Clear();
            Pyramid.Clear();

            Timer.Reset();


            using (new CardMoveTimer(deck.CardMover, false))
            {
                //deck.CardMover.AddCardEvent(null, (c1) => 
                //{ 

                Pyramid.HideBlockedCards = false;
                deck.CardMover.RegisterMovingStack(Pyramid);
                PersistHelper.LoadPyramidStack(levelData, deck, Pyramid);

                deck.CardMover.AddCardEvent(null,
                    (c2) =>
                    {
                        using (new CardMoveTimer(deck.CardMover, true))
                        {
                            deck.CardMover.RegisterMovingStack(Pyramid);
                            Pyramid.HideBlockedCards = false;
                            Pyramid.UpdateBlockings();

                            TurnCards();
                            //TurnedCards.AddCard(deck.TakeCard());
                            Timer.Start(Main.Instance.GameSettings.LevelDuration);
                            UserControlEnabled = true;
                        }
                    }, 1);
                //tracker.Reset();

                //}, 1);
            }
        }

        public void AddDeflector(CardColor color)
        {
            deflectors[color]++;

            
            HighlightManager.Highlight(new DeflectorHighlighter(HighlightManager, color));
        }

        public bool RemoveDeflector(CardColor color)
        {
            if (!deflectors.ContainsKey(color) || deflectors[color] <= 0)
                return false;

            deflectors[color]--;
            return true;

            //TODO: visualize
        }

        private bool CheckGameOver()
        {
            if (Timer.TimeLeft > 0)
            {
                if (deck.MainStack.CardsLeft > 0)
                    return false;

                foreach (Card c1 in GetAccessibleCards())
                {
                    foreach (Card c2 in GetAccessibleCards())
                    {
                        if (c1 == c2)
                            continue;

                        if (c1.Value == CardValue.King || (int)c1.Value + (int)c2.Value == 13)
                            return false;
                    }
                }

                if (DeflectorCardPlace.CardsLeft == 0)
                {
                    foreach (Card c in GetAccessibleCards())
                    {
                        if (deflectors[c.CardColor] > 0)
                            return false;
                    }
                }
            }

            Timer.SetPlayState(false);
            UserControlEnabled = false;

            backgroundColor.Interpolator.EndValue = Globals.FailColor;
            backgroundColor.Animator.IsPaused = false;

            return true;
        }

        private bool CheckWin()
        {
            bool win = Pyramid.CardsLeft == 0;

            if (win)
            {
                Timer.SetPlayState(false);
                UserControlEnabled = false;

                backgroundColor.Interpolator.EndValue = Globals.SuccessColor;
                backgroundColor.Animator.IsPaused = false;
            }
            return win;
        }

        private IEnumerable<Card> GetAccessibleCards()
        {
            foreach (Card c in Pyramid.GetCardsOnTop())
                yield return c;

            for (int i = 0; i < TurnedCardStacks.Length; i++)
                if (TurnedCardStacks[i].CardsLeft > 0)
                    yield return TurnedCardStacks[i].CardOnTop;

            if (DeflectorCardPlace.CardsLeft > 0)
                yield return DeflectorCardPlace.CardOnTop;
        }

        internal override void LevelSolved()
        {
            Proxy.GetMode<LevelOverScreen>().GameIsOver = !Main.Instance.GameSettings.NextLevel();
            Proxy.GetMode<LevelOverScreen>().LevelSolved = true;
            Proxy.GetMode<LevelOverScreen>().BackgroundMode = this;
            Proxy.SetMode<LevelOverScreen>();
        }

        internal override void GameOver()
        {
            if (!CheckGameOver())
                return;

            Proxy.GetMode<LevelOverScreen>().GameIsOver = true;
            Proxy.GetMode<LevelOverScreen>().LevelSolved = false;
            Proxy.GetMode<LevelOverScreen>().BackgroundMode = this;
            Proxy.SetMode<LevelOverScreen>();
        }

        internal int GetDeflectorCount()
        {
            return deflectors[CardColor.Cross] + deflectors[CardColor.Diamonds] + deflectors[CardColor.Heart] + deflectors[CardColor.Spades];
        }

        protected void DrawTurnedStackInfo(SpriteBatch batch)
        {
            foreach (Stack s in TurnedCardStacks)
            {
                // Draw stack count
                Vector2 pos = s.Position + new Vector2(20, -69);
                Assets.GuiSheet.Draw(batch, Assets.RoundCountFrame, pos, 0, 1, Color.White * 0.666f);
                pos -= 0.5f * Assets.FontMedium.MeasureString(s.CardsLeft.ToString());
                batch.DrawString(Assets.FontMedium, s.CardsLeft.ToString(), pos, Globals.Dark);


                if (s.CardsLeft == 0)
                    continue;

                // Draw color
                for (int i = Math.Max(0, s.Cards.Count - 3); i < s.Cards.Count - 1; i++)
                {
                    Card c = s.Cards[i];
                    int frameIdx = (int)c.CardColor;
                    Assets.GuiSheet.Draw(batch, Assets.CardColorSymbols.CalculateFrame(frameIdx), c.Position + new Vector2(-18, -48), c.Rotation.ClockwiseRadians, c.Scale, c.CardColor.GetFillColor());
                }

            }
        }
    }
}
