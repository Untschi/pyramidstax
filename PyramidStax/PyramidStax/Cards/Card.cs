﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Shapes.Geometry;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Sunbow.Util.Manipulation;
using Sunbow.Util.Sprites;

namespace PyramidStax.Cards
{
    public class Card : ITransformable2D
    {
        public const int CARD_BACK_IDX = 52;
        public const int JOKER_BLACK_IDX = 53;
        public const int JOKER_RED_IDX = 54;
        public const int UNDO_RED_IDX = 55;
        public const int UNDO_BLACK_IDX = 56;
        public const int PLACEHOLDER_IDX = 57;
        public const int HIGHLIGHTER_IDX = 58;

        public const float CardMoveDuration = 0.333f;

        public static bool ForceTransform { get; set; }
        public static bool ForceWaitForTurnTransform { get; set; }

        struct TurnInfo
        {
            public bool Hidden { get; set; }
            public float ScaleH { get; set; }

            public static TurnInfo Lerp(TurnInfo a, TurnInfo b, float amount)
            {
                if (a.Hidden == b.Hidden)
                    return a;

                TurnInfo c = new TurnInfo();
                c.Hidden = (amount < 0.5f) ? a.Hidden : b.Hidden;
                c.ScaleH = MathHelper.Lerp(0, 1, 2 * Math.Abs(amount - 0.5f));
                return c;
            }
        }

        public Deck Deck { get; private set; }
        public bool IsMoving { get { return (!animator.IsPaused || animator.Value < 1); } }
        //positionChanger.CurrentValue != Position || rotationChanger.CurrentValue != Rotation || Math.Abs(turner.CurrentValue.ScaleH) < 1 || !animator.IsPaused; } }//

        internal bool IsRenderedByMover { get; private set; }

        public CardColor CardColor { get; private set; }
        public CardValue Value { get; private set; }

        public Color DrawColor { get; set; }

        public bool IsHidden { get { return isHidden; } 
            set {
                if (ForceTransform)
                {
                    isHidden = value;
                }
                else
                {
                    if (value != isHidden)
                    {
                        turner.SetValues(new TurnInfo() { Hidden = isHidden }, new TurnInfo() { Hidden = value });
                        Deck.CardMover.AddCard(this);

                        IsRenderedByMover = true;
                    }
                    else
                    {
                        turner.SetValues(new TurnInfo() { Hidden = value, ScaleH = 1 });
                    }
                }
            } 
        }
        bool isHidden;

        public bool IsBlocked { get; set; }

        public Rect Bounds { get; private set; }

        TwoValueInterpolator<Vector2> positionChanger = new TwoValueInterpolator<Vector2>(Vector2.Lerp);
        TwoValueInterpolator<float> rotationChanger = new TwoValueInterpolator<float>(MathHelper.Lerp);
        TwoValueInterpolator<TurnInfo> turner = new TwoValueInterpolator<TurnInfo>(TurnInfo.Lerp);

        SinusSingleAnimator animator = new SinusSingleAnimator(CardMoveDuration);


        private int FrameIndex 
        {
            get
            {
                if (IsPlaceholder())
                    return PLACEHOLDER_IDX;

                if (isHidden)
                    return CARD_BACK_IDX;

                return GetFrameIndex(Value, CardColor);
            } 
        }

        public static int GetFrameIndex(CardValue value, CardColor color)
        {
            if (value == CardValue.Joker)
                return (color == CardColor.SpecialCardBlack) ? JOKER_BLACK_IDX : JOKER_RED_IDX;

            if (value == CardValue.Undo)
                return (color == CardColor.SpecialCardBlack) ? UNDO_BLACK_IDX : UNDO_RED_IDX;


            return (int)color * ((int)CardValue._Count - 1) + (int)value - 1; 
        }

        public Card(Deck deck, CardColor color, CardValue value)
        {
            this.Deck = deck;
            this.CardColor = color;
            this.Value = value;
            this.DrawColor = Color.White;

            Bounds = new Rect(Assets.CardFrames.FrameWidth - 4, Assets.CardFrames.FrameHeight - 4);
            Bounds.Origin = Assets.CardFrames.FrameOrigin - new Vector2(2, 2);
            animator.Pause();

            isHidden = true;
            //turner.CurrentValue = new TurnInfo() { ScaleH = 1, Hidden = IsHidden ):
        }

        public void Update(float seconds)
        {
            if (animator.IsPaused)
                return;

            animator.Update(seconds);
            Bounds.Position = positionChanger.Lerp(animator);
            Bounds.Rotation = -rotationChanger.Lerp(animator);
            Bounds.Scale = new Vector2(turner.Lerp(animator).ScaleH, 1);
            isHidden = turner.CurrentValue.Hidden;

            IsRenderedByMover = true;
        }
        
        public void Draw(SpriteBatch batch)
        {
            {
                Assets.Cards.Draw(batch, Assets.CardFrames.CalculateFrame(FrameIndex),
                    positionChanger.CurrentValue, rotationChanger.CurrentValue, Bounds.Scale, DrawColor);
            }
        }


        public void MovingStopped()
        {
            IsRenderedByMover = false;
            positionChanger.SetValues(Bounds.Position);
            rotationChanger.SetValues(Bounds.Rotation.ClockwiseRadians);

            //if ( turner.CurrentValue.Hidden != isHidden)
            //{
                turner.SetValues(new TurnInfo() { Hidden = isHidden, ScaleH = 1 });
                animator.Reset();

            //}
        }
        public void StartMoving()
        {
            animator.Reset();
            animator.Play();
        }

        #region ITransformable2D members

        public Vector2 Position
        {
            get
            {
                return Bounds.Position;
            }
            set
            {
                if (!ForceTransform)
                {
                    //positionChanger.SetValues(Bounds.Position, value);
                    positionChanger.SetValues(positionChanger.CurrentValue, value);
                    Deck.CardMover.AddCard(this);
                }
                else
                {
                    positionChanger.SetValues(value);
                }
                Bounds.Position = value;
            }
        }

        public Vector2 Origin
        {
            get
            {
                return Bounds.Origin;
            }
            set
            {
                throw new NotImplementedException("origin of shape and frame must be the same");
            }
        }

        public Vector2 Scale
        {
            get
            {
                return Bounds.Scale;
            }
            set
            {
                Bounds.Scale = value;
            }
        }

        public Angle Rotation
        {
            get
            {
                return Bounds.Rotation;
            }
            set
            {
                if (!ForceTransform)
                {
                    //rotationChanger.SetValues(Bounds.Rotation.ClockwiseRadians, value.ClockwiseRadians);
                    rotationChanger.SetValues(rotationChanger.CurrentValue, value.ClockwiseRadians);
                    Deck.CardMover.AddCard(this);
                }
                else
                {
                    rotationChanger.SetValues(value.ClockwiseRadians);
                }
                Bounds.Rotation = value;
            }
        }
        #endregion

        public override string ToString()
        {
            return string.Format("card: {0} {1}", CardColor.ToString(), Value.ToString());
        }


        public bool IsPlaceholder()
        {
            return CardColor == Cards.CardColor.undefined && Value == CardValue.undefined;
        }

    }
}
