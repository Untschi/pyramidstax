﻿//using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Shapes.Geometry;
using Sunbow.Util;
using Microsoft.Xna.Framework.Graphics;
using PyramidStax.Modes.Pyramids;
using System;

namespace PyramidStax.Cards
{
    public class Stack : CardCollection
    {
        const float BOUNDS_OVERLAP = 5;

        public Vector2 Position { get { return position; } set { position = value; UpdateCards(); } }
        Vector2 position;
        public Vector2 PositionOffest { get; set; }

        public Angle Orientation { get { return Bounds.Rotation; } set { Bounds.Rotation = value; } }
        public Angle OrientationOffset { get; set; }

        public bool Orderly { get; set; }

        public bool Hidden { get; set; }

        public override int CardsLeft { get { return cards.Count; } }
        public Card CardOnTop { get { return (CardsLeft == 0) ? null : cards[cards.Count - 1]; } }

        public bool CardOnTopAtPosition { get; set; }

        public Rect Bounds { get; private set; }

        public Vector2 MaxPositionOffset { get; set; }
        public Vector2 MinPositionOffset { get; set; }

        public event Action LastCardTaken;

        public Stack(Deck deck)
            : base(deck)
        {
            Bounds = new Rect(Assets.CardFrames.FrameWidth + BOUNDS_OVERLAP, Assets.CardFrames.FrameHeight + BOUNDS_OVERLAP);
            Bounds.Origin = Assets.CardFrames.FrameOrigin;
        }

        public override void AddCard(Card card)
        {
            cards.Add(card);
            SetupCard(card, CardsLeft, !(Orderly && CardOnTopAtPosition));// || Deck.CardMover.MoveImmediately);
            Commit();
        }
        private void SetupCard(Card card, int cardNumber, bool updateTransform)
        {
            Vector2 offset = (Orderly) ? PositionOffest * cardNumber
                : new Vector2(Sunbow.Util.Random.NextFloat(-PositionOffest.X, PositionOffest.X), Sunbow.Util.Random.NextFloat(-PositionOffest.Y, PositionOffest.Y));
            Angle oriOffset = (Orderly) ? OrientationOffset * cardNumber
                : Sunbow.Util.Random.NextFloat(-OrientationOffset.Radians, OrientationOffset.Radians);

            CutOffset(ref offset);

            if (updateTransform)
            {
                card.Position = Position + offset;
                card.Rotation = Orientation + oriOffset;
            }
            card.IsHidden = Hidden;

        }

        private void Commit()
        {
            if (Orderly && CardOnTopAtPosition)
                MakeOrderly();
            else
                UpdateBounds();

            if (CardsLeft == 0 && LastCardTaken != null)
                LastCardTaken();
        }
        public void MakeOrderly()
        {
            MakeOrderly(PositionOffest, OrientationOffset.ClockwiseRadians);
        }
        public void MakeOrderly(Vector2 positionOffset, float rotationOffset)
        {
            Deck.CardMover.RegisterMovingStack(this);
            using (new CardMoveTimer(Deck.CardMover, true))
            {
                for (int i = 0; i < cards.Count; i++)
                {
                    float mul = (CardOnTopAtPosition) ? cards.Count - 1 - i : i;
                    Vector2 offset = positionOffset * mul;
                    Angle oriOffset = rotationOffset * mul;

                    CutOffset(ref offset);

                    cards[i].Position = Position + offset;
                    cards[i].Rotation = Orientation + oriOffset;
                }
                UpdateBounds();
            }
        }

        private void CutOffset(ref Vector2 offset)
        {
            if (MinPositionOffset != Vector2.Zero)
            {
                offset.X = Math.Max(offset.X, MinPositionOffset.X);
                offset.Y = Math.Max(offset.Y, MinPositionOffset.Y);
            }
            if (MaxPositionOffset != Vector2.Zero)
            {
                offset.X = Math.Min(offset.X, MaxPositionOffset.X);
                offset.Y = Math.Min(offset.Y, MaxPositionOffset.Y);
            }
        }
        public override void TakeCard(Card card)
        {
            cards.Remove(card);

            Commit();
        }

        public override Card TakeCard()
        {
            if (CardsLeft == 0)
                return null;

            Card c = cards[cards.Count - 1];
            cards.RemoveAt(cards.Count - 1);

            Commit();

            return c;
        }

        public void Clear()
        {
            cards.Clear();
        }


        private void UpdateCards()
        {
            for(int i = 0; i < cards.Count; i++)
                SetupCard(cards[i], i, true);

            UpdateBounds();
        }
        public void UpdateBounds()
        {
            float boundsAddX = (Orderly)
                ? Math.Abs(CardsLeft * PositionOffest.X)
                : Math.Abs(2 * PositionOffest.X);
            float boundsAddY = (Orderly)
                ? Math.Abs(CardsLeft * PositionOffest.Y)
                : Math.Abs(2 * PositionOffest.Y);

            Vector2 boundsAdd = new Vector2(boundsAddX, boundsAddY);

            Vector2 posOff = (Orderly)
                ? CardsLeft * PositionOffest
                : -0.5f * boundsAdd;

            Bounds.ChangeDimension(Globals.CardTemplate.Bounds.Width + boundsAdd.X, Globals.CardTemplate.Bounds.Height + boundsAdd.Y);

            Bounds.Position = this.position + posOff;
        }

        public void FanOut(Vector2 rotationCenter, float rotationMid, float centerDistance, float cardDistance, int maxCardsInRow)
        {
            using (new CardMoveTimer(Deck.CardMover, true))
            {
                int rowCount = (int)Math.Ceiling((float)CardsLeft / maxCardsInRow);
                int cardsInRow = (int)Math.Ceiling((float)CardsLeft / rowCount);

                int row = 0;
                for (int i = 0; i < CardsLeft; i += cardsInRow)
                {
                    int cnt = (i + cardsInRow > CardsLeft) ? CardsLeft % cardsInRow : cardsInRow;

                    float rotationStep = cardDistance / centerDistance;
                    float rotationStart = rotationMid - (0.5f * (cnt - 1)) * rotationStep;

                    for (int j = 0; j < cnt; j++)
                    {
                        Card c = cards[i + j];
                        float rotation = rotationStart + j * rotationStep;

                        c.Position = rotationCenter + centerDistance * new Vector2((float)Math.Cos(rotation), (float)Math.Sin(rotation));
                        c.Rotation = MathHelper.PiOver2 - rotation;
                    }

                    rotationCenter += new Vector2(0, Math.Sign(rotationCenter.Y) * 80);
                    row++;
                }
            }
        }

        public Card SelectCard(Vector2 position)
        {
            foreach (Card c in cards)
            {
                if (c.Bounds.IsPointInside(position))
                    return c;
            }
            return null;
        }
    }
}
