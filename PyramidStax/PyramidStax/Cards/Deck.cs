﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Shapes.Geometry;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Util.Manipulation;

namespace PyramidStax.Cards
{
    public class Deck : CardCollection
    {
        public override int CardsLeft { get { return MainStack.CardsLeft; } }

        public Stack MainStack { get; private set; }

        public CardMover CardMover { get; private set; }


        public Deck(int cardSetCount, bool includeJokers, bool spacedStack, Vector2 position)
            : base(null)
        {
            CardMover = new CardMover();

            for (int set = 0; set < cardSetCount; set++)
            {
                for (int i = 0; i < (int)CardColor._Count; i++)
                {
                    for (int j = 1; j < (int)CardValue._Count; j++)
                    {
                        cards.Add(new Card(this, (CardColor)i, (CardValue)j));
                    }
                }

                if (includeJokers)
                {
                    cards.Add(new Card(this, CardColor.SpecialCardBlack, CardValue.Joker));
                    cards.Add(new Card(this, CardColor.SpecialCardRed, CardValue.Joker));
                    cards.Add(new Card(this, CardColor.SpecialCardBlack, CardValue.Joker));
                }
            }

            MainStack = new Stack(this)
                {
                    Position = position, //new Vector2(100, 740),
                    PositionOffest = (spacedStack)
                        ? new Vector2(-3f, 0f)
                        : new Vector2(0.3f, -0.2f),
                    OrientationOffset = 0f,
                    Hidden = true,
                    Orderly = true,
                    CardOnTopAtPosition = spacedStack,
                };

            bool tmp = Card.ForceTransform;
            Card.ForceTransform = true;
            CollectAndShuffle();
            Card.ForceTransform = false;
        }

        public void CollectAndShuffle()
        {
            using (new CardMoveTimer(this.CardMover, true))
            {
                using (new TempValue<bool>(Card.ForceTransform, true, (val) => Card.ForceTransform = val))
                {
                    Shuffle();

                    MainStack.Clear();
                    foreach (Card c in cards)
                        MainStack.AddCard(c);

                    MainStack.MakeOrderly();
                    Card.ForceTransform = false;
                }
            }

            foreach (Card c in cards)
                c.IsHidden = true;
        }

        public Card TakeCard(CardValue value, CardColor color)
        {
            foreach (Card c in MainStack.Cards)
            {
                if (c.Value == value && (c.CardColor == color || color == CardColor.undefined))
                {
                    MainStack.TakeCard(c);
                    return c;
                }
            }
            return null;
        }
        public override void TakeCard(Card card)
        {
            MainStack.TakeCard(card);
        }
        public override Card TakeCard()
        {
            return MainStack.TakeCard();
        }

        public void Update(float seconds)
        {
            CardMover.Update(seconds);
        }

        public void DrawMovingCards(SpriteBatch batch)
        {
            CardMover.Draw(batch);
        }

        public override void AddCard(Card card)
        {
            MainStack.AddCard(card);
        }
    }
}
