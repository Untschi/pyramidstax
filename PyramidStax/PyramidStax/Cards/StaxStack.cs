﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Shapes.Geometry;
using Shapes.Misc;

namespace PyramidStax.Cards
{
    public class StaxStack : PyramidStack
    {
        public struct PositionInfo : IEquatable<Card>, IEquatable<PositionInfo>
        {
            public Vector2 Position;
            public Angle Rotation;

            public bool Equals(Card other)
            {
                return this.Position == other.Position && this.Rotation == other.Rotation;
            }

            public bool Equals(PositionInfo other)
            {
                return this.Position == other.Position && this.Rotation == other.Rotation;
            }
        }
        
        List<PositionInfo> placeHolders = new List<PositionInfo>();

        public StaxStack(Deck deck)
            : base(deck)
        {
            HideBlockedCards = false;
        }

        public override void AddCard(Card card)
        {
            PositionInfo info = new PositionInfo() { Position = card.Position, Rotation = card.Rotation };
            if (!placeHolders.Contains(info))
                placeHolders.Add(info);

            base.AddCard(card);
        }
        public void AddPlaceHolder(Vector2 position, Angle rotation)
        {
            PositionInfo info = new PositionInfo() { Position = position, Rotation = rotation };
            if (!placeHolders.Contains(info))
                placeHolders.Add(info);
        }

        public void ShowPlaceholders()
        {
            ShowPlaceholders(CardValue.undefined);
        }
        public void ShowPlaceholders(CardValue placement)
        {
            bool tmp = Card.ForceTransform;
            Card.ForceTransform = true;

            foreach (PositionInfo info in placeHolders)
            {
                bool visible = true;
                foreach (Card c in cards)
                {
                    if (info.Equals(c))
                    {
                        visible = false;
                        break;
                    }
                }

                if (visible)
                {
                    Card placeholder = new Card(Deck, CardColor.undefined, CardValue.undefined);
                    cards.Add(placeholder);
                    placeholder.Position = info.Position;
                    placeholder.Rotation = info.Rotation;
                }
            }

            // hide unneeded placeholders
            for (int i = cards.Count - 1; i >= 0; i--)
            {
                if (cards[i].IsPlaceholder())
                {
                    bool allowed = true;
                    int cnt = 0;
                    foreach (Card c in GetCardsBeneath(cards[i]))
                    {
                        if (c.IsPlaceholder())
                            continue;

                        bool placeHolderPassed = false;
                        for (int j = 0; j < placeHolders.Count; j++)
                        {
                            if (placeHolderPassed && placeHolders[j].Equals(c))
                                allowed = false;

                            if (placeHolders[j].Equals(cards[i]))
                                placeHolderPassed = true;
                        }

                        if (placement != CardValue.undefined && !placement.IsNeighbour(c.Value))
                            allowed = false;
                        if (!allowed)
                            break;
                        cnt++;
                    }

                    if(!allowed || cnt == 0)
                        cards.RemoveAt(i);
                }
            }

            Card.ForceTransform = tmp;
            UpdateBlockings();
        }

        public void HidePlaceHolders()
        {
            for (int i = cards.Count - 1; i >= 0; i--)
            {
                if (cards[i].IsPlaceholder())
                    cards.RemoveAt(i);
            }
            UpdateBlockings();
        }


        internal void Transform(Vector2 centerTop, float rotation)
        {
            

            float minX = float.MaxValue;
            float minY = float.MaxValue;
            float maxX = float.MinValue;
            //float maxY = float.MinValue;

            foreach (PositionInfo pi in placeHolders)
            {
                if (pi.Position.X < minX)
                    minX = pi.Position.X;
                if (pi.Position.X > maxX)
                    maxX = pi.Position.X;

                if (pi.Position.Y < minY)
                    minY = pi.Position.Y;
                //if (pi.Position.X > maxX)
                //    maxY = pi.Position.Y;
            }
            Vector2 ct = new Vector2(MathHelper.Lerp(minX, maxX, 0.5f), minY);
            Vector2 diff = centerTop - ct;
            foreach (Card c in cards)
                c.Position += diff;
            for (int i = 0; i < placeHolders.Count; i++ )
                placeHolders[i] = new PositionInfo()
                { 
                    Position = placeHolders[i].Position + diff,
                    Rotation = placeHolders[i].Rotation
                };

            if (rotation != 0)
            {

                foreach (Card c in cards)
                {
                    Vector2 dir = c.Position - centerTop;
                    float dist = dir.Length();
                    float rot = rotation + (float)Math.Atan2(dir.Y, dir.X);
                    c.Position = centerTop + new Vector2(dist * (float)Math.Cos(rot), dist * (float)Math.Sin(rot));

                    c.Rotation -= rotation;
                }

                for (int i = 0; i < placeHolders.Count; i++)
                {
                    Vector2 dir = placeHolders[i].Position - centerTop;
                    float dist = dir.Length();
                    float rot = rotation + (float)Math.Atan2(dir.Y, dir.X);

                    placeHolders[i] = new PositionInfo()
                    {
                        Position = centerTop + new Vector2(dist * (float)Math.Cos(rot), dist * (float)Math.Sin(rot)),
                        Rotation = placeHolders[i].Rotation - rotation,
                    };
                }

            }
        }
        
        public IEnumerable<Card> GetCardsOnTopWhenCardIsRemoved(Card potentiallyRemovedCard)
        {
            foreach (Card c in cards)
            {
                if (c == potentiallyRemovedCard)
                    continue;

                if (!c.IsBlocked || CollisionDetector2D.RectRectIntersection(potentiallyRemovedCard.Bounds, c.Bounds))
                    yield return c;
            }
        }
    }
}
