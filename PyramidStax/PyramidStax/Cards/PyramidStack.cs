﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Shapes.Misc;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PyramidStax.Cards
{
    public class PyramidStack : CardCollection
    {
        static readonly Color NormalColor = Color.White;
        static readonly Color DarkColor = Color.Gray;

        public bool HideBlockedCards { get; set; }

        public PyramidStack(Deck deck)
            : this(deck, true)
        {
        }
        public PyramidStack(Deck deck, bool hideBlockedCards)
            : base(deck)
        {
            this.HideBlockedCards = hideBlockedCards;
        }

        public override void AddCard(Card card)
        {
            if (cards.Contains(card))
                return;

            card.IsHidden = HideBlockedCards;
            cards.Add(card);
        }

        public Card SelectCard(Vector2 location)
        {
            return SelectCard(location, true);
        }
        public Card SelectCard(Vector2 location, bool unblockedCardsOnly)
        {
            Card card = null;
            for(int i = cards.Count - 1; i >= 0; i--)
            {
                Card c = cards[i];
                if (c.Bounds.IsPointInside(location) && (!c.IsBlocked || !unblockedCardsOnly))
                {
                    card = c;
                    break;
                }
            }

            return card;
        }
        public override void TakeCard(Card card)
        {
            card.DrawColor = NormalColor;

            cards.Remove(card);

            for(int i = 0; i < cards.Count; i++)
            {
                Card c = cards[i];
                if (!c.IsBlocked)
                    continue;

                if (CollisionDetector2D.RectRectIntersection(card.Bounds, c.Bounds))
                {
                    bool blocked = false;
                    for(int j = i + 1; j < cards.Count; j++)
                    {
                        if (CollisionDetector2D.RectRectIntersection(c.Bounds, cards[j].Bounds))
                        {
                            blocked = true;
                            break;
                        }
                    }
                    c.IsBlocked = blocked;

                    if (HideBlockedCards)
                        c.IsHidden = blocked;
                }
            }
        }

        public List<Card> GetCardsBeneath(Card card)
        {
            int idx = cards.IndexOf(card);

            if (idx < 0)
                return null;

            List<Card> tmp = new List<Card>();
            for (int i = idx - 1; i >= 0; i--)
            {
                if (CollisionDetector2D.RectRectIntersection(card.Bounds, cards[i].Bounds))
                    tmp.Add(cards[i]);
            }

            return tmp;
        }

        public void Clear()
        {
            foreach (Card c in cards)
                c.DrawColor = NormalColor;

            cards.Clear();
        }
        public void UpdateBlockings()
        {
            using (new CardMoveTimer(Deck.CardMover, true))
            {
                for (int i = cards.Count - 1; i >= 0; i--)
                {
                    Card card = cards[i];

                    bool blocked = false;
                    for (int j = i + 1; j < cards.Count; j++)
                    {
                        if (CollisionDetector2D.RectRectIntersection(card.Bounds, cards[j].Bounds))
                        {
                            blocked = true;
                            break;
                        }
                    }

                    card.IsBlocked = blocked;

                    if (HideBlockedCards)
                        card.IsHidden = blocked;
                    else
                        card.DrawColor = (blocked) ? DarkColor : NormalColor;
                }
            }
        }

        public void ArrangeUp(Card card)
        {
            int idx = cards.IndexOf(card);
            if (idx == cards.Count)
                return;

            cards.RemoveAt(idx);

            if (idx == cards.Count)
                cards.Add(card);
            else
                cards.Insert(idx + 1, card);
        }
        public void ArrangeDown(Card card)
        {
            int idx = cards.IndexOf(card);
            if (idx == 0)
                return;

            cards.RemoveAt(idx);
            cards.Insert(idx - 1, card);
        }

        public void Arrange(Card card, int index)
        {
            cards.Remove(card);
            cards.Insert(index, card);
        }


        public override int CardsLeft
        {
            get { return cards.Count; }
        }

        public override Card TakeCard()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Card> GetCardsOnTop()
        {
            foreach (Card c in cards)
                if (!c.IsBlocked)
                    yield return c;
        }

        public void DrawCardOrder(SpriteBatch batch)
        {
            for (int i = 0; i < cards.Count; i++)
            {
                string s = (i + 1).ToString();
                Vector2 pos = cards[i].Position - 0.5f * Assets.FontSmall.MeasureString(s);

                batch.DrawString(Assets.FontSmall, s, pos + new Vector2(1, 1), Color.Black);
                batch.DrawString(Assets.FontSmall, s, pos + new Vector2(-1, -1), Color.Black);
                batch.DrawString(Assets.FontSmall, s, pos + new Vector2(-1, 1), Color.Black);
                batch.DrawString(Assets.FontSmall, s, pos + new Vector2(1, -1), Color.Black);
                batch.DrawString(Assets.FontSmall, s, pos, Color.White);
            }
        }
    }
}
