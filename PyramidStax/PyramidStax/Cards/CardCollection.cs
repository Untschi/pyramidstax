﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Microsoft.Xna.Framework.Graphics;

namespace PyramidStax.Cards
{
    public abstract  class CardCollection
    {
        protected List<Card> cards = new List<Card>();
        public ReadOnlyCollection<Card> Cards { get { return cards.AsReadOnly(); } }
        public abstract int CardsLeft { get; }

        public Deck Deck { get; private set; }

        protected CardCollection(Deck deck)
        {
            this.Deck = deck;
        }

        public abstract void AddCard(Card card);
        public abstract Card TakeCard();
        public abstract void TakeCard(Card card);
        

        public void Shuffle()
        {
            List<Card> tmp = new List<Card>();

            while (cards.Count > 0)
            {
                int idx = Sunbow.Util.Random.Next(cards.Count);
                tmp.Add(cards[idx]);
                cards.RemoveAt(idx);
            }

            cards = tmp;
        }

        public void HideAll()
        {
            using (new CardMoveTimer(Deck.CardMover, true))
            {
                foreach (Card c in cards)
                    c.IsHidden = true;
            }
        }

        public void ShowAll()
        {
            using (new CardMoveTimer(Deck.CardMover, true))
            {
                foreach (Card c in cards)
                    c.IsHidden = false;
            }
        }

        public virtual void Draw(SpriteBatch batch)
        {
            foreach (Card c in cards)
            {
                //if (!c.IsRenderedByMover)
                {
                    c.Draw(batch);
                }
            }
        }

    }
}
