﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PyramidStax.Cards
{
    public enum CardColor
    {
        SpecialCardRed = -2,
        SpecialCardBlack = -1,

        Cross = 0,
        Spades = 1,
        Heart = 2,
        Diamonds = 3,

        _Count,
        undefined,
    }

    public enum CardValue
    {
        Undo = -1,
        Joker = 0,

        Ace = 1, // must be 1 for an easier calculation to match 13
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Ten,
        Jack,
        Queen,
        King,

        _Count,
        undefined,
    }

    public static class CardEnumExtensions
    {

        public static bool IsNeighbour(this CardValue self, CardValue other)
        {
            if (self == CardValue.Joker || other == CardValue.Joker)
                return true;

            return Math.Abs((int)self - (int)other) == 1
                || (self == CardValue.Ace && other == CardValue.King)
                || (self == CardValue.King && other == CardValue.Ace);
        }

        public static Color GetFillColor(this CardColor self)
        {
            if (self == CardColor.Cross || self == CardColor.Spades || self == CardColor.SpecialCardBlack)
                return Color.Black;
            else
                return Color.Red;
        }
        public static Color GetOutlineColor(this CardColor self)
        {
            if (self == CardColor.Cross || self == CardColor.Spades || self == CardColor.SpecialCardBlack)
                return Color.White;
            else
                return Color.Black;
        }
    }
}
