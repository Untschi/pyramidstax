﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Shapes.Geometry;
using Sunbow.Util;

namespace PyramidStax.Cards
{
    public class CardMoveTimer : IDisposable
    {
        bool prevMode;
        CardMover mover;
        public CardMoveTimer(CardMover mover, bool moveImmediately)
        {
            this.mover = mover;
            this.prevMode = mover.MoveImmediately;

            mover.MoveImmediately = moveImmediately;
        }

        public void Dispose()
        {
            mover.MoveImmediately = prevMode;
            mover = null;
        }
    }

    public class CardMover
    {
        class CardEventData : IEquatable<CardEventData>
        {
            public bool IsLastAction;
            public float WaitTime;
            public Card Card;
            public Action<Card> Action;

            public void DoAction()
            {
                if(Action != null)
                    Action(Card);
            }

            public bool Equals(CardEventData other)
            {
                return this.Card == other.Card && this.WaitTime == other.WaitTime;
            }
        }

        public float DefaultWaitTime { get; set; }

        List<Card> movingCards = new List<Card>();
        //Dictionary<CardCollection, List<Card>> movingStacks = new Dictionary<CardCollection, List<Card>>();
        List<CardCollection> movingStacks = new List<CardCollection>();

        Queue<CardEventData> cardEvents = new Queue<CardEventData>();

        float nextEvent = float.MaxValue;

        public bool IsWorking { get { return movingCards.Count > 0 || movingStacks.Count > 0; } }

        public bool MoveImmediately { get; set; }

        List<Card> recentlyAddedImmediateMovingCards = new List<Card>();
        List<CardEventData> recentlyAddedTimedMovingCards = new List<CardEventData>();

        CardEventData nextImmediatelyAction;

        public CardMover()
        {
            MoveImmediately = true;
        }

        public void AddCard(Card card)
        {
            if (MoveImmediately)
            {
                CardEventData cevd = FindRecentCardEvent(card);
                if (cevd != null)
                    recentlyAddedTimedMovingCards.Remove(cevd);

                if (!recentlyAddedImmediateMovingCards.Contains(card))
                    recentlyAddedImmediateMovingCards.Add(card);

                //RegisterMovingCard(card);
            }
            else
            {
                //if (!recentlyAddedTimedMovingCards.Contains(card))
                //    recentlyAddedTimedMovingCards.Add(card);

                if (recentlyAddedImmediateMovingCards.Contains(card))
                    recentlyAddedImmediateMovingCards.Remove(card);

                AddCardEvent(card, (c) => RegisterMovingCard(c), DefaultWaitTime);
            }
        }
        public void AddCardEvent(Card card, Action<Card> cardEvt)
        {
            AddCardEvent(card, cardEvt, DefaultWaitTime);
        }

        public void AddCardEvent(Card card, Action<Card> cardEvt, float waitTime)
        {
            AddCardEvent(card, cardEvt, waitTime, false);
        }
        public void AddCardEvent(Card card, Action<Card> cardEvt, float waitTime, bool isLastAction)
        {
            CardEventData cardEvent = new CardEventData() { Card = card, Action = cardEvt, WaitTime = waitTime, IsLastAction = isLastAction };

            if (!recentlyAddedTimedMovingCards.Contains(cardEvent))
                recentlyAddedTimedMovingCards.Add(cardEvent);

        }

        public void RegisterMovingCard(Card card)
        {
            bool individualCard = true;
            foreach (CardCollection s in movingStacks)
            {
                if (s.Cards.Contains(card))
                {
                    individualCard = false;
                    break;
                }
            }

            if (individualCard && !movingCards.Contains(card))
            {
                movingCards.Add(card);
            }
            card.StartMoving();
        }

        private void UnregisterMovingCard(Card card)
        {
            card.MovingStopped();

            if (movingCards.Contains(card))
            {
                movingCards.Remove(card);
            }
            else
            {
                CardCollection removeStack = null;
                foreach (CardCollection s in movingStacks)
                {
                    if (s.Cards.Contains(card))
                    {
                        removeStack = s;
                        foreach (Card c in s.Cards)
                        {
                            if (c.IsRenderedByMover)
                            {
                                removeStack = null;
                                break;
                            }
                        }
                        break;
                    }
                }

                if (removeStack != null)
                    movingStacks.Remove(removeStack);
            }
        }

        public void RegisterMovingStack(CardCollection stack)
        {
            if (!movingStacks.Contains(stack))
            {
                movingStacks.Add(stack);
            }
        }

        public void Update(float seconds)
        {
            // dirty cards
            {
                if (recentlyAddedImmediateMovingCards.Count > 0)
                {
                    if (nextImmediatelyAction == null)
                    {
                        CardEventData data = new CardEventData() { WaitTime = 0, };
                        cardEvents.Enqueue(data);

                        nextImmediatelyAction = data;
                    }

                    foreach (Card card in recentlyAddedImmediateMovingCards)
                    {
                        AddImmediatelyCardMoveAction(nextImmediatelyAction, card);
                    }
                    recentlyAddedImmediateMovingCards.Clear();
                }

                for (int i = 0; i < recentlyAddedTimedMovingCards.Count; i++)
                {
                    CardEventData cardEvt = recentlyAddedTimedMovingCards[i];
                    if (!cardEvents.Contains(cardEvt) && (!cardEvt.IsLastAction || cardEvents.Count == 0))
                    {
                        cardEvents.Enqueue(cardEvt);
                        recentlyAddedTimedMovingCards.RemoveAt(i);
                        i--;
                    }
                }
                if (cardEvents.Count >= 1 && nextEvent > 10) // nothing should wait 10 seconds 
                    nextEvent = cardEvents.Peek().WaitTime;

            }

            // update events
            nextEvent -= seconds;
            while (nextEvent <= 0 && cardEvents.Count > 0)
            {
                CardEventData evt = cardEvents.Dequeue();

                if (evt == nextImmediatelyAction)
                    nextImmediatelyAction = null;

                evt.DoAction();


                if (cardEvents.Count > 0)
                    nextEvent += cardEvents.Peek().WaitTime;
                else
                    nextEvent = float.MaxValue;
            }

            // update cards
            for (int i = 0; i < movingCards.Count; i++)
            {
                movingCards[i].Update(seconds);

                if (!movingCards[i].IsMoving)
                {
                    UnregisterMovingCard(movingCards[i]);
                    i--;
                }
            }
            for (int i = movingStacks.Count - 1; i >= 0; i--)
            {
                CardCollection stack = movingStacks[i];
                bool stackMoving = false;
                foreach (Card c in movingStacks[i].Cards)
                {
                    c.Update(seconds);

                    if (!c.IsMoving)
                        UnregisterMovingCard(c);

                    if (c.IsRenderedByMover)
                        stackMoving = true;
                }

                if (!stackMoving)
                    movingStacks.Remove(stack);
            }
        }

        public void Draw(SpriteBatch batch)
        {
            foreach (CardCollection s in movingStacks)
                foreach(Card c in s.Cards)
                        c.Draw(batch);

            foreach (Card c in movingCards)
                c.Draw(batch);

        }


        private void AddImmediatelyCardMoveAction(CardEventData data, Card card)
        {
            data.Action += (c) => RegisterMovingCard(card);
        }


        private CardEventData FindRecentCardEvent(Card card)
        {
            foreach (CardEventData ced in recentlyAddedTimedMovingCards)
            {
                if (ced.Card == card)
                    return ced;
            }
            return null;
        }

        internal void Clear()
        {
            this.cardEvents.Clear();
            this.movingCards.Clear();
            this.movingStacks.Clear();
            this.nextEvent = float.MaxValue;
            this.nextImmediatelyAction = null;
            this.recentlyAddedImmediateMovingCards.Clear();
            this.recentlyAddedTimedMovingCards.Clear();
        }
    }
}
