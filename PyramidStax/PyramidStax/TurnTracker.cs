﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Sunbow.Util.Delegation;
using PyramidStax.Cards;
using PyramidStax.Modes;
using Microsoft.Xna.Framework.Graphics;
using PyramidStax.Modes.Feedback;

namespace PyramidStax
{
    public abstract class TurnTracker<T> where T : PyramidsSolitairMode
    {
        const int ScoreMul = 10;

        protected abstract Vector2 KilledUndoCardPosition { get; }

        protected Stack<Turn<T>> turns = new Stack<Turn<T>>();
        protected List<Card> lastSeries = new List<Card>();

        protected int[] turnPreview = new int[52];

        protected T game;

        public int SeriesCount { get { return lastSeries.Count; } }


        
        public TurnTracker(T game)
        {
            this.game = game;
        }

        internal void AddToSeries(Card card)
        {
            lastSeries.Add(card);
            CheckSeries();

            game.Score += ScoreMul * lastSeries.Count;
        }
        internal void RemoveLastFromSeries()
        {
            if (lastSeries.Count > 0)
            {
                game.Score -= ScoreMul * lastSeries.Count;

                lastSeries.RemoveAt(lastSeries.Count - 1);
            }
        }

        internal void ClearSeries()
        {
            lastSeries.Clear();
        }


        public void Reset()
        {
            turns.Clear();
            lastSeries.Clear();
            ResetTurnPreview();
            //Score = 0;
        }



        public void Do(Turn<T> turn)
        {
            turn.TurnTracker = this;

            if (turn.Do(game))
                turns.Push(turn);
        }

        public void Undo()
        {
            if (game.Undos.CardsLeft <= 0 || turns.Count == 0)
                return;


            Card c = game.Undos.TakeCard();
            c.Position = KilledUndoCardPosition;

            Turn<T> turn = turns.Pop();
            turn.Undo(game);

            ResetTurnPreview();
        }

        internal abstract void ResetTurnPreview();
        internal abstract void CheckSeries();
        public abstract void Draw(SpriteBatch batch);

    }
}
