using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Sunbow.Util.Delegation;
using PyramidStax.Modes;
using Sunbow.Util.IO;
using Sunbow.Util.Data;
using starLiGHT.GUI;
using Sunbow.Util;

namespace PyramidStax
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Main : Microsoft.Xna.Framework.Game, IGame
    {
        public static Main Instance { get; private set; }

        public Localization Loc { get; set; }


        public PyramidStax.GameSettings GameSettings { get; private set; }

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public SpriteBatch SpriteBatch { get { return spriteBatch; } }

        public ModeProxy Proxy { get; private set; }

        public GuiManager GuiManager { get; private set; }

        public bool WaitForWeb { get; set; }

        public Main()
        {
            Instance = this;

            GameSettings = new PyramidStax.GameSettings();

            graphics = new GraphicsDeviceManager(this);
            Content = new ThreadedContentManager(Services);
            Content.RootDirectory = "Content";

            graphics.SupportedOrientations = DisplayOrientation.Portrait;
            graphics.PreferredBackBufferWidth = 480;
            graphics.PreferredBackBufferHeight = 800;

#if WINDOWS_PHONE
            graphics.IsFullScreen = true;
#endif
            graphics.ApplyChanges();

            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            ViewController.Initialize(GraphicsDevice);

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            SystemParseMethods.RegisterCommonSystemTypeParseMethods();
            XnaParseMethods.RegisterCommonXnaParseMethods();

            Loc = new Localization(new Table(@"Content\Localization.csv", 6, 1));
            //Loc.Table[0, 0] = "key";
            Loc.Table[1, 0] = "en-US";
            Loc.Table[2, 0] = "de-DE";
            Loc.Table[3, 0] = "fr-FR";
            Loc.Table[4, 0] = "es-ES";
            Loc.Table[5, 0] = "pt-PT";

            GuiManager = new starLiGHT.GUI.GuiManager(this, @"Content\GuiSkin.xml");
            GuiManager.LoadSkin();
            //GuiManager.IsScissoringEnabled = false;

            Proxy = new ModeProxy(this);

            Proxy.Register(new Modes.Menu.MainMenu(GuiManager));
            Proxy.Register(new Modes.Menu.SettingsMode());
            Proxy.Register(new Modes.Menu.LevelSelection.LevelSelectionMode());
            Proxy.Register(new Modes.Menu.HighscoreListMode(GuiManager));

            Proxy.Register(new Modes.Menu.Hud.GameGui());
            Proxy.Register(new Modes.Menu.Hud.PauseMenu());
            Proxy.Register(new Modes.Menu.Hud.LevelOverScreen());
            Proxy.Register(new Modes.Menu.Hud.StaxEndScreen());

            Proxy.Register(new Modes.Menu.StaxSettingsMode());

            Proxy.Register(new Modes.Menu.Help.PeaksHelpMode());
            Proxy.Register(new Modes.Menu.Help.Pyramids13HelpMode());
            Proxy.Register(new Modes.Menu.Help.StaxHelpMode());
            Proxy.Register(new Modes.Menu.Help.EditorHelpMode());

            Proxy.Register(new Modes.Edit.PyramidsEdit());

            Proxy.Register(new Modes.Pyramids.PyramidsGame());
            Proxy.Register(new Modes.Pyramids13.Pyramids13Game());
            Proxy.Register(new Modes.Stax.StaxGame());


            Proxy.SetMode<Modes.Menu.SettingsMode>();
            Proxy.SetMode<Modes.Menu.MainMenu>();
        }
        
        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            Proxy.Update(gameTime);
            

            GuiManager.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Globals.Bright);//new Color(255, 238, 198));

            if (WaitForWeb)
                return;

            // TODO: Add your drawing code here
            Proxy.Draw(gameTime, spriteBatch);


            spriteBatch.Prepare();

            spriteBatch.Draw(Assets.GuiSheet.Texture, new Rectangle(0, 0, 480, 80), Assets.ColorDark.SourceRectangle, Color.White);
            //Assets.GameSheet.Draw(spriteBatch, Assets.TimeInnerFrame, Vector2.Zero, 0,
            //    new Vector2(480f / Assets.TimeInnerFrame.SourceRectangle.Width, 80 / Assets.TimeInnerFrame.SourceRectangle.Height));

            spriteBatch.End();

            GuiManager.Draw(gameTime);

            base.Draw(gameTime);
        }

    }
}
