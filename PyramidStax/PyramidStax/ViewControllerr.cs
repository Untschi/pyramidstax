﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PyramidStax
{
    public enum VideoBoxingMode
    {
        /// <summary>
        /// The aspect ratio is correct. No Bars needed.
        /// </summary>
        None = 0,
        /// <summary>
        /// The aspect ratio is too small. Bars above and below needed.
        /// </summary>
        Letterboxing = 1,
        /// <summary>
        /// The aspect ratio is too big. Bars on the left and right needed.
        /// </summary>
        Pillarboxing = 2,
    }

    public static class ViewController
    {
        const int DEFAULT_WIDTH = 480;
        const int DEFAULT_HEIGHT = 800;

        static Matrix transform = Matrix.Identity;
        static bool isTransformIdentity = true;
        public static Viewport Viewport { get; private set; }
        public static Matrix ViewMatrix { get { return transform * cameraMatrix; } }

        public static Matrix CameraMatrix { get { return cameraMatrix; } set { if (cameraMatrix != value) { cameraMatrix = value; if (MatrixChanged != null) { MatrixChanged(); } isTransformIdentity = false; } } }
        static Matrix cameraMatrix = Matrix.Identity;

        public static event Action MatrixChanged;

		public static bool UseFakeLandscapeMode { get; set; }
		
	    public static int DefaultWidth { get { return (UseFakeLandscapeMode) ? DEFAULT_HEIGHT : DEFAULT_WIDTH; } }
		public static int DefaultHeight { get { return (UseFakeLandscapeMode) ? DEFAULT_WIDTH : DEFAULT_HEIGHT; } }

        public static VideoBoxingMode BoxingMode { get; private set; }
        public static int BoxingBarSize { get; private set; }

        internal static void Initialize(GraphicsDevice graphics)
        {
            int width, height;
#if ANDROID
            width = graphics.Viewport.Width;
            height = graphics.Viewport.Height;
//#elif IOS
            width = graphics.PresentationParameters.BackBufferHeight;
            height = graphics.PresentationParameters.BackBufferWidth;
#else
            width = graphics.PresentationParameters.BackBufferWidth;
            height = graphics.PresentationParameters.BackBufferHeight;

#endif

            Initialize(graphics, width, height);
        }
        public static void Initialize(GraphicsDevice graphics, int width, int height)
        {
            float ratio = (float)width / height;
            float origRatio = (float)DefaultWidth / DefaultHeight;
			
			float viewWidth = width;
			float viewHeight = height;
			
            if (ratio > origRatio)
                width = (int)(height * origRatio);
            else if (ratio < origRatio)
                height = (int)(width / origRatio);

            int x = graphics.Viewport.X + (int)(0.5f * (viewWidth - width));
            int y = graphics.Viewport.Y + (int)(0.5f * (viewHeight - height));

            if (x > 0)
            {
                BoxingBarSize = x;
                BoxingMode = VideoBoxingMode.Pillarboxing;
            }
            else if (y > 0)
            {
                BoxingBarSize = y;
                BoxingMode = VideoBoxingMode.Letterboxing;
            }
            else
            {
                BoxingMode = VideoBoxingMode.None;
            }


            Viewport = new Viewport() { X = x, Y = y, Width = width, Height = height };
			
            float wMul = (float)width / DefaultWidth;
            float hMul = (float)height / DefaultHeight;

            float scale = Math.Min(wMul, hMul);

			if(UseFakeLandscapeMode)
			{
				transform = Matrix.CreateRotationZ(MathHelper.PiOver2) * Matrix.CreateScale(scale) * Matrix.CreateTranslation(width + x, y, 0);
			}
			else
			{
                if (scale != 1 || BoxingMode != VideoBoxingMode.None)
                    transform = Matrix.CreateScale(scale) * Matrix.CreateTranslation(x, y, 0);
			}

            isTransformIdentity = !UseFakeLandscapeMode && (scale == 1) && (Viewport.X == 0) && (Viewport.Y == 0);

            //graphics.Viewport = Viewport;

        }

        public static void Prepare(this SpriteBatch self)
        {
            if (isTransformIdentity)
                self.Begin();
            else
                self.Begin(SpriteSortMode.Deferred, null, null, null, null, null, ViewMatrix);
        }

        public static void Prepare(this SpriteBatch self, BlendState blendState)
        {
            if (isTransformIdentity)
                self.Begin(SpriteSortMode.Immediate, blendState);
            else
                self.Begin(SpriteSortMode.Deferred, blendState, null, null, null, null, ViewMatrix);
        }

    }
}
