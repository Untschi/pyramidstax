﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.IO;
using System.IO;

namespace PyramidStax
{
    public enum GameType
    {
        PyramidPeaks = 0,
        Pyramids13 = 1,
        Stax = 2,
    }
    public static class GameTypeExtensions
    {
        public static string GetShortcut(this GameType self)
        {
            switch (self)
            {
                case GameType.PyramidPeaks:
                    return "peaks";
                case GameType.Pyramids13:
                    return "py13";
                case GameType.Stax:
                    return "stax";
            }
            return null;
        }
        public static string GetContentRelatedName(this GameType self)
        {
            switch (self)
            {
                case GameType.PyramidPeaks:
                case GameType.Pyramids13:
                    return "Pyramids";
                case GameType.Stax:
                    return "Stax";
            }
            return null;
        }
    }


    public class GameSettings
    {
        public GameType GameType;


        List<Table> levels = new List<Table>();

        public int CurrentLevelIndex { get { return currentLevelIndex; } }
        int currentLevelIndex;

        public Table CurrentLevelData { get { return levels[currentLevelIndex % levels.Count]; } }
        public bool IsSingleLevelMode { get { return levels.Count == 1; } }

        public bool IsStaxPlayerAHuman, IsStaxPlayerBHuman;

        public string PlayerName;
        public float MusicVolume, SfxVolume;

        float levelDuration;
        public float LevelDuration { get { return (IsSingleLevelMode) ? levelDuration * Assets.TimeLossCurve.Evaluate(currentLevelIndex / 15f) : levelDuration; } set { levelDuration = value; } }


        public string GameId { get; private set; }

        public GameSettings()
        {
            IsStaxPlayerAHuman = false;
            IsStaxPlayerBHuman = true;

            MusicVolume = 0.7f;
            SfxVolume = 0.6f;
        }

        public void InitPyramidLevels(Table levelMapping, string id)
        {
            levels.Clear();
            currentLevelIndex = 0;

            if (levelMapping.Get<bool>("is_campaign", id))
            {
                int row = levelMapping.GetRowIndex(id);

                while (row < levelMapping.Rows - 1) // -1 because increment comes first
                {
                    row++;

                    bool tmp;
                    if (levelMapping.TryGet<bool>("is_campaign", row, out tmp) && tmp)
                        break;

                    AddLevel(levelMapping["id", row]);
                }
            }
            else
            {
                AddLevel(id);
            }

            GameId = id;
        }

        private void AddLevel(string id)
        {
            Table level = Main.Instance.Content.Load<Table>(Path.Combine("Levels", Path.Combine("Pyramids", id)));
            levels.Add(level);
        }

        public void SetSingleLevel(Table table, string id)
        {
            levels.Clear();
            levels.Add(table);
            currentLevelIndex = 0;

            GameId = id;
        }

        public bool NextLevel()
        {
            if (IsSingleLevelMode)
            {
                currentLevelIndex++;
                return true;
            }

            if (currentLevelIndex + 1 >= levels.Count)
                return false;

            currentLevelIndex++;
            return true;
        }


    }
}
