﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.Delegation;
using PyramidStax.Modes;

namespace PyramidStax
{
    public abstract class Turn<T> where T : PyramidsSolitairMode
    {
        public TurnTracker<T> TurnTracker { get; set; }

        public abstract bool Do(T game);
        public abstract void Undo(T game);
    }
}
