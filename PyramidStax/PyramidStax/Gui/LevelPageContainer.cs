﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI.Widgets;
using starLiGHT.GUI;
using Microsoft.Xna.Framework;

namespace PyramidStax.Gui
{
    public class LevelPageContainer : PageContainer
    {
        StackPanel currentPage;
        StackPanel currentRow;

        public LevelPageContainer(GuiManager manager, string name, Vector2 buttonSize, Vector2 pageSize)
            : base (manager, name, buttonSize, pageSize)
        {
        }


        public void AddEntry(Widget entry)
        {
            if (currentPage == null)
            {
                //Commit();
                AddPage();
            }

            if (currentRow == null || currentRow.Size.X + entry.Size.X > base.PageSize.X)
            {
                if (currentPage.Widgets.Count >= 3)//currentPage.Size.Y + entry.Size.Y > base.PageSize.Y)//
                {
                    Commit();
                    AddPage();
                }

                currentRow = new StackPanel(Manager, Name + "row" + Pages.Count + "_" + currentPage.Widgets.Count.ToString())
                {
                    Orientation = Orientation.Horizontal,
                    Size = new Vector2(base.PageSize.X, entry.Size.Y),
                };

                currentPage.Widgets.Add(currentRow);
            }
           

            currentRow.Widgets.Add(entry);
        }

        private void AddPage()
        {
            currentPage = new StackPanel(base.Manager, base.Name + "_Page" + Pages.Count)
            {
                Size = PageSize,
            };
            Pages.Add(currentPage);
        }

        public override void Clear()
        {
            foreach (Widget w in Pages)
            {
                StackPanel sp = (w as StackPanel);
                foreach (Widget s in sp.Widgets)
                {
                    (s as StackPanel).Clear();
                }
                sp.Clear();
            }
            currentPage = null;
            currentRow = null;

            base.Clear();
        }

        public void Commit()
        {
            if(currentPage != null)
                currentPage.FitSizeToContents();
        }

    }
}
