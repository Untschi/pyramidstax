﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI;
using Microsoft.Xna.Framework;
using starLiGHT.GUI.Widgets;

namespace PyramidStax.Gui
{
    public class ListPageContainer : PageContainer
    {
        StackPanel currentPage;

        public ListPageContainer(GuiManager manager, string name, Vector2 buttonSize, Vector2 pageSize)
            : base (manager, name, buttonSize, pageSize)
        {
        }


        public void AddEntry(Widget entry)
        {
            if (currentPage == null || currentPage.Size.Y + entry.Size.Y > base.PageSize.Y)
            {
                Commit();
                AddPage();
            }

            currentPage.Widgets.Add(entry);
        }

        private void AddPage()
        {
            currentPage = new StackPanel(base.Manager, base.Name + "_Page" + Pages.Count)
            {
                Size = PageSize,
            };
            Pages.Add(currentPage);
        }

        public override void Clear()
        {
            foreach (Widget s in Pages)
                (s as StackPanel).Clear();

            currentPage = null;

            base.Clear();
        }

        public void Commit()
        {
            if(currentPage != null)
                currentPage.FitSizeToContents();
        }

    }
}
