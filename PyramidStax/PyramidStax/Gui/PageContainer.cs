﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI.Widgets;
using Microsoft.Xna.Framework;
using starLiGHT.GUI;

namespace PyramidStax.Gui
{
    public class PageContainer : ContainerWidget
    {
        public List<Widget> Pages { get { return pages; } }
        List<Widget> pages = new List<Widget>();
        
        Button nextBtn, previousBtn;
        Label pageNumberLbl;

        public Vector2 PageSize { get; private set; }
        int currentIndex;

        public PageContainer(GuiManager manager, string name, Vector2 buttonSize, Vector2 pageSize)
            : base(manager, name)
        {

            this.PageSize = pageSize;

            previousBtn = new Button(manager, name + "_prev", "LeftButton")
            {
                Location = new Vector2(0, pageSize.Y),
                Size = buttonSize,
            };
            previousBtn.Click += (o, a) => SetPageIndex(currentIndex - 1);

            nextBtn = new Button(manager, name + "_next", "RightButton")
            {
                Location = new Vector2(pageSize.X - buttonSize.X, pageSize.Y),
                Size = buttonSize,
            };
            nextBtn.Click += (o, a) => SetPageIndex(currentIndex + 1);

            pageNumberLbl = new Label(manager, name + "_pageNum")
            {
                Location = new Vector2(buttonSize.X, pageSize.Y + 40),
                Size = new Vector2(pageSize.X - 2 * buttonSize.X, buttonSize.Y),
                TextAlignment = starLiGHT.GUI.TextAlignment.Center,
                Font = Assets.FontMedium,
            };

            this.Size = new Vector2(pageSize.X, pageSize.Y + buttonSize.Y);
            this.Widgets.Add(previousBtn);
            this.Widgets.Add(nextBtn);
            this.Widgets.Add(pageNumberLbl);

        }

        public void SetPageIndex(int index)
        {
            if (pages.Count > 0)
            {
                this.Widgets.Remove(pages[currentIndex]);

                currentIndex = (index + pages.Count) % pages.Count;

                this.Widgets.Add(pages[currentIndex]);
            }
            else
            {
                currentIndex = 0;
            }
            UpdateText();
        }

        public void UpdateText()
        {
            pageNumberLbl.Text = string.Format("{0} / {1}", currentIndex + 1, pages.Count); 
        }

        public virtual void Clear()
        {
            foreach (Widget w in pages)
            {
                base.Widgets.Remove(w);
            }
            currentIndex = 0;
            pages.Clear();
        }

    }
}
