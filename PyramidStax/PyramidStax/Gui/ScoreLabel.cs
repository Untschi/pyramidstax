﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI.Widgets;
using starLiGHT.GUI;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace PyramidStax.Gui
{
    public class ScoreLabel : ContainerWidget
    {
        const float PrefixSpace = 5;

        Label prefixLbl, titleLbl, scoreLbl;
        public string Prefix { get { return prefixLbl.Text; } set { prefixLbl.Text = value ?? ""; } }
        public string Title { get { return titleLbl.Text; } set { titleLbl.Text = value; } }
        public int Score { get { return int.Parse(scoreLbl.Text.Replace(" ", "")); } set { scoreLbl.Text = value.ToString("### ##0"); } }

        public new SpriteFont Font { get { return base.Font; } set { base.Font = value; titleLbl.Font = value; scoreLbl.Font = value; } }
        public new Color ForeColor { get { return base.ForeColor; } set { base.ForeColor = value; titleLbl.ForeColor = value; scoreLbl.ForeColor = value; } }


        public ScoreLabel(GuiManager manager, string name)
            : base (manager, name)
        {
            prefixLbl = new Label(manager, Name + "title")
            {
                Font = Assets.FontMedium,
                TextAlignment = starLiGHT.GUI.TextAlignment.Right,
                Size = new Vector2(50, 30),
            };
            titleLbl = new Label(manager, Name + "title")
            {
                Font = Assets.FontMedium,
                Location = new Vector2(prefixLbl.Size.X + PrefixSpace, 0),
            };
            scoreLbl = new Label(manager, name + "score")
            {
                Font = Assets.FontMedium,
                TextAlignment = starLiGHT.GUI.TextAlignment.Right,
            };

            this.Widgets.Add(titleLbl);
            this.Widgets.Add(scoreLbl);
            this.Widgets.Add(prefixLbl);
        }

        public override void OnSizeChanged(object sender, EventArgs e)
        {
            base.OnSizeChanged(sender, e);

            titleLbl.Size = new Vector2(Size.X - (prefixLbl.Size.X + PrefixSpace), Size.Y);
            scoreLbl.Size = Size;
        }
    }
}
