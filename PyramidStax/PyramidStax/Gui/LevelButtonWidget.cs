﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI.Widgets;
using starLiGHT.GUI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PyramidStax.Modes.Menu.LevelSelection;

namespace PyramidStax.Gui
{
    public class LevelButtonWidget : Button
    {
        static readonly Vector2 WidgetSize = new Vector2(133, 133);//new Vector2(35 + ThumbnailPool.SCALE * 480, 8 + ThumbnailPool.SCALE * Globals.TIMER_POS_Y);

        Texture2D levelTexture;
        public Texture2D LevelTexture { get { return levelTexture; } set { levelTexture = value; } }

        public LevelButtonWidget(GuiManager manager, string name, string locText)
            : base(manager, name, "LevelButton")
        { 
            this.Text = locText;
            this.Font = Assets.FontSmall;
            this.ForeColor = Globals.Dark;
            this.Size = WidgetSize;
        }

        public override void OnStateChanged(object sender, EventArgs e)
        {
            base.OnStateChanged(sender, e);


            this.ForeColor = Globals.Dark;
        }

        public override void Draw(GameTime gameTime, IGuiRenderer renderer)
        {
            base.Draw(gameTime, renderer);

            if(levelTexture != null)
                renderer.DrawSprite(levelTexture, 0, this.DisplayRectangle.X + 17, this.DisplayRectangle.Y + 5 /* 23 */, ThumbnailPool.SCALE * 480, ThumbnailPool.SCALE * (Globals.TIMER_POS_Y - 80), 0, 0, levelTexture.Width, levelTexture.Height, Color.White);
        }
    }
}
