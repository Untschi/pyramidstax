﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PyramidStax.Cards;
using Microsoft.Xna.Framework;

namespace PyramidStax
{
    public static class Globals
    {
        public static readonly Color VeryBright = new Color(255, 250, 234);
        public static readonly Color Bright = new Color(255, 238, 198);
        public static readonly Color Dark = new Color(171, 152, 109);
        public static readonly Color VeryDark = new Color(84, 68, 31);
        public static readonly Color SuccessColor = new Color(229, 234, 178);
        public static readonly Color FailColor = new Color(255, 200, 178);


        public const float PeaksLevelDuration = 2 * 60;
        public const float Py13LevelDuration = 2.5f * 60;

        public const float LevelOverWaitDuration = 5 * Card.CardMoveDuration;


        public const int TIMER_HEIGHT = 17;
        public const int TIMER_POS_Y = 625;
        public const int TURNER_POS_Y = TIMER_POS_Y + TIMER_HEIGHT;


        public const float CENTER_HEIGHT = 430;
        public const float AD_HEIGHT = 80;

        public const float VisibleHeight =
#if LITE
            800 - AD_HEIGHT;
#else
            800;
#endif

        public const int MAX_HAND_CARDS = 5;

        public const float FAST_CARD_WAIT = 0.15f;
        public const float SLOW_CARD_WAIT = 0.5f;


        public static readonly Card CardTemplate = new Card(null, CardColor.undefined, CardValue.undefined);

        static readonly int[][] RANDOM_STACK_IDX = new int[][]
        {
            new int[] { 0, 1, 2, },
            new int[] { 0, 2, 1, },

            new int[] { 1, 0, 2, },
            new int[] { 1, 2, 0, },

            new int[] { 2, 0, 1, },
            new int[] { 2, 1, 0, },
        };
        public static int[] GetRandomStackIndices()
        {
            return RANDOM_STACK_IDX[Sunbow.Util.Random.Next(RANDOM_STACK_IDX.Length)];
        }

        static int nameIndexCounter = 0;
        public static string GetCustomName(string prefix)
        {
            return prefix + nameIndexCounter++;
        }

    }
}
