﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.IO;
using PyramidStax.Cards;
using Microsoft.Xna.Framework;
using System.IO.IsolatedStorage;
using System.IO;
using Sunbow.Util;

namespace PyramidStax
{
    public static class PersistHelper
    {
        public static void SavePyramidStack(string filename, PyramidStack stack, bool saveCardValue)
        {
            Table table = new Table(filename, (saveCardValue) ? 3 : 2, stack.CardsLeft + 1);
            table[0, 0] = "pos";
            table[1, 0] = "ori";
            if (saveCardValue)
            {
                table[2, 0] = "value";
            }

            for (int i = 0; i < stack.CardsLeft; i++)
            {
                int idx = i + 1;
                table["pos", idx] = stack.Cards[i].Position.UnParse();
                table["ori", idx] = stack.Cards[i].Rotation.Radians.ToString();

                if (saveCardValue && stack.Cards[i].IsPlaceholder())
                {
                    table["value", idx] = (stack.Cards[i].IsPlaceholder()) ? CardValue.undefined.ToString() : "";
                }
            }
            //table.Save();

            using (IsolatedStorageFile file = Helper.GetUserStoreForAppDomain())
            {
                string dir = Path.GetDirectoryName(filename);
                if (!string.IsNullOrEmpty(dir) && !file.DirectoryExists(dir))
                    file.CreateDirectory(dir);

                if (file.FileExists(filename))
                    file.DeleteFile(filename);
                using (IsolatedStorageFileStream stream = file.OpenFile(filename, FileMode.OpenOrCreate))
                {
                    table.Save(stream, CSVSetting.UniqueColumnHeaders);
                    stream.Close();
                }
            }
        }
        public static void LoadPyramidStack(Table table, Deck deck, PyramidStack stack)
        {
            LoadPyramidStack(table, deck, stack, false);
        }
        public static void LoadPyramidStack(Table table, Deck deck, PyramidStack stack, bool isEditorLoading)
        {
            for (int i = 1; i < table.Rows; i++)
            {
                Card c;
                Vector2 pos = table.Get<Vector2>("pos", i);
                float ori = table.Get<float>("ori", i);

                CardValue val;
                if (table.TryGet<CardValue>("value", i, out val))
                {
                    if (val == CardValue.undefined)
                    {
                        if(isEditorLoading)
                        {
                            c = new Card(null, CardColor.undefined, CardValue.undefined);   
                        }
                        else
                        {
                            (stack as StaxStack).AddPlaceHolder(pos, ori);
                            continue;
                        }
                    }
                    else
                    {
                        CardColor col;
                        if (table.TryGet<CardColor>("color", i, out col))
                            c = deck.TakeCard(val, col);
                        else
                            c = deck.TakeCard(val, CardColor.undefined);
                    }
                }
                else
                {
                    c = deck.TakeCard();
                }

                c.Position = new Vector2((int)pos.X, (int)pos.Y);
                c.Rotation = ori;

                stack.AddCard(c);
            }
            //stack.UpdateBlockings();
        }

    }
}
